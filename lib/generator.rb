module Jasper
  
  class Generator
    include Config
  
    def self.generate_report(xml_data, report_design, output_type, select_criteria)
      report_design << '.jasper' if !report_design.match(/\.jasper$/)
      
      dir = Rails.root.join("lib/jasper-bridge")
      classpath = "#{dir}/jasper/bin"
        
      Dir.foreach("#{dir}/jasper/lib") do |file|
        classpath << ":#{dir}/jasper/lib/"+file if (file != '.' and file != '..' and file.match(/\.jar$/))
      end

      pipe = IO.popen "java -cp \"#{classpath}\" XmlJasperInterface -o:#{output_type} -patient_id:1 -f:reports/#{report_design} -x:#{select_criteria}", "w+b" 
      pipe.write xml_data
      pipe.close_write
      result = pipe.read
      pipe.close
      result
    end
    
  end

end