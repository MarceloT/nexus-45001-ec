# STACK RAILS BASE - Free Rails Admin Theme

Stack Rails Base - the boilerplate of Stack Rails Admin.

rails g scaffold Project name:string description:text  due_date:date status:integer owner_id:integer project_type_id:integer 

rails g scaffold WorkCenter name:string description:text active:boolean company_id:integer address:string

rails g scaffold Commission work_center_id:integer company_id:integer nro_members:integer date:datetime active:boolean type_commission:integer member_1:integer member_2:integer member_3:integer member_4:integer member_5:integer member_6:integer member_7:integer member_8:integer member_9:integer member_10:integer member_11:integer member_12:integer


rails g scaffold Project name:string description:text object:text scope:text due_date:date status:integer owner_id:integer project_type_id:integer company_id:integer created_by_id:integer

rails g scaffold Task name:string description:text due_date:date status:integer owner_id:integer project_id:integer



rails g scaffold CompanyProcess name:string description:text owner_id:integer company_id:integer parent_id:integer level:integer process_in_id:integer process_out_id:integer process_type:integer

rails g model Activity name:string process_id:integer number:integer

rails g scaffold PersonalProtection name:string protection_type_id:integer company_id:integer stock:integer image:string features:text details:text

rails g scaffold PersonalProtectionPatient personal_protection_id:integer patient_id:integer date:datetime signature:string company_id:integer

rails g scaffold PersonalProtectionEntry origin_work_center_id:integer destination_work_center_id:integer patient_id:integer date:datetime signature:string company_id:integer user_id:integer description:text

rails g scaffold Recipe diagnostic_description:text medicines:text indications:text medical_record_id:integer patient_id:integer

rails g model MedicalAppointmentExam medical_appointment_id:integer exam_id:integer description:string

rails g model HealthCycle year:string month:string patient_id:integer area_id:integer work_center_id:integer company_id:integer complete:boolean active:boolean

rails g model PersonalProtectionEntryPersonalProtection personal_protection_entry_id:integer personal_protection_id:integer max_date:datetime changeable:boolean description:text


rails g scaffold MedicalCertificate date:datetime patient_id:integer medical_record_id:integer reason:text diagnostic_description:text days:integer starts_date:datetime ends_date:datetime created_by_id:integer document_id:integer type_certificate:integer

rails g model ExamPatient patient_id:integer exam_id:integer determination:text result:string unit:string ref_value:string nro_ord:string active:true


Metodolgia

"William T. Fine", "INSST / INSHT"
