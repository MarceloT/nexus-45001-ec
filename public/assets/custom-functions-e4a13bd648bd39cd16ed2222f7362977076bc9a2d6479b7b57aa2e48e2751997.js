$(window).scroll(function() {
  count = $(window).scrollTop();
  if (count > 85){
  	$("#sticky-header").addClass('fix-section');
  	width_sidebar = $("#default-drawer").width();
  	$("#sticky-header").css('width', 'calc(100% - ' + width_sidebar + 'px)');
  	$("#to_sticky").appendTo("#sticky-header");
  }
  else{
  	$("#sticky-header").removeClass('fix-section');
  	$("#sticky-header").css('width', 'auto');
  	$("#sticky-header #to_sticky").appendTo("#to_sticky-container");
  }
});

$(document).on('nested:fieldAdded', function(event){
  // this field was just inserted into your form
  var field = event.field; 
  // it's a jQuery object already! Now you can find date input
  // var dateField = field.find('.select_2');
  // and activate datepicker on it
  $('#medical_record_diagnostics .select_2').select2({
    language: {
      noResults: function () {
        return "No se encontró resultados";
      }
    },
    placeholder: function(){
      $(this).data('placeholder');
    },
    containerCssClass: ':all:',
    theme: "bootstrap"
  }).on('change', function (evt) {
    console.log(evt);
  });

  $(".date-selector").flatpickr({
    enableTime: false,
    dateFormat: "Y-m-d",
  });    
})



;
