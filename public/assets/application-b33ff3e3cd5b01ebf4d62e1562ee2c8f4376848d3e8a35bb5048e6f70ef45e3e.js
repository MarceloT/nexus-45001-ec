
// Self Initialize DOM Factory Components
domFactory.handler.autoInit();

// Connect button(s) to drawer(s)
var sidebarToggle = Array.prototype.slice.call(document.querySelectorAll('[data-toggle="sidebar"]'));
sidebarToggle.forEach(function(e) {
    e.addEventListener("click", function(e) {
        var t = e.currentTarget.getAttribute("data-target") || "#default-drawer",
            a = document.querySelector(t);
        a && a.mdkDrawer.toggle()
    })
});
var drawers = document.querySelectorAll(".mdk-drawer");
(drawers = Array.prototype.slice.call(drawers)).forEach(function(e) {
    e.addEventListener("mdk-drawer-change", function(e) {
        if (e.target.mdkDrawer) {
            document.querySelector("body").classList[e.target.mdkDrawer.opened ? "add" : "remove"]("has-drawer-opened");
            var t = document.querySelector('[data-target="#' + e.target.id + '"]');
            t && t.classList[e.target.mdkDrawer.opened ? "add" : "remove"]("active")
        }
    })
}), $(".sidebar .collapse").on("show.bs.collapse", function(e) {
    e.stopPropagation();
    var t = $(this).parents(".sidebar-submenu").get(0) || $(this).parents(".sidebar-menu").get(0);
    $(t).find(".open").find(".collapse").collapse("hide"), $(this).closest("li").addClass("open")
}), $(".sidebar .collapse").on("hidden.bs.collapse", function(e) {
    e.stopPropagation(), $(this).closest("li").removeClass("open")
}), $('[data-toggle="tooltip"]').tooltip(), $('[data-toggle="tab"]').on("hide.bs.tab", function(e) {
    $(e.target).removeClass("active")
});

// ENABLE TOOLTIPS
$('[data-toggle="tooltip"]').tooltip()

load_tables();

$(document).on("turbolinks:load",function(){
  
 
})

;
