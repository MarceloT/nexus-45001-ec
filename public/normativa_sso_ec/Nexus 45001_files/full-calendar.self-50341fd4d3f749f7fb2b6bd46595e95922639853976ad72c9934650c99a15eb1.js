// var initialize_calendar;

function lock_calendar(){
  console.log("lock");
  $(".calendar").addClass("lock");
  $(".lds-spinner").show();

}
function unlock_calendar(){
  console.log("unlock");
  $(".calendar").removeClass("lock"); 
  $(".lds-spinner").hide();
}


// function initialize_calendar() {
//   console.log("asdasd111");
//   $('.calendar.empty').each(function(){
//     $('.calendar.empty').empty();
//     var calendar = $(this);
//     calendar.fullCalendar({
//       header: {
//         left: 'prev,next, today',
//         center: 'title',
//         right: 'month,agendaWeek,agendaDay'
//       },
//       locale: 'es',
//       selectable: true,
//       selectHelper: true,
//       editable: false,
//       weekNumbers: true,
//       droppable: false,
//       events: '/events.json',
//       eventRender: function (event, element) {
//         dates = ""
//         total_dates = event.dates.length
//         $.each(event.dates, function( index, value ) {
//           dates = dates + '<span class="event-date" style="width:'+101/total_dates+'%">'+value+'</span>'
//         });
//         element.find('.fc-title').after('<div class="hr-line-solid"></div>'+dates+'</div>');
//       },

//       select: function(start, end) {
//         end = end.subtract(30, "minutes")
//         lock_calendar();
//         start_date = moment(start).format("MM/DD/YYYY HH:mm")
//         end_date = moment(end).format("MM/DD/YYYY HH:mm")
//         $.getScript('/events/new?start='+start_date+'&end='+end_date, function() {
//           $('#event_date_range').val(moment(start).format("MM/DD/YYYY HH:mm") + ' - ' + moment(end).format("MM/DD/YYYY HH:mm"))
//           date_range_picker();
//           $('.start_hidden').val(moment(start).format('YYYY-MM-DD HH:mm'));
//           $('.end_hidden').val(moment(end).format('YYYY-MM-DD HH:mm'));
//           unlock_calendar();
//           load_upload();
//           init_select();
//         });

//         calendar.fullCalendar('unselect');

//       },

//       eventDrop: function(event, delta, revertFunc) {
//         console.log(event)
//         event_data = { 
//           event: {
//             id: event.id,
//             start: event.start.format(),
//             end: event.end.format(),
//             move: true
//           }
//         };
//         $.ajax({
//             url: event.update_url,
//             data: event_data,
//             type: 'PATCH'
//         });
//       },
      
//       eventClick: function(event, jsEvent, view) {
//         lock_calendar();
//         $.getScript(event.edit_url, function() {
//           $('#event_date_range').val(moment(event.start).format("MM/DD/YYYY HH:mm") + ' - ' + moment(event.end).format("MM/DD/YYYY HH:mm"))
//           date_range_picker();
//           $('.start_hidden').val(moment(event.start).format('YYYY-MM-DD HH:mm'));
//           $('.end_hidden').val(moment(event.end).format('YYYY-MM-DD HH:mm'));
//           if($("#details .bs-callout").length == 0){
//             $("#label-no-details").show();
//           }else{
//             $("#label-no-details").hide();
//           }
//           unlock_calendar();
//           load_upload();
//           init_select();
//         });
//       }
//     });
//     // calendar.removeClass("empty");
//   })  
  
// };

var calendarEl = document.getElementById('calendar');
var url_events = '/medical_appointments.json?patient=1';

if (calendarEl){
  var calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'listDay,dayGridMonth,timeGridWeek,timeGridDay'
    },
    defaultDate: new Date(),
    navLinks: true, // can click day/week names to navigate views
    selectable: true,
    selectHelper: true,
    editable: false,
    eventLimit: true, // allow "more" link when too many events
    droppable: false,
    locale: 'es',
    eventRender:function (event, element){
      
      
    },
    select: function(arg) {
      lock_calendar();
      start_date = arg.start;
      end_date = arg.end;
      $("#new_medical_appointment_button").attr("href", '/medical_appointments/new?start='+start_date+'&end='+end_date);
      $("#new_medical_appointment_button").click();
      calendar.unselect()
    },
    eventClick: function(info) {
      info.jsEvent.preventDefault(); // don't let the browser navigate
      console.log("dddd");
      lock_calendar();
      
      edit_url = "medical_appointments/" + info.event.id + "/edit";
      console.log(info.event.id);
      console.log(edit_url);
      $("#edit_medical_appointment_button").attr("href", edit_url);
      $("#edit_medical_appointment_button").click();
      calendar.unselect()
    },
    events: {
      url: '/medical_appointments.json',
      extraParams: function() { // a function that returns an object
        return {
          patient_id: $("#_medical_records_patient_id").val(),
          doctor_id: $("#_medical_records_doctor_id").val(),
          medical_type_id: $("#_medical_records_medical_type_id").val()
        };
      }
    }
  });

  calendar.render();
  remove_fliter_label();
}


function load_calendar(){
  console.log("Load calendar")
  if (calendar){
    calendar.refetchEvents();  
  }
  
}

$(".select_2").change(function() {
  remove_fliter_label();
  load_calendar();
});

function remove_fliter_label(){
  $("#filter_c").hide();
  $("#patient_filter_c").hide();
  $("#medical_type_filter_c").hide();
  $("#doctor_filter_c").hide();

  if ($("#_medical_records_patient_id").val()){
    $("#filter_c").show();
    $("#patient_filter_c").show();
  }

  if ($("#_medical_records_doctor_id").val()){
    $("#filter_c").show();
    $("#doctor_filter_c").show(); 
  }

  if ($("#_medical_records_medical_type_id").val()){
    $("#filter_c").show();
    $("#medical_type_filter_c").show();
  }
}

function remove_patient_filter(){
  $("#_medical_records_patient_id").prop("selectedIndex", 0);
  $("#_medical_records_patient_id").trigger('change');
  remove_fliter_label();
  return false;
}

function remove_status_filter(){
  $("#_medical_records_status_id").prop("selectedIndex", 0);
  $("#_medical_records_status_id").trigger('change');
  remove_fliter_label();
  return false;
}

function remove_medical_type_filter(){
  $("#_medical_records_medical_type_id").prop("selectedIndex", 0);
  $("#_medical_records_medical_type_id").trigger('change');
  remove_fliter_label();
  return false;
}

function remove_doctor_filter(){
  $("#_medical_records_doctor_id").prop("selectedIndex", 0);
  $("#_medical_records_doctor_id").trigger('change');
  remove_fliter_label();
  return false;
}

function remove_year_filter(){
  $("#_medical_records_year").prop("selectedIndex", 0);
  $("#_medical_records_year").trigger('change');
  remove_fliter_label();
  return false;
}

