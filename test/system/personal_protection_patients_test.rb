require "application_system_test_case"

class PersonalProtectionPatientsTest < ApplicationSystemTestCase
  setup do
    @personal_protection_patient = personal_protection_patients(:one)
  end

  test "visiting the index" do
    visit personal_protection_patients_url
    assert_selector "h1", text: "Personal Protection Patients"
  end

  test "creating a Personal protection patient" do
    visit personal_protection_patients_url
    click_on "New Personal Protection Patient"

    fill_in "Company", with: @personal_protection_patient.company_id
    fill_in "Date", with: @personal_protection_patient.date
    fill_in "Patient", with: @personal_protection_patient.patient_id
    fill_in "Personal protection", with: @personal_protection_patient.personal_protection_id
    fill_in "Signature", with: @personal_protection_patient.signature
    click_on "Create Personal protection patient"

    assert_text "Personal protection patient was successfully created"
    click_on "Back"
  end

  test "updating a Personal protection patient" do
    visit personal_protection_patients_url
    click_on "Edit", match: :first

    fill_in "Company", with: @personal_protection_patient.company_id
    fill_in "Date", with: @personal_protection_patient.date
    fill_in "Patient", with: @personal_protection_patient.patient_id
    fill_in "Personal protection", with: @personal_protection_patient.personal_protection_id
    fill_in "Signature", with: @personal_protection_patient.signature
    click_on "Update Personal protection patient"

    assert_text "Personal protection patient was successfully updated"
    click_on "Back"
  end

  test "destroying a Personal protection patient" do
    visit personal_protection_patients_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Personal protection patient was successfully destroyed"
  end
end
