require "application_system_test_case"

class PersonalProtectionEntriesTest < ApplicationSystemTestCase
  setup do
    @personal_protection_entry = personal_protection_entries(:one)
  end

  test "visiting the index" do
    visit personal_protection_entries_url
    assert_selector "h1", text: "Personal Protection Entries"
  end

  test "creating a Personal protection entry" do
    visit personal_protection_entries_url
    click_on "New Personal Protection Entry"

    fill_in "Company", with: @personal_protection_entry.company_id
    fill_in "Date", with: @personal_protection_entry.date
    fill_in "Description", with: @personal_protection_entry.description
    fill_in "Destination work center", with: @personal_protection_entry.destination_work_center_id
    fill_in "Origin work center", with: @personal_protection_entry.origin_work_center_id
    fill_in "Patient", with: @personal_protection_entry.patient_id
    fill_in "Signature", with: @personal_protection_entry.signature
    fill_in "User", with: @personal_protection_entry.user_id
    click_on "Create Personal protection entry"

    assert_text "Personal protection entry was successfully created"
    click_on "Back"
  end

  test "updating a Personal protection entry" do
    visit personal_protection_entries_url
    click_on "Edit", match: :first

    fill_in "Company", with: @personal_protection_entry.company_id
    fill_in "Date", with: @personal_protection_entry.date
    fill_in "Description", with: @personal_protection_entry.description
    fill_in "Destination work center", with: @personal_protection_entry.destination_work_center_id
    fill_in "Origin work center", with: @personal_protection_entry.origin_work_center_id
    fill_in "Patient", with: @personal_protection_entry.patient_id
    fill_in "Signature", with: @personal_protection_entry.signature
    fill_in "User", with: @personal_protection_entry.user_id
    click_on "Update Personal protection entry"

    assert_text "Personal protection entry was successfully updated"
    click_on "Back"
  end

  test "destroying a Personal protection entry" do
    visit personal_protection_entries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Personal protection entry was successfully destroyed"
  end
end
