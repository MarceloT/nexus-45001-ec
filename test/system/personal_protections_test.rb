require "application_system_test_case"

class PersonalProtectionsTest < ApplicationSystemTestCase
  setup do
    @personal_protection = personal_protections(:one)
  end

  test "visiting the index" do
    visit personal_protections_url
    assert_selector "h1", text: "Personal Protections"
  end

  test "creating a Personal protection" do
    visit personal_protections_url
    click_on "New Personal Protection"

    fill_in "Company", with: @personal_protection.company_id
    fill_in "Details", with: @personal_protection.details
    fill_in "Features", with: @personal_protection.features
    fill_in "Image", with: @personal_protection.image
    fill_in "Name", with: @personal_protection.name
    fill_in "Protection type", with: @personal_protection.protection_type_id
    fill_in "Stock", with: @personal_protection.stock
    click_on "Create Personal protection"

    assert_text "Personal protection was successfully created"
    click_on "Back"
  end

  test "updating a Personal protection" do
    visit personal_protections_url
    click_on "Edit", match: :first

    fill_in "Company", with: @personal_protection.company_id
    fill_in "Details", with: @personal_protection.details
    fill_in "Features", with: @personal_protection.features
    fill_in "Image", with: @personal_protection.image
    fill_in "Name", with: @personal_protection.name
    fill_in "Protection type", with: @personal_protection.protection_type_id
    fill_in "Stock", with: @personal_protection.stock
    click_on "Update Personal protection"

    assert_text "Personal protection was successfully updated"
    click_on "Back"
  end

  test "destroying a Personal protection" do
    visit personal_protections_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Personal protection was successfully destroyed"
  end
end
