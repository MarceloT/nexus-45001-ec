require "application_system_test_case"

class RisksTest < ApplicationSystemTestCase
  setup do
    @risk = risks(:one)
  end

  test "visiting the index" do
    visit risks_url
    assert_selector "h1", text: "Risks"
  end

  test "creating a Risk" do
    visit risks_url
    click_on "New Risk"

    fill_in "Code", with: @risk.code
    fill_in "Description", with: @risk.description
    fill_in "Name", with: @risk.name
    fill_in "Risk category", with: @risk.risk_category_id
    click_on "Create Risk"

    assert_text "Risk was successfully created"
    click_on "Back"
  end

  test "updating a Risk" do
    visit risks_url
    click_on "Edit", match: :first

    fill_in "Code", with: @risk.code
    fill_in "Description", with: @risk.description
    fill_in "Name", with: @risk.name
    fill_in "Risk category", with: @risk.risk_category_id
    click_on "Update Risk"

    assert_text "Risk was successfully updated"
    click_on "Back"
  end

  test "destroying a Risk" do
    visit risks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Risk was successfully destroyed"
  end
end
