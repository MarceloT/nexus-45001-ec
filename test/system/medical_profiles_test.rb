require "application_system_test_case"

class MedicalProfilesTest < ApplicationSystemTestCase
  setup do
    @medical_profile = medical_profiles(:one)
  end

  test "visiting the index" do
    visit medical_profiles_url
    assert_selector "h1", text: "Medical Profiles"
  end

  test "creating a Medical profile" do
    visit medical_profiles_url
    click_on "New Medical Profile"

    fill_in "Active", with: @medical_profile.active
    fill_in "Code", with: @medical_profile.code
    fill_in "Company", with: @medical_profile.company_id
    fill_in "Description", with: @medical_profile.description
    fill_in "Name", with: @medical_profile.name
    click_on "Create Medical profile"

    assert_text "Medical profile was successfully created"
    click_on "Back"
  end

  test "updating a Medical profile" do
    visit medical_profiles_url
    click_on "Edit", match: :first

    fill_in "Active", with: @medical_profile.active
    fill_in "Code", with: @medical_profile.code
    fill_in "Company", with: @medical_profile.company_id
    fill_in "Description", with: @medical_profile.description
    fill_in "Name", with: @medical_profile.name
    click_on "Update Medical profile"

    assert_text "Medical profile was successfully updated"
    click_on "Back"
  end

  test "destroying a Medical profile" do
    visit medical_profiles_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Medical profile was successfully destroyed"
  end
end
