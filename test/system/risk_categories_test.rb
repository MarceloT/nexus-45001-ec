require "application_system_test_case"

class RiskCategoriesTest < ApplicationSystemTestCase
  setup do
    @risk_category = risk_categories(:one)
  end

  test "visiting the index" do
    visit risk_categories_url
    assert_selector "h1", text: "Risk Categories"
  end

  test "creating a Risk category" do
    visit risk_categories_url
    click_on "New Risk Category"

    fill_in "Color", with: @risk_category.color
    fill_in "Description", with: @risk_category.description
    fill_in "Name", with: @risk_category.name
    click_on "Create Risk category"

    assert_text "Risk category was successfully created"
    click_on "Back"
  end

  test "updating a Risk category" do
    visit risk_categories_url
    click_on "Edit", match: :first

    fill_in "Color", with: @risk_category.color
    fill_in "Description", with: @risk_category.description
    fill_in "Name", with: @risk_category.name
    click_on "Update Risk category"

    assert_text "Risk category was successfully updated"
    click_on "Back"
  end

  test "destroying a Risk category" do
    visit risk_categories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Risk category was successfully destroyed"
  end
end
