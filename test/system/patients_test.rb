require "application_system_test_case"

class PatientsTest < ApplicationSystemTestCase
  setup do
    @patient = patients(:one)
  end

  test "visiting the index" do
    visit patients_url
    assert_selector "h1", text: "Patients"
  end

  test "creating a Patient" do
    visit patients_url
    click_on "New Patient"

    fill_in "Active", with: @patient.active
    fill_in "Area", with: @patient.area_id
    fill_in "Birth date", with: @patient.birth_date
    fill_in "Blood type", with: @patient.blood_type
    fill_in "Ci", with: @patient.ci
    fill_in "Ci type", with: @patient.ci_type
    fill_in "Civil status", with: @patient.civil_status
    fill_in "Company", with: @patient.company_id
    fill_in "Description", with: @patient.description
    fill_in "Direction", with: @patient.direction
    fill_in "Email", with: @patient.email
    fill_in "Gender", with: @patient.gender
    fill_in "Last name", with: @patient.last_name
    fill_in "Last name 1", with: @patient.last_name_1
    fill_in "Name", with: @patient.name
    fill_in "Name 1", with: @patient.name_1
    fill_in "Nationality", with: @patient.nationality
    fill_in "Phone 1", with: @patient.phone_1
    fill_in "Phone 2", with: @patient.phone_2
    fill_in "Photo", with: @patient.photo
    fill_in "Position", with: @patient.position_id
    fill_in "Profession", with: @patient.profession
    click_on "Create Patient"

    assert_text "Patient was successfully created"
    click_on "Back"
  end

  test "updating a Patient" do
    visit patients_url
    click_on "Edit", match: :first

    fill_in "Active", with: @patient.active
    fill_in "Area", with: @patient.area_id
    fill_in "Birth date", with: @patient.birth_date
    fill_in "Blood type", with: @patient.blood_type
    fill_in "Ci", with: @patient.ci
    fill_in "Ci type", with: @patient.ci_type
    fill_in "Civil status", with: @patient.civil_status
    fill_in "Company", with: @patient.company_id
    fill_in "Description", with: @patient.description
    fill_in "Direction", with: @patient.direction
    fill_in "Email", with: @patient.email
    fill_in "Gender", with: @patient.gender
    fill_in "Last name", with: @patient.last_name
    fill_in "Last name 1", with: @patient.last_name_1
    fill_in "Name", with: @patient.name
    fill_in "Name 1", with: @patient.name_1
    fill_in "Nationality", with: @patient.nationality
    fill_in "Phone 1", with: @patient.phone_1
    fill_in "Phone 2", with: @patient.phone_2
    fill_in "Photo", with: @patient.photo
    fill_in "Position", with: @patient.position_id
    fill_in "Profession", with: @patient.profession
    click_on "Update Patient"

    assert_text "Patient was successfully updated"
    click_on "Back"
  end

  test "destroying a Patient" do
    visit patients_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Patient was successfully destroyed"
  end
end
