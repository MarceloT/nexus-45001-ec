require "application_system_test_case"

class WorkCentersTest < ApplicationSystemTestCase
  setup do
    @work_center = work_centers(:one)
  end

  test "visiting the index" do
    visit work_centers_url
    assert_selector "h1", text: "Work Centers"
  end

  test "creating a Work center" do
    visit work_centers_url
    click_on "New Work Center"

    fill_in "Active", with: @work_center.active
    fill_in "Address", with: @work_center.address
    fill_in "Company", with: @work_center.company_id
    fill_in "Description", with: @work_center.description
    fill_in "Name", with: @work_center.name
    click_on "Create Work center"

    assert_text "Work center was successfully created"
    click_on "Back"
  end

  test "updating a Work center" do
    visit work_centers_url
    click_on "Edit", match: :first

    fill_in "Active", with: @work_center.active
    fill_in "Address", with: @work_center.address
    fill_in "Company", with: @work_center.company_id
    fill_in "Description", with: @work_center.description
    fill_in "Name", with: @work_center.name
    click_on "Update Work center"

    assert_text "Work center was successfully updated"
    click_on "Back"
  end

  test "destroying a Work center" do
    visit work_centers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Work center was successfully destroyed"
  end
end
