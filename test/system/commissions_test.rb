require "application_system_test_case"

class CommissionsTest < ApplicationSystemTestCase
  setup do
    @commission = commissions(:one)
  end

  test "visiting the index" do
    visit commissions_url
    assert_selector "h1", text: "Commissions"
  end

  test "creating a Commission" do
    visit commissions_url
    click_on "New Commission"

    fill_in "Active", with: @commission.active
    fill_in "Company", with: @commission.company_id
    fill_in "Date", with: @commission.date
    fill_in "Member 1", with: @commission.member_1
    fill_in "Member 10", with: @commission.member_10
    fill_in "Member 11", with: @commission.member_11
    fill_in "Member 12", with: @commission.member_12
    fill_in "Member 2", with: @commission.member_2
    fill_in "Member 3", with: @commission.member_3
    fill_in "Member 4", with: @commission.member_4
    fill_in "Member 5", with: @commission.member_5
    fill_in "Member 6", with: @commission.member_6
    fill_in "Member 7", with: @commission.member_7
    fill_in "Member 8", with: @commission.member_8
    fill_in "Member 9", with: @commission.member_9
    fill_in "Nro members", with: @commission.nro_members
    fill_in "Type commission", with: @commission.type_commission
    fill_in "Work center", with: @commission.work_center_id
    click_on "Create Commission"

    assert_text "Commission was successfully created"
    click_on "Back"
  end

  test "updating a Commission" do
    visit commissions_url
    click_on "Edit", match: :first

    fill_in "Active", with: @commission.active
    fill_in "Company", with: @commission.company_id
    fill_in "Date", with: @commission.date
    fill_in "Member 1", with: @commission.member_1
    fill_in "Member 10", with: @commission.member_10
    fill_in "Member 11", with: @commission.member_11
    fill_in "Member 12", with: @commission.member_12
    fill_in "Member 2", with: @commission.member_2
    fill_in "Member 3", with: @commission.member_3
    fill_in "Member 4", with: @commission.member_4
    fill_in "Member 5", with: @commission.member_5
    fill_in "Member 6", with: @commission.member_6
    fill_in "Member 7", with: @commission.member_7
    fill_in "Member 8", with: @commission.member_8
    fill_in "Member 9", with: @commission.member_9
    fill_in "Nro members", with: @commission.nro_members
    fill_in "Type commission", with: @commission.type_commission
    fill_in "Work center", with: @commission.work_center_id
    click_on "Update Commission"

    assert_text "Commission was successfully updated"
    click_on "Back"
  end

  test "destroying a Commission" do
    visit commissions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Commission was successfully destroyed"
  end
end
