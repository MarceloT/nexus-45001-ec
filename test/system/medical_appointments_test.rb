require "application_system_test_case"

class MedicalAppointmentsTest < ApplicationSystemTestCase
  setup do
    @medical_appointment = medical_appointments(:one)
  end

  test "visiting the index" do
    visit medical_appointments_url
    assert_selector "h1", text: "Medical Appointments"
  end

  test "creating a Medical appointment" do
    visit medical_appointments_url
    click_on "New Medical Appointment"

    fill_in "Date", with: @medical_appointment.date
    fill_in "Doctor", with: @medical_appointment.doctor_id
    fill_in "Duration", with: @medical_appointment.duration
    fill_in "Medical type", with: @medical_appointment.medical_type_id
    fill_in "Patient", with: @medical_appointment.patient_id
    fill_in "Reason", with: @medical_appointment.reason
    fill_in "Time", with: @medical_appointment.time
    click_on "Create Medical appointment"

    assert_text "Medical appointment was successfully created"
    click_on "Back"
  end

  test "updating a Medical appointment" do
    visit medical_appointments_url
    click_on "Edit", match: :first

    fill_in "Date", with: @medical_appointment.date
    fill_in "Doctor", with: @medical_appointment.doctor_id
    fill_in "Duration", with: @medical_appointment.duration
    fill_in "Medical type", with: @medical_appointment.medical_type_id
    fill_in "Patient", with: @medical_appointment.patient_id
    fill_in "Reason", with: @medical_appointment.reason
    fill_in "Time", with: @medical_appointment.time
    click_on "Update Medical appointment"

    assert_text "Medical appointment was successfully updated"
    click_on "Back"
  end

  test "destroying a Medical appointment" do
    visit medical_appointments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Medical appointment was successfully destroyed"
  end
end
