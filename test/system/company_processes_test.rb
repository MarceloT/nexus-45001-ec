require "application_system_test_case"

class CompanyProcessesTest < ApplicationSystemTestCase
  setup do
    @company_process = company_processes(:one)
  end

  test "visiting the index" do
    visit company_processes_url
    assert_selector "h1", text: "Company Processes"
  end

  test "creating a Company process" do
    visit company_processes_url
    click_on "New Company Process"

    fill_in "Company", with: @company_process.company_id
    fill_in "Description", with: @company_process.description
    fill_in "Level", with: @company_process.level
    fill_in "Name", with: @company_process.name
    fill_in "Owner", with: @company_process.owner_id
    fill_in "Parent", with: @company_process.parent_id
    fill_in "Process in", with: @company_process.process_in_id
    fill_in "Process out", with: @company_process.process_out_id
    fill_in "Process type", with: @company_process.process_type
    click_on "Create Company process"

    assert_text "Company process was successfully created"
    click_on "Back"
  end

  test "updating a Company process" do
    visit company_processes_url
    click_on "Edit", match: :first

    fill_in "Company", with: @company_process.company_id
    fill_in "Description", with: @company_process.description
    fill_in "Level", with: @company_process.level
    fill_in "Name", with: @company_process.name
    fill_in "Owner", with: @company_process.owner_id
    fill_in "Parent", with: @company_process.parent_id
    fill_in "Process in", with: @company_process.process_in_id
    fill_in "Process out", with: @company_process.process_out_id
    fill_in "Process type", with: @company_process.process_type
    click_on "Update Company process"

    assert_text "Company process was successfully updated"
    click_on "Back"
  end

  test "destroying a Company process" do
    visit company_processes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Company process was successfully destroyed"
  end
end
