require "application_system_test_case"

class RiskEvaluationsTest < ApplicationSystemTestCase
  setup do
    @risk_evaluation = risk_evaluations(:one)
  end

  test "visiting the index" do
    visit risk_evaluations_url
    assert_selector "h1", text: "Risk Evaluations"
  end

  test "creating a Risk evaluation" do
    visit risk_evaluations_url
    click_on "New Risk Evaluation"

    fill_in "Area", with: @risk_evaluation.area_id
    fill_in "Company evaluation", with: @risk_evaluation.company_evaluation
    fill_in "Company", with: @risk_evaluation.company_id
    fill_in "Date", with: @risk_evaluation.date
    fill_in "Description", with: @risk_evaluation.description
    fill_in "Evaluation manager", with: @risk_evaluation.evaluation_manager
    fill_in "Name", with: @risk_evaluation.name
    fill_in "Occupational manager", with: @risk_evaluation.occupational_manager
    fill_in "Position", with: @risk_evaluation.position_id
    fill_in "Process", with: @risk_evaluation.process_id
    fill_in "Sub process", with: @risk_evaluation.sub_process_id
    click_on "Create Risk evaluation"

    assert_text "Risk evaluation was successfully created"
    click_on "Back"
  end

  test "updating a Risk evaluation" do
    visit risk_evaluations_url
    click_on "Edit", match: :first

    fill_in "Area", with: @risk_evaluation.area_id
    fill_in "Company evaluation", with: @risk_evaluation.company_evaluation
    fill_in "Company", with: @risk_evaluation.company_id
    fill_in "Date", with: @risk_evaluation.date
    fill_in "Description", with: @risk_evaluation.description
    fill_in "Evaluation manager", with: @risk_evaluation.evaluation_manager
    fill_in "Name", with: @risk_evaluation.name
    fill_in "Occupational manager", with: @risk_evaluation.occupational_manager
    fill_in "Position", with: @risk_evaluation.position_id
    fill_in "Process", with: @risk_evaluation.process_id
    fill_in "Sub process", with: @risk_evaluation.sub_process_id
    click_on "Update Risk evaluation"

    assert_text "Risk evaluation was successfully updated"
    click_on "Back"
  end

  test "destroying a Risk evaluation" do
    visit risk_evaluations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Risk evaluation was successfully destroyed"
  end
end
