require "application_system_test_case"

class MedicalServicesTest < ApplicationSystemTestCase
  setup do
    @medical_service = medical_services(:one)
  end

  test "visiting the index" do
    visit medical_services_url
    assert_selector "h1", text: "Medical Services"
  end

  test "creating a Medical service" do
    visit medical_services_url
    click_on "New Medical Service"

    fill_in "Active", with: @medical_service.active
    fill_in "Company id.integer", with: @medical_service.company_id
    fill_in "Exam", with: @medical_service.exam_id
    fill_in "Name", with: @medical_service.name
    fill_in "Simple", with: @medical_service.simple
    click_on "Create Medical service"

    assert_text "Medical service was successfully created"
    click_on "Back"
  end

  test "updating a Medical service" do
    visit medical_services_url
    click_on "Edit", match: :first

    fill_in "Active", with: @medical_service.active
    fill_in "Company id.integer", with: @medical_service.company_id
    fill_in "Exam", with: @medical_service.exam_id
    fill_in "Name", with: @medical_service.name
    fill_in "Simple", with: @medical_service.simple
    click_on "Update Medical service"

    assert_text "Medical service was successfully updated"
    click_on "Back"
  end

  test "destroying a Medical service" do
    visit medical_services_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Medical service was successfully destroyed"
  end
end
