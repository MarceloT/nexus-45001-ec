require "application_system_test_case"

class MedicalRecordsTest < ApplicationSystemTestCase
  setup do
    @medical_record = medical_records(:one)
  end

  test "visiting the index" do
    visit medical_records_url
    assert_selector "h1", text: "Medical Records"
  end

  test "creating a Medical record" do
    visit medical_records_url
    click_on "New Medical Record"

    fill_in "Aptitude", with: @medical_record.aptitude
    fill_in "Aptitude limit", with: @medical_record.aptitude_limit
    fill_in "Aptitude obs", with: @medical_record.aptitude_obs
    fill_in "Current disease", with: @medical_record.current_disease
    fill_in "Date", with: @medical_record.date
    fill_in "Doctor", with: @medical_record.doctor_id
    fill_in "Doctor signature", with: @medical_record.doctor_signature
    fill_in "Patient", with: @medical_record.patient_id
    fill_in "Reason", with: @medical_record.reason
    fill_in "Recommendations", with: @medical_record.recommendations
    click_on "Create Medical record"

    assert_text "Medical record was successfully created"
    click_on "Back"
  end

  test "updating a Medical record" do
    visit medical_records_url
    click_on "Edit", match: :first

    fill_in "Aptitude", with: @medical_record.aptitude
    fill_in "Aptitude limit", with: @medical_record.aptitude_limit
    fill_in "Aptitude obs", with: @medical_record.aptitude_obs
    fill_in "Current disease", with: @medical_record.current_disease
    fill_in "Date", with: @medical_record.date
    fill_in "Doctor", with: @medical_record.doctor_id
    fill_in "Doctor signature", with: @medical_record.doctor_signature
    fill_in "Patient", with: @medical_record.patient_id
    fill_in "Reason", with: @medical_record.reason
    fill_in "Recommendations", with: @medical_record.recommendations
    click_on "Update Medical record"

    assert_text "Medical record was successfully updated"
    click_on "Back"
  end

  test "destroying a Medical record" do
    visit medical_records_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Medical record was successfully destroyed"
  end
end
