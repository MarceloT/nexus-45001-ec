require "application_system_test_case"

class RiskEvaluationRisksTest < ApplicationSystemTestCase
  setup do
    @risk_evaluation_risk = risk_evaluation_risks(:one)
  end

  test "visiting the index" do
    visit risk_evaluation_risks_url
    assert_selector "h1", text: "Risk Evaluation Risks"
  end

  test "creating a Risk evaluation risk" do
    visit risk_evaluation_risks_url
    click_on "New Risk Evaluation Risk"

    click_on "Create Risk evaluation risk"

    assert_text "Risk evaluation risk was successfully created"
    click_on "Back"
  end

  test "updating a Risk evaluation risk" do
    visit risk_evaluation_risks_url
    click_on "Edit", match: :first

    click_on "Update Risk evaluation risk"

    assert_text "Risk evaluation risk was successfully updated"
    click_on "Back"
  end

  test "destroying a Risk evaluation risk" do
    visit risk_evaluation_risks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Risk evaluation risk was successfully destroyed"
  end
end
