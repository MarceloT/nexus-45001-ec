require "application_system_test_case"

class MedicalCertificatesTest < ApplicationSystemTestCase
  setup do
    @medical_certificate = medical_certificates(:one)
  end

  test "visiting the index" do
    visit medical_certificates_url
    assert_selector "h1", text: "Medical Certificates"
  end

  test "creating a Medical certificate" do
    visit medical_certificates_url
    click_on "New Medical Certificate"

    fill_in "Created by", with: @medical_certificate.created_by_id
    fill_in "Date", with: @medical_certificate.date
    fill_in "Days", with: @medical_certificate.days
    fill_in "Diagnostic description", with: @medical_certificate.diagnostic_description
    fill_in "Document", with: @medical_certificate.document_id
    fill_in "Ends date", with: @medical_certificate.ends_date
    fill_in "Medical record", with: @medical_certificate.medical_record_id
    fill_in "Patient", with: @medical_certificate.patient_id
    fill_in "Reason", with: @medical_certificate.reason
    fill_in "Starts date", with: @medical_certificate.starts_date
    fill_in "Type certificate", with: @medical_certificate.type_certificate
    click_on "Create Medical certificate"

    assert_text "Medical certificate was successfully created"
    click_on "Back"
  end

  test "updating a Medical certificate" do
    visit medical_certificates_url
    click_on "Edit", match: :first

    fill_in "Created by", with: @medical_certificate.created_by_id
    fill_in "Date", with: @medical_certificate.date
    fill_in "Days", with: @medical_certificate.days
    fill_in "Diagnostic description", with: @medical_certificate.diagnostic_description
    fill_in "Document", with: @medical_certificate.document_id
    fill_in "Ends date", with: @medical_certificate.ends_date
    fill_in "Medical record", with: @medical_certificate.medical_record_id
    fill_in "Patient", with: @medical_certificate.patient_id
    fill_in "Reason", with: @medical_certificate.reason
    fill_in "Starts date", with: @medical_certificate.starts_date
    fill_in "Type certificate", with: @medical_certificate.type_certificate
    click_on "Update Medical certificate"

    assert_text "Medical certificate was successfully updated"
    click_on "Back"
  end

  test "destroying a Medical certificate" do
    visit medical_certificates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Medical certificate was successfully destroyed"
  end
end
