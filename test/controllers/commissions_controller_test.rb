require 'test_helper'

class CommissionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @commission = commissions(:one)
  end

  test "should get index" do
    get commissions_url
    assert_response :success
  end

  test "should get new" do
    get new_commission_url
    assert_response :success
  end

  test "should create commission" do
    assert_difference('Commission.count') do
      post commissions_url, params: { commission: { active: @commission.active, company_id: @commission.company_id, date: @commission.date, member_1: @commission.member_1, member_10: @commission.member_10, member_11: @commission.member_11, member_12: @commission.member_12, member_2: @commission.member_2, member_3: @commission.member_3, member_4: @commission.member_4, member_5: @commission.member_5, member_6: @commission.member_6, member_7: @commission.member_7, member_8: @commission.member_8, member_9: @commission.member_9, nro_members: @commission.nro_members, type_commission: @commission.type_commission, work_center_id: @commission.work_center_id } }
    end

    assert_redirected_to commission_url(Commission.last)
  end

  test "should show commission" do
    get commission_url(@commission)
    assert_response :success
  end

  test "should get edit" do
    get edit_commission_url(@commission)
    assert_response :success
  end

  test "should update commission" do
    patch commission_url(@commission), params: { commission: { active: @commission.active, company_id: @commission.company_id, date: @commission.date, member_1: @commission.member_1, member_10: @commission.member_10, member_11: @commission.member_11, member_12: @commission.member_12, member_2: @commission.member_2, member_3: @commission.member_3, member_4: @commission.member_4, member_5: @commission.member_5, member_6: @commission.member_6, member_7: @commission.member_7, member_8: @commission.member_8, member_9: @commission.member_9, nro_members: @commission.nro_members, type_commission: @commission.type_commission, work_center_id: @commission.work_center_id } }
    assert_redirected_to commission_url(@commission)
  end

  test "should destroy commission" do
    assert_difference('Commission.count', -1) do
      delete commission_url(@commission)
    end

    assert_redirected_to commissions_url
  end
end
