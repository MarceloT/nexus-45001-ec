require 'test_helper'

class CompanyProcessesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @company_process = company_processes(:one)
  end

  test "should get index" do
    get company_processes_url
    assert_response :success
  end

  test "should get new" do
    get new_company_process_url
    assert_response :success
  end

  test "should create company_process" do
    assert_difference('CompanyProcess.count') do
      post company_processes_url, params: { company_process: { company_id: @company_process.company_id, description: @company_process.description, level: @company_process.level, name: @company_process.name, owner_id: @company_process.owner_id, parent_id: @company_process.parent_id, process_in_id: @company_process.process_in_id, process_out_id: @company_process.process_out_id, process_type: @company_process.process_type } }
    end

    assert_redirected_to company_process_url(CompanyProcess.last)
  end

  test "should show company_process" do
    get company_process_url(@company_process)
    assert_response :success
  end

  test "should get edit" do
    get edit_company_process_url(@company_process)
    assert_response :success
  end

  test "should update company_process" do
    patch company_process_url(@company_process), params: { company_process: { company_id: @company_process.company_id, description: @company_process.description, level: @company_process.level, name: @company_process.name, owner_id: @company_process.owner_id, parent_id: @company_process.parent_id, process_in_id: @company_process.process_in_id, process_out_id: @company_process.process_out_id, process_type: @company_process.process_type } }
    assert_redirected_to company_process_url(@company_process)
  end

  test "should destroy company_process" do
    assert_difference('CompanyProcess.count', -1) do
      delete company_process_url(@company_process)
    end

    assert_redirected_to company_processes_url
  end
end
