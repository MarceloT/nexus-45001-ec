require 'test_helper'

class MedicalProfilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @medical_profile = medical_profiles(:one)
  end

  test "should get index" do
    get medical_profiles_url
    assert_response :success
  end

  test "should get new" do
    get new_medical_profile_url
    assert_response :success
  end

  test "should create medical_profile" do
    assert_difference('MedicalProfile.count') do
      post medical_profiles_url, params: { medical_profile: { active: @medical_profile.active, code: @medical_profile.code, company_id: @medical_profile.company_id, description: @medical_profile.description, name: @medical_profile.name } }
    end

    assert_redirected_to medical_profile_url(MedicalProfile.last)
  end

  test "should show medical_profile" do
    get medical_profile_url(@medical_profile)
    assert_response :success
  end

  test "should get edit" do
    get edit_medical_profile_url(@medical_profile)
    assert_response :success
  end

  test "should update medical_profile" do
    patch medical_profile_url(@medical_profile), params: { medical_profile: { active: @medical_profile.active, code: @medical_profile.code, company_id: @medical_profile.company_id, description: @medical_profile.description, name: @medical_profile.name } }
    assert_redirected_to medical_profile_url(@medical_profile)
  end

  test "should destroy medical_profile" do
    assert_difference('MedicalProfile.count', -1) do
      delete medical_profile_url(@medical_profile)
    end

    assert_redirected_to medical_profiles_url
  end
end
