require 'test_helper'

class PersonalProtectionEntriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @personal_protection_entry = personal_protection_entries(:one)
  end

  test "should get index" do
    get personal_protection_entries_url
    assert_response :success
  end

  test "should get new" do
    get new_personal_protection_entry_url
    assert_response :success
  end

  test "should create personal_protection_entry" do
    assert_difference('PersonalProtectionEntry.count') do
      post personal_protection_entries_url, params: { personal_protection_entry: { company_id: @personal_protection_entry.company_id, date: @personal_protection_entry.date, description: @personal_protection_entry.description, destination_work_center_id: @personal_protection_entry.destination_work_center_id, origin_work_center_id: @personal_protection_entry.origin_work_center_id, patient_id: @personal_protection_entry.patient_id, signature: @personal_protection_entry.signature, user_id: @personal_protection_entry.user_id } }
    end

    assert_redirected_to personal_protection_entry_url(PersonalProtectionEntry.last)
  end

  test "should show personal_protection_entry" do
    get personal_protection_entry_url(@personal_protection_entry)
    assert_response :success
  end

  test "should get edit" do
    get edit_personal_protection_entry_url(@personal_protection_entry)
    assert_response :success
  end

  test "should update personal_protection_entry" do
    patch personal_protection_entry_url(@personal_protection_entry), params: { personal_protection_entry: { company_id: @personal_protection_entry.company_id, date: @personal_protection_entry.date, description: @personal_protection_entry.description, destination_work_center_id: @personal_protection_entry.destination_work_center_id, origin_work_center_id: @personal_protection_entry.origin_work_center_id, patient_id: @personal_protection_entry.patient_id, signature: @personal_protection_entry.signature, user_id: @personal_protection_entry.user_id } }
    assert_redirected_to personal_protection_entry_url(@personal_protection_entry)
  end

  test "should destroy personal_protection_entry" do
    assert_difference('PersonalProtectionEntry.count', -1) do
      delete personal_protection_entry_url(@personal_protection_entry)
    end

    assert_redirected_to personal_protection_entries_url
  end
end
