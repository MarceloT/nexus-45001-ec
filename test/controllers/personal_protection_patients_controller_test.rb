require 'test_helper'

class PersonalProtectionPatientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @personal_protection_patient = personal_protection_patients(:one)
  end

  test "should get index" do
    get personal_protection_patients_url
    assert_response :success
  end

  test "should get new" do
    get new_personal_protection_patient_url
    assert_response :success
  end

  test "should create personal_protection_patient" do
    assert_difference('PersonalProtectionPatient.count') do
      post personal_protection_patients_url, params: { personal_protection_patient: { company_id: @personal_protection_patient.company_id, date: @personal_protection_patient.date, patient_id: @personal_protection_patient.patient_id, personal_protection_id: @personal_protection_patient.personal_protection_id, signature: @personal_protection_patient.signature } }
    end

    assert_redirected_to personal_protection_patient_url(PersonalProtectionPatient.last)
  end

  test "should show personal_protection_patient" do
    get personal_protection_patient_url(@personal_protection_patient)
    assert_response :success
  end

  test "should get edit" do
    get edit_personal_protection_patient_url(@personal_protection_patient)
    assert_response :success
  end

  test "should update personal_protection_patient" do
    patch personal_protection_patient_url(@personal_protection_patient), params: { personal_protection_patient: { company_id: @personal_protection_patient.company_id, date: @personal_protection_patient.date, patient_id: @personal_protection_patient.patient_id, personal_protection_id: @personal_protection_patient.personal_protection_id, signature: @personal_protection_patient.signature } }
    assert_redirected_to personal_protection_patient_url(@personal_protection_patient)
  end

  test "should destroy personal_protection_patient" do
    assert_difference('PersonalProtectionPatient.count', -1) do
      delete personal_protection_patient_url(@personal_protection_patient)
    end

    assert_redirected_to personal_protection_patients_url
  end
end
