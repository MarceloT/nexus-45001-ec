require 'test_helper'

class WorkCentersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @work_center = work_centers(:one)
  end

  test "should get index" do
    get work_centers_url
    assert_response :success
  end

  test "should get new" do
    get new_work_center_url
    assert_response :success
  end

  test "should create work_center" do
    assert_difference('WorkCenter.count') do
      post work_centers_url, params: { work_center: { active: @work_center.active, address: @work_center.address, company_id: @work_center.company_id, description: @work_center.description, name: @work_center.name } }
    end

    assert_redirected_to work_center_url(WorkCenter.last)
  end

  test "should show work_center" do
    get work_center_url(@work_center)
    assert_response :success
  end

  test "should get edit" do
    get edit_work_center_url(@work_center)
    assert_response :success
  end

  test "should update work_center" do
    patch work_center_url(@work_center), params: { work_center: { active: @work_center.active, address: @work_center.address, company_id: @work_center.company_id, description: @work_center.description, name: @work_center.name } }
    assert_redirected_to work_center_url(@work_center)
  end

  test "should destroy work_center" do
    assert_difference('WorkCenter.count', -1) do
      delete work_center_url(@work_center)
    end

    assert_redirected_to work_centers_url
  end
end
