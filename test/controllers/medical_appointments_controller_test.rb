require 'test_helper'

class MedicalAppointmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @medical_appointment = medical_appointments(:one)
  end

  test "should get index" do
    get medical_appointments_url
    assert_response :success
  end

  test "should get new" do
    get new_medical_appointment_url
    assert_response :success
  end

  test "should create medical_appointment" do
    assert_difference('MedicalAppointment.count') do
      post medical_appointments_url, params: { medical_appointment: { date: @medical_appointment.date, doctor_id: @medical_appointment.doctor_id, duration: @medical_appointment.duration, medical_type_id: @medical_appointment.medical_type_id, patient_id: @medical_appointment.patient_id, reason: @medical_appointment.reason, time: @medical_appointment.time } }
    end

    assert_redirected_to medical_appointment_url(MedicalAppointment.last)
  end

  test "should show medical_appointment" do
    get medical_appointment_url(@medical_appointment)
    assert_response :success
  end

  test "should get edit" do
    get edit_medical_appointment_url(@medical_appointment)
    assert_response :success
  end

  test "should update medical_appointment" do
    patch medical_appointment_url(@medical_appointment), params: { medical_appointment: { date: @medical_appointment.date, doctor_id: @medical_appointment.doctor_id, duration: @medical_appointment.duration, medical_type_id: @medical_appointment.medical_type_id, patient_id: @medical_appointment.patient_id, reason: @medical_appointment.reason, time: @medical_appointment.time } }
    assert_redirected_to medical_appointment_url(@medical_appointment)
  end

  test "should destroy medical_appointment" do
    assert_difference('MedicalAppointment.count', -1) do
      delete medical_appointment_url(@medical_appointment)
    end

    assert_redirected_to medical_appointments_url
  end
end
