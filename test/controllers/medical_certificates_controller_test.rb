require 'test_helper'

class MedicalCertificatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @medical_certificate = medical_certificates(:one)
  end

  test "should get index" do
    get medical_certificates_url
    assert_response :success
  end

  test "should get new" do
    get new_medical_certificate_url
    assert_response :success
  end

  test "should create medical_certificate" do
    assert_difference('MedicalCertificate.count') do
      post medical_certificates_url, params: { medical_certificate: { created_by_id: @medical_certificate.created_by_id, date: @medical_certificate.date, days: @medical_certificate.days, diagnostic_description: @medical_certificate.diagnostic_description, document_id: @medical_certificate.document_id, ends_date: @medical_certificate.ends_date, medical_record_id: @medical_certificate.medical_record_id, patient_id: @medical_certificate.patient_id, reason: @medical_certificate.reason, starts_date: @medical_certificate.starts_date, type_certificate: @medical_certificate.type_certificate } }
    end

    assert_redirected_to medical_certificate_url(MedicalCertificate.last)
  end

  test "should show medical_certificate" do
    get medical_certificate_url(@medical_certificate)
    assert_response :success
  end

  test "should get edit" do
    get edit_medical_certificate_url(@medical_certificate)
    assert_response :success
  end

  test "should update medical_certificate" do
    patch medical_certificate_url(@medical_certificate), params: { medical_certificate: { created_by_id: @medical_certificate.created_by_id, date: @medical_certificate.date, days: @medical_certificate.days, diagnostic_description: @medical_certificate.diagnostic_description, document_id: @medical_certificate.document_id, ends_date: @medical_certificate.ends_date, medical_record_id: @medical_certificate.medical_record_id, patient_id: @medical_certificate.patient_id, reason: @medical_certificate.reason, starts_date: @medical_certificate.starts_date, type_certificate: @medical_certificate.type_certificate } }
    assert_redirected_to medical_certificate_url(@medical_certificate)
  end

  test "should destroy medical_certificate" do
    assert_difference('MedicalCertificate.count', -1) do
      delete medical_certificate_url(@medical_certificate)
    end

    assert_redirected_to medical_certificates_url
  end
end
