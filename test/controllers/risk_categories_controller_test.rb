require 'test_helper'

class RiskCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @risk_category = risk_categories(:one)
  end

  test "should get index" do
    get risk_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_risk_category_url
    assert_response :success
  end

  test "should create risk_category" do
    assert_difference('RiskCategory.count') do
      post risk_categories_url, params: { risk_category: { color: @risk_category.color, description: @risk_category.description, name: @risk_category.name } }
    end

    assert_redirected_to risk_category_url(RiskCategory.last)
  end

  test "should show risk_category" do
    get risk_category_url(@risk_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_risk_category_url(@risk_category)
    assert_response :success
  end

  test "should update risk_category" do
    patch risk_category_url(@risk_category), params: { risk_category: { color: @risk_category.color, description: @risk_category.description, name: @risk_category.name } }
    assert_redirected_to risk_category_url(@risk_category)
  end

  test "should destroy risk_category" do
    assert_difference('RiskCategory.count', -1) do
      delete risk_category_url(@risk_category)
    end

    assert_redirected_to risk_categories_url
  end
end
