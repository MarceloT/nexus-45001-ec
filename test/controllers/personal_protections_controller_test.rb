require 'test_helper'

class PersonalProtectionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @personal_protection = personal_protections(:one)
  end

  test "should get index" do
    get personal_protections_url
    assert_response :success
  end

  test "should get new" do
    get new_personal_protection_url
    assert_response :success
  end

  test "should create personal_protection" do
    assert_difference('PersonalProtection.count') do
      post personal_protections_url, params: { personal_protection: { company_id: @personal_protection.company_id, details: @personal_protection.details, features: @personal_protection.features, image: @personal_protection.image, name: @personal_protection.name, protection_type_id: @personal_protection.protection_type_id, stock: @personal_protection.stock } }
    end

    assert_redirected_to personal_protection_url(PersonalProtection.last)
  end

  test "should show personal_protection" do
    get personal_protection_url(@personal_protection)
    assert_response :success
  end

  test "should get edit" do
    get edit_personal_protection_url(@personal_protection)
    assert_response :success
  end

  test "should update personal_protection" do
    patch personal_protection_url(@personal_protection), params: { personal_protection: { company_id: @personal_protection.company_id, details: @personal_protection.details, features: @personal_protection.features, image: @personal_protection.image, name: @personal_protection.name, protection_type_id: @personal_protection.protection_type_id, stock: @personal_protection.stock } }
    assert_redirected_to personal_protection_url(@personal_protection)
  end

  test "should destroy personal_protection" do
    assert_difference('PersonalProtection.count', -1) do
      delete personal_protection_url(@personal_protection)
    end

    assert_redirected_to personal_protections_url
  end
end
