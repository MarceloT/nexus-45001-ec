require 'test_helper'

class RiskEvaluationRisksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @risk_evaluation_risk = risk_evaluation_risks(:one)
  end

  test "should get index" do
    get risk_evaluation_risks_url
    assert_response :success
  end

  test "should get new" do
    get new_risk_evaluation_risk_url
    assert_response :success
  end

  test "should create risk_evaluation_risk" do
    assert_difference('RiskEvaluationRisk.count') do
      post risk_evaluation_risks_url, params: { risk_evaluation_risk: {  } }
    end

    assert_redirected_to risk_evaluation_risk_url(RiskEvaluationRisk.last)
  end

  test "should show risk_evaluation_risk" do
    get risk_evaluation_risk_url(@risk_evaluation_risk)
    assert_response :success
  end

  test "should get edit" do
    get edit_risk_evaluation_risk_url(@risk_evaluation_risk)
    assert_response :success
  end

  test "should update risk_evaluation_risk" do
    patch risk_evaluation_risk_url(@risk_evaluation_risk), params: { risk_evaluation_risk: {  } }
    assert_redirected_to risk_evaluation_risk_url(@risk_evaluation_risk)
  end

  test "should destroy risk_evaluation_risk" do
    assert_difference('RiskEvaluationRisk.count', -1) do
      delete risk_evaluation_risk_url(@risk_evaluation_risk)
    end

    assert_redirected_to risk_evaluation_risks_url
  end
end
