require 'test_helper'

class RiskEvaluationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @risk_evaluation = risk_evaluations(:one)
  end

  test "should get index" do
    get risk_evaluations_url
    assert_response :success
  end

  test "should get new" do
    get new_risk_evaluation_url
    assert_response :success
  end

  test "should create risk_evaluation" do
    assert_difference('RiskEvaluation.count') do
      post risk_evaluations_url, params: { risk_evaluation: { area_id: @risk_evaluation.area_id, company_evaluation: @risk_evaluation.company_evaluation, company_id: @risk_evaluation.company_id, date: @risk_evaluation.date, description: @risk_evaluation.description, evaluation_manager: @risk_evaluation.evaluation_manager, name: @risk_evaluation.name, occupational_manager: @risk_evaluation.occupational_manager, position_id: @risk_evaluation.position_id, process_id: @risk_evaluation.process_id, sub_process_id: @risk_evaluation.sub_process_id } }
    end

    assert_redirected_to risk_evaluation_url(RiskEvaluation.last)
  end

  test "should show risk_evaluation" do
    get risk_evaluation_url(@risk_evaluation)
    assert_response :success
  end

  test "should get edit" do
    get edit_risk_evaluation_url(@risk_evaluation)
    assert_response :success
  end

  test "should update risk_evaluation" do
    patch risk_evaluation_url(@risk_evaluation), params: { risk_evaluation: { area_id: @risk_evaluation.area_id, company_evaluation: @risk_evaluation.company_evaluation, company_id: @risk_evaluation.company_id, date: @risk_evaluation.date, description: @risk_evaluation.description, evaluation_manager: @risk_evaluation.evaluation_manager, name: @risk_evaluation.name, occupational_manager: @risk_evaluation.occupational_manager, position_id: @risk_evaluation.position_id, process_id: @risk_evaluation.process_id, sub_process_id: @risk_evaluation.sub_process_id } }
    assert_redirected_to risk_evaluation_url(@risk_evaluation)
  end

  test "should destroy risk_evaluation" do
    assert_difference('RiskEvaluation.count', -1) do
      delete risk_evaluation_url(@risk_evaluation)
    end

    assert_redirected_to risk_evaluations_url
  end
end
