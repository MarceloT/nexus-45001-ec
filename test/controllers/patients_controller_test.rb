require 'test_helper'

class PatientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @patient = patients(:one)
  end

  test "should get index" do
    get patients_url
    assert_response :success
  end

  test "should get new" do
    get new_patient_url
    assert_response :success
  end

  test "should create patient" do
    assert_difference('Patient.count') do
      post patients_url, params: { patient: { active: @patient.active, area_id: @patient.area_id, birth_date: @patient.birth_date, blood_type: @patient.blood_type, ci: @patient.ci, ci_type: @patient.ci_type, civil_status: @patient.civil_status, company_id: @patient.company_id, description: @patient.description, direction: @patient.direction, email: @patient.email, gender: @patient.gender, last_name: @patient.last_name, last_name_1: @patient.last_name_1, name: @patient.name, name_1: @patient.name_1, nationality: @patient.nationality, phone_1: @patient.phone_1, phone_2: @patient.phone_2, photo: @patient.photo, position_id: @patient.position_id, profession: @patient.profession } }
    end

    assert_redirected_to patient_url(Patient.last)
  end

  test "should show patient" do
    get patient_url(@patient)
    assert_response :success
  end

  test "should get edit" do
    get edit_patient_url(@patient)
    assert_response :success
  end

  test "should update patient" do
    patch patient_url(@patient), params: { patient: { active: @patient.active, area_id: @patient.area_id, birth_date: @patient.birth_date, blood_type: @patient.blood_type, ci: @patient.ci, ci_type: @patient.ci_type, civil_status: @patient.civil_status, company_id: @patient.company_id, description: @patient.description, direction: @patient.direction, email: @patient.email, gender: @patient.gender, last_name: @patient.last_name, last_name_1: @patient.last_name_1, name: @patient.name, name_1: @patient.name_1, nationality: @patient.nationality, phone_1: @patient.phone_1, phone_2: @patient.phone_2, photo: @patient.photo, position_id: @patient.position_id, profession: @patient.profession } }
    assert_redirected_to patient_url(@patient)
  end

  test "should destroy patient" do
    assert_difference('Patient.count', -1) do
      delete patient_url(@patient)
    end

    assert_redirected_to patients_url
  end
end
