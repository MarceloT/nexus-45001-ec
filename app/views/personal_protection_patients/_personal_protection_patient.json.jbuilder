json.extract! personal_protection_patient, :id, :personal_protection_id, :patient_id, :date, :signature, :company_id, :created_at, :updated_at
json.url personal_protection_patient_url(personal_protection_patient, format: :json)
