json.extract! position, :id, :name, :code, :gender, :age_min, :age_max, :active, :number, :mission, :created_at, :updated_at
json.url position_url(position, format: :json)
