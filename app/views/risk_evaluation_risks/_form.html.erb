<%= simple_form_for(@risk_evaluation_risk, remote: request.xhr?, html: { data: { modal: true } }) do |f| %>
  <div class="modal-body">

    <div class="row">
      <div class="col-md-12">
        <div class="card-header card-header-tabs-basic nav" role="tablist">
          <a href="#evaluation" class="active" data-toggle="tab" role="tab" aria-controls="medical_data" aria-selected="true">Evaluación de Riesgo (1)</a>

          <a href="#control" data-toggle="tab" role="tab" aria-controls="risk_data" aria-selected="true" class="">Medidas de Control (2)</a>

          <a href="#plan" data-toggle="tab" role="tab" aria-controls="risk_data" aria-selected="true" class="">Plan de Acción (3)</a>

        </div>

        <div class="list-group tab-content list-group-flush">
          <div class="tab-pane fade active show" id="evaluation">
            <div class="row">
              <div class="col-md-12">
                <%= f.input :description %>    
              </div>

              <div class="col-md-12">
                <p style="margin-bottom: 0" class="text-muted">Nº de expuestos</p>
              </div>
              <div class="col-md-4">
                <%= f.input :m_count %>
              </div>
              <div class="col-md-4">
                <%= f.input :f_count %>    
              </div>
              <div class="col-md-4">
                <%= f.input :s_count %>    
              </div>


              <% if @risk_evaluation_risk.risk_evaluation.evaluation_type_id.to_i == 1 %>
                
                <div class="col-md-12">
                  <p style="margin-bottom: 0" class="text-muted">Valores (GP = C * E * P)</p>
                </div>

                <div class="col-md-12">  
                  <!-- <%= f.input :reference_value %> -->
                  <%= f.input :reference_value, collection: @reference_values, prompt: "Seleccione opción", input_html: {class: 'select_2 select_value'} %>
                </div>

                <div class="col-md-12">  
                  <!-- <%= f.input :medium_value %> -->
                  <%= f.input :medium_value, collection: @medium_values, prompt: "Seleccione opción", input_html: {class: 'select_2 select_value'} %>
                </div>

                <div class="col-md-12">
                  <!-- <%= f.input :exposition_value %> -->
                  <%= f.input :exposition_value, collection: @exposition_values, prompt: "Seleccione opción", input_html: {class: 'select_2 select_value'} %>
                </div>

                <div class="col-md-12">
                  <p style="margin-bottom: 0" class="text-muted">Resultados</p>
                </div>
                <div class="col-md-6">
                  <%= f.input :result, as: :hidden %>
                  <label class="form-control-label string optional" for="risk_evaluation_risk_result_gr">Valoración del GP ó Dosis</label>
                  <div id="result" class="text-label mb-3">0</div>
                </div>

                <div class="col-md-6">
                  <%= f.input :result_gr, as: :hidden %>
                  <label class="form-control-label string optional" for="risk_evaluation_risk_result_gr">Grado del Riesgo</label><br/>
                  <span id="result-text" class="label-form badge gp-1"><%= @risk_evaluation_risk.result_gr %></span>
                </div>

              <% else %>
                <div class="col-md-12">
                  <p style="margin-bottom: 0" class="text-muted">Valores (V = P * C)</p>
                </div>

                <div class="col-md-4">  
                  <%= f.input :reference_value, collection: @probability_values, prompt: "Seleccione opción", input_html: {class: 'select_2 select_value'} %>
                </div>

                <div class="col-md-4">  
                  <%= f.input :medium_value, collection: @consequence_values, prompt: "Seleccione opción", input_html: {class: 'select_2 select_value'} %>
                </div>

                <div class="col-md-4">
                  <label class="form-control-label string optional" for="risk_evaluation_risk_result_gr">Valoración del Riesgo</label>
                  <div id="result" class="text-label mb-3">0</div>
                  <%= f.input :result, as: :hidden, label: false %>
                </div>

                

              <% end  %>
            </div>
          </div>

          <div class="tab-pane" id="control">
            <div class="row">
              
              <div class="col-md-6">
                <%= f.input :control_id, collection: @controls, prompt: "Seleccione opción", input_html: {class: 'select_2 select_value'} %>
              </div>

              <div class="col-md-6">
                <%= f.input :process_work %>
              </div>

              <div class="col-md-6">
                <%= f.input :information %>
              </div>

              <div class="col-md-6">
                <%= f.input :formation %>
              </div>

              <div class="col-md-12">
                <%= f.input :controled_risk, :as => :radio_buttons, :collection => [['Si',true],['No',false]], :include_blank => false, item_wrapper_class: 'form-check-inline' %>
              </div>
            </div>
          </div>

          <div class="tab-pane" id="plan">
            <div class="row">

              <div class="col-md-12">
                <%= f.input :project_id, collection: @controls, prompt: "Seleccione opción", input_html: {class: 'select_2 select_value'} %>
              </div>
            </div>
          </div>

        </div>
      </div>   
      
      <%= f.input :risk_evaluation_id, as: :hidden %>    
      
    </div>
  </div>
    
  <div class="modal-footer">
    <div class="actions">
      <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">
        <i class="material-icons mr-1">close</i> Cerrar
      </button>
      <%= button_tag  class: 'btn btn-outline-primary' do %>
        <i class='material-icons mr-1'>save</i> Guardar
      <% end %>
    </div>
  </div>
<% end %>

<script type="text/javascript">

  var evaluation_method = <%= @risk_evaluation_risk.risk_evaluation.evaluation_type_id.to_i %>

  $('select.select_2').select2({
    language: {
      noResults: function () {
        return "No se encontró resultados";
      }
    },
    placeholder: function(){
      $(this).data('placeholder');
    },
    containerCssClass: ':all:',
    theme: "bootstrap",
    dropdownParent: $('#modal .modal-body')
  });

  $(".select_value").change(function() {
    get_result();    
  });

  function get_result(){
    var result = 0;
    var reference_value = $("#risk_evaluation_risk_reference_value").val();
    var medium_value = $("#risk_evaluation_risk_medium_value").val();
    var exposition_value = $("#risk_evaluation_risk_exposition_value").val();
    
    if (evaluation_method == 1){
      // Logic to method 1
      result = reference_value * medium_value * exposition_value
      $("#risk_evaluation_risk_result").val(result);
      $("#result").html(result);

      $("#result-text").removeClass("gp-1");
      $("#result-text").removeClass("gp-2");
      $("#result-text").removeClass("gp-3");
      $("#result-text").removeClass("gp-4");

      if (result >= 0 && result < 18){
        $("#result-text").html("Bajo");
        $("#risk_evaluation_risk_result_gr").val("Bajo");
        $("#result-text").addClass("gp-1");
      }else{
        if (result > 18 && result <= 85){
          $("#result-text").html("Medio");
          $("#risk_evaluation_risk_result_gr").val("Medio");
          $("#result-text").addClass("gp-2");
        }else{
          if (result > 85 && result <= 200){
            $("#result-text").html("Alto");
            $("#risk_evaluation_risk_result_gr").val("Alto");
            $("#result-text").addClass("gp-3");
          }else{
            if (result > 200){
              $("#result-text").html("Crítico");
              $("#risk_evaluation_risk_result_gr").val("Crítico");
              $("#result-text").addClass("gp-4");
            }          
          }       
        }     
      }
    } else {
      // Logic to method 2
      result = parseInt(reference_value) + parseInt(medium_value)
      $("#risk_evaluation_risk_result").val(result);
      

      $("#result").removeClass("val-0");
      $("#result").removeClass("val-1");
      $("#result").removeClass("val-2");
      $("#result").removeClass("val-3");
      $("#result").removeClass("val-4");

      $("#result").html(result);

      if (result == 0){
        $("#result").html("Riesgo Trivial (T)");
        $("#result").addClass("val-0");
      } else {
        if (result == 1){
          $("#result").html("Riesgo Tolerable (TO)");
          $("#result").addClass("val-1");
        } else {
          if (result == 2){
            $("#result").html("Riesgo Moderado (M)");
            $("#result").addClass("val-2");
          } else {
            if (result == 3){
              $("#result").html("Riesgo Importante (I)");
              $("#result").addClass("val-3");
            } else {
              if (result == 4){
                $("#result").html("Riesgo Intolerable (IN)");
                $("#result").addClass("val-4");
              }
            }
          }
        }
      }     


    }

  }

  get_result();

</script>



