json.extract! risk_evaluation_risk, :id, :created_at, :updated_at
json.url risk_evaluation_risk_url(risk_evaluation_risk, format: :json)
