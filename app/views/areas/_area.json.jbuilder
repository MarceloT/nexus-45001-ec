json.extract! area, :id, :name, :code, :active, :description, :parent_id, :created_at, :updated_at
json.url area_url(area, format: :json)
