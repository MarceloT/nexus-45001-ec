json.extract! diagnostic, :id, :created_at, :updated_at
json.url diagnostic_url(diagnostic, format: :json)
