json.extract! work_center, :id, :name, :description, :active, :company_id, :address, :created_at, :updated_at
json.url work_center_url(work_center, format: :json)
