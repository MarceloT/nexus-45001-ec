json.extract! risk, :id, :name, :code, :description, :risk_category_id, :created_at, :updated_at
json.url risk_url(risk, format: :json)
