json.extract! exam, :id, :name, :code, :active, :description, :created_at, :updated_at
json.url exam_url(exam, format: :json)
