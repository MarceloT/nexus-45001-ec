json.extract! risk_evaluation, :id, :name, :company_id, :process_id, :sub_process_id, :position_id, :area_id, :date, :occupational_manager, :evaluation_manager, :company_evaluation, :description, :created_at, :updated_at
json.url risk_evaluation_url(risk_evaluation, format: :json)
