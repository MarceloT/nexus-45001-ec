json.extract! company_process, :id, :name, :description, :owner_id, :company_id, :parent_id, :level, :process_in_id, :process_out_id, :process_type, :created_at, :updated_at
json.url company_process_url(company_process, format: :json)
