json.extract! patient, :id, :name, :name_1, :last_name, :last_name_1, :ci, :ci_type, :gender, :birth_date, :phone_1, :phone_2, :email, :civil_status, :nationality, :direction, :profession, :photo, :blood_type, :active, :description, :company_id, :area_id, :position_id, :created_at, :updated_at
json.url patient_url(patient, format: :json)
