json.extract! medical_certificate, :id, :date, :patient_id, :medical_record_id, :reason, :diagnostic_description, :days, :starts_date, :ends_date, :created_by_id, :document_id, :type_certificate, :created_at, :updated_at
json.url medical_certificate_url(medical_certificate, format: :json)
