json.extract! recipe, :id, :diagnostic_description, :medicines, :indications, :medical_record_id, :patient_id, :created_at, :updated_at
json.url recipe_url(recipe, format: :json)
