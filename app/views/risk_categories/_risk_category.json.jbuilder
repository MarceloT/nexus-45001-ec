json.extract! risk_category, :id, :name, :color, :description, :created_at, :updated_at
json.url risk_category_url(risk_category, format: :json)
