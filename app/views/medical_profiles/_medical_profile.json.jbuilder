json.extract! medical_profile, :id, :name, :code, :active, :description, :company_id, :created_at, :updated_at
json.url medical_profile_url(medical_profile, format: :json)
