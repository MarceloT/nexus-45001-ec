json.extract! project, :id, :name, :description, :object, :scope, :due_date, :status, :owner_id, :project_type_id, :company_id, :created_by_id, :created_at, :updated_at
json.url project_url(project, format: :json)
