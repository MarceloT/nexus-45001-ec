json.extract! medical_appointment, :id, :title, :start, :end, :color
json.url medical_appointment_url(medical_appointment, format: :json)

# date_format = medical_appointment.all_day_event? ? '%Y-%m-%d' : '%Y-%m-%dT%H:%M:%S'
# date_format = '%Y-%m-%dT%H:%M:%S'
# # date_format = '%Y-%m-%d'
# json.id medical_appointment[:id]
# json.title medical_appointment[:title]
# json.start medical_appointment[:start].strftime(date_format)
# json.end medical_appointment[:end].strftime(date_format)
# # json.color medical_appointment[:color] unless medical_appointment.color.blank?

# json.update_url medical_appointment_path(medical_appointment, method: :patch) if !@medical_appointment.errors.any?
# json.edit_url edit_medical_appointment_path(medical_appointment) if !@medical_appointment.errors.any?

# json.errors medical_appointment.errors
