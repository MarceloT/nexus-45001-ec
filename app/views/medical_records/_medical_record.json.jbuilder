json.extract! medical_record, :id, :date, :reason, :current_disease, :recommendations, :patient_id, :doctor_id, :doctor_signature, :aptitude, :aptitude_obs, :aptitude_limit, :created_at, :updated_at
json.url medical_record_url(medical_record, format: :json)
