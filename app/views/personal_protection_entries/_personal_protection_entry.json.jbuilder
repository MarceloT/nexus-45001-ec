json.extract! personal_protection_entry, :id, :origin_work_center_id, :destination_work_center_id, :patient_id, :date, :signature, :company_id, :user_id, :description, :created_at, :updated_at
json.url personal_protection_entry_url(personal_protection_entry, format: :json)
