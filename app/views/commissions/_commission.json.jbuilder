json.extract! commission, :id, :work_center_id, :company_id, :nro_members, :date, :active, :type_commission, :member_1, :member_2, :member_3, :member_4, :member_5, :member_6, :member_7, :member_8, :member_9, :member_10, :member_11, :member_12, :created_at, :updated_at
json.url commission_url(commission, format: :json)
