json.extract! personal_protection, :id, :name, :protection_type_id, :company_id, :stock, :image, :features, :details, :created_at, :updated_at
json.url personal_protection_url(personal_protection, format: :json)
