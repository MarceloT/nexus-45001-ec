#!/usr/bin/env ruby

# prawn can easily create pdf files. Must be 0.12.0, not above to support :template
require 'prawn'
class PdfWatermark
  def self.conbinate_watermark_pdf(pdf_file_path, watermark_file_path)
    Rails.logger.info "========pdf_file_path: #{pdf_file_path}"
    Rails.logger.info "========watermark_file_path: #{watermark_file_path}"
    watermark_file = CombinePDF.load(watermark_file_path).pages[0]
    pdf = CombinePDF.load pdf_file_path
    pdf.pages.each {|page| page << watermark_file}
    pdf.save pdf_file_path
  end
end