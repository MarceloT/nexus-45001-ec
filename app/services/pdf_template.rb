class PdfTemplate
	def self.header(pdf, title)
		# widht: 540
		logopath =  "#{Rails.root}/public/images/ecua-american-logo.png"

  	table_data = [
		  [
		  	{:image => logopath, :image_width => 200, :rowspan => 2, :vposition => :center}, 
		  	{:content => "ATENCIÓN PERSONALIZADA", size: 9, align: :center, valign: :bottom}
		  ],
		  [
		  	{:content => "<color rgb='77a6d1'>MATRIZ QUITO: Av. América N33-42 y Rumipamba, frente al Colegio San Gabriel
											PBX: 02.394.7880 / 099.9809.269
											GUAYAQUIL: Av. Boloña 107 y Av. Kennedy · Telf.: 04.239.0182
											CUENCA: Av. Paseo de los Cañaris 04-61 y Cacique Duma · Telf.: 07.2806.492
											laboratorioclinico@ecua-american.com - www.ecua-american.com</color>", size: 8, align: :right,  valign: :top}
		  ]
  	]

    pdf.table(
    	table_data, 
    	:column_widths => [240, 300],
    	:cell_style => { :inline_format => true, :border_color => "808080", :border_width => 0, :padding => [0, 0, 5,0] }
    ) 

    pdf.move_down 15

    table_data = [
			[{:content => "<b>#{title}</b>", size: 20, font_style: :bold, align: :center}]
		]

		pdf.table(
			table_data, 
			:column_widths => [540],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => 0 }
		) 
		pdf.move_down 15

		table_data = [
			[
				{:content => "<b>Fecha:</b>", size: 12, font_style: :bold, align: :center},
				{:content => "Quito, miércoles 14 de octubre 2020", size: 12, align: :center},
				{:content => "Página 1 de 1", size: 12, align: :right}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [50, 220, 270],
			:cell_style => { :inline_format => true, :border_color => "808080",
			:border_width => 2, :borders => [:top, :bottom] }
		) 

		pdf.move_down 15

	end

	def self.watermark(pdf, watermark)
    # function = lambda {pdf.formatted_text_box([{:text => "#{watermark} " * 1, :color => 'dddddd', :size => 80}], :rotate => 30, :rotate_around => :center, :align => :center, :valign => :center)}
    # # call the function for the first page
    # function.call
    # # call the function for every subsequent page that is created
    # pdf.on_page_create {function.call}
    pdf.repeat :all do
      pdf.formatted_text_box([{:text => "#{watermark} " * 1, :color => 'dddddd', :size => 80}], :rotate => 30, :rotate_around => :center, :align => :center, :valign => :center)
		end
  end


	def self.certificate(pdf, medical_record)
		# Goval value
		border_width     = 1
		title_size       = 10
		subtitle_size    = 8
		small_title_size = 7
		# DB value
		patient = medical_record.patient
		company = patient.company
		
		pdf.move_down 5		

		#============================================= Section A =============================================#
		table_data = [
			[{:content => "A. DATOS DEL ESTABLECIMIENTO - EMPRESA Y USUARIO", size: subtitle_size, :colspan => 6, font_style: :bold}],
			[
				{:content => "INSTITUCIÓN DEL SISTEMA O NOMBRE DE LA EMPRESA", size: subtitle_size, align: :center, valign: :center},
				{:content => "RUC", size: subtitle_size, align: :center, valign: :center},
				{:content => "CIIU", size: subtitle_size, align: :center, valign: :center},
				{:content => "ESTABLECIMIENTO DE SALUD", size: subtitle_size, align: :center, valign: :center},
				{:content => "NÚMERO DE HISTORIA CLÍNICA", size: subtitle_size, align: :center, valign: :center},
				{:content => "NÚMERO DE ARCHIVO", size: subtitle_size, align: :center, valign: :center}
			],
			[
				{:content => company.present? ? company.name : "----", size: subtitle_size},
				{:content => company.present? ? company.ruc : "----", size: subtitle_size},
				{:content => "861", size: subtitle_size},
				{:content => company.present? ? company.name : "----", size: subtitle_size},
				{:content => "##{patient.ci}" , size: subtitle_size},
				{:content => medical_record.created_at.strftime("%Y"), size: subtitle_size}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [130, 60, 60,115,115, 60],
			:row_colors => ["cccefe", "c8fdcd", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 

		table_data = [
			[
				{:content => "PRIMER APELLIDO ", size: subtitle_size, align: :center, valign: :center},
				{:content => "SEGUNDO APELLIDO ", size: subtitle_size, align: :center, valign: :center},
				{:content => "PRIMER NOMBRE ", size: subtitle_size, align: :center, valign: :center},
				{:content => "SEGUNDO NOMBRE ", size: subtitle_size, align: :center, valign: :center},
				{:content => "SEXO", size: subtitle_size, align: :center, valign: :center},
				{:content => "CARGO / OCUPACIÓN", size: subtitle_size, align: :center, valign: :center}
			],
			[
				{:content => patient.last_name.present? ? patient.last_name : "----", size: subtitle_size},
				{:content => patient.last_name_1.present? ? patient.last_name_1 : "----", size: subtitle_size},
				{:content => patient.name.present? ? patient.name : "----", size: subtitle_size},
				{:content => patient.name_1.present? ? patient.name_1 : "----", size: subtitle_size},
				{:content => patient.gender.present? ? patient.gender_to_s : "----", size: subtitle_size},
				{:content => patient.position.present? ? patient.position.code : "----", size: subtitle_size},
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [100, 100, 100,100, 40, 100],
			:row_colors => ["c8fdcd", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 

		pdf.move_down 5
		#============================================= Section B =============================================#

		table_data = [
			[{:content => "B. DATOS GENERALES", size: subtitle_size, :colspan => 9, font_style: :bold}],
			[
				{:content => "FECHA DE EMISIÓN", size: subtitle_size},
				{:content => Time.now.strftime("%Y-%m-%d"), size: subtitle_size, :colspan => 8, font_style: :bold}
			],
			[
				{:content => "EVALUACIÓN:", size: subtitle_size},
				{:content => "INGRESO", size: subtitle_size},
				{:content => medical_record.initial? ? "X" : "", size: subtitle_size, align: :center},
				{:content => "PERIÓDICO", size: subtitle_size},
				{:content => medical_record.periodic? ? "X" : "", size: subtitle_size, align: :center},
				{:content => "REINTEGRO", size: subtitle_size},
				{:content => medical_record.refund? ? "X" : "", size: subtitle_size, align: :center},
				{:content => "SALIDA", size: subtitle_size},
				{:content => medical_record.retirement? ? "X" : "", size: subtitle_size, align: :center}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [120, 65, 40, 65, 40, 65, 40, 65, 40],
			:row_colors => ["cccefe", "ffffff", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 

		pdf.move_down 5
		#============================================= Section C =============================================#
		table_data = [
			[{:content => "C. CONCEPTO PARA APTITUD LABORAL", size: subtitle_size, :colspan => 8, font_style: :bold}],
			[{:content => "Después de la valoración médica ocupacional se certifica que la persona en mención, es calificada como:", size: subtitle_size, :colspan => 8}],
			[
				{:content => "APTO", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.aptitude == 1 ? "X" : "", size: subtitle_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "APTO EN OBSERVACIÓN", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.aptitude == 2 ? "X" : "", size: subtitle_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "APTO CON LIMITACIONES", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.aptitude == 3 ? "X" : "", size: subtitle_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "NO APTO", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.aptitude == 4 ? "X" : "", size: subtitle_size, align: :center, valign: :center, :background_color => "ffffff"}
			],
			[
				{:content => "DETALLE DE OBSERVACIONES:", size: subtitle_size, :colspan => 8}
			],
			[
				{:content => medical_record.aptitude_obs.present? || medical_record.aptitude_limit.present? ? "#{medical_record.aptitude_obs} \n #{medical_record.aptitude_limit}" : "----", size: subtitle_size, :colspan => 8, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [95, 30, 105, 30, 115, 30, 105, 30],
			:row_colors => ["cccefe", "ffffff", "c8fdcd", "ffffff", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 
		pdf.move_down 5
		#============================================= Section D =============================================#
		table_data = [
			[{:content => "D. CONDICIONES DE SALUD AL MOMENTO DEL RETIRO", size: subtitle_size, :colspan => 5, font_style: :bold}],
			[
				{:content => "Después de la valoración médica ocupacional se certifica que la persona en mención, es calificada como:", size: subtitle_size, :background_color => "ffffff"},
				{:content => "SATISFACTORIO", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.retirement_satisfaction.present? ? "X" : "", size: subtitle_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "NO SATISFACTORIO", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.retirement_satisfaction.present? ? "" : "X", size: subtitle_size, align: :center, valign: :center, :background_color => "ffffff"},
			],
			[
				{:content => "OBSERVACIONES RELACIONADAS CON LAS CONDICIONES DE SALUD AL MOMENTO DEL RETIRO:", size: subtitle_size, :colspan => 5}
			],
			[
				{:content => medical_record.retirement_comments.present? ? medical_record.retirement_comments : "----", size: subtitle_size, :colspan => 5, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [260, 105, 30, 115, 30],
			:row_colors => ["cccefe", "c8fdcd", "ffffff", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 
		pdf.move_down 5
		#============================================= Section E =============================================#

		table_data = [
			[{:content => "E. RECOMENDACIONES ", size: subtitle_size, font_style: :bold}],
			[
				{:content => medical_record.aptitude_recomendations.present? ? medical_record.aptitude_recomendations : "----", size: subtitle_size, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [540],
			:row_colors => ["cccefe", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 
		pdf.move_down 5

		table_data = [
			[{:content => "Con este documento certifico que el trabajador se ha sometido a la evaluación médica requerida para (el ingreso /la ejecución/ el
			reintegro y retiro) al puesto laboral y se ha informado sobre los riesgos relacionados con el trabajo emitiendo recomendaciones
			relacionadas con su estado de salud.", size: subtitle_size, font_style: :bold}]
		]

		pdf.table(
			table_data, 
			:column_widths => [540],
			:row_colors => ["c8fdcd"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 
		pdf.move_down 5

		pdf.text "La presente certificación se expide con base en la historia ocupacional del usuario (a), la cual tiene carácter de confidencial.", size: subtitle_size
		
		pdf.move_down 5		

		#============================================= Section F =============================================#

		table_data = [
			[
				{:content => "F. DATOS DEL PROFESIONAL DE SALUD", size: subtitle_size, :colspan => 6, font_style: :bold},
				{:content => "G. FIRMA DEL USUARIO", size: subtitle_size, font_style: :bold}
			],
			[
				{:content => "NOMBRE Y APELLIDO", size: small_title_size, align: :center, valign: :center},
				{:content => "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "CÓDIGO", size: small_title_size, align: :center, valign: :center},
				{:content => "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "FIRMA Y SELLO", size: small_title_size, align: :center, valign: :center},
				{:content => "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [50, 90, 50, 90, 50, 90, 120],
			:row_colors => ["cccefe", "c8fdcd", "ffffff", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 

		pdf.move_down 5

		if medical_record.edit? || medical_record.finished? || medical_record.comments?
			watermark(pdf, "Documento Incompleto")
		end
	  
	end


	def self.periodic(pdf, medical_record)
		# widht: 540
		# Goval value
		border_width     = 1
		title_size       = 10
		subtitle_size    = 8
		small_title_size = 6
		# DB value
		patient = medical_record.patient
		company = patient.company
		
		pdf.move_down 5		

		#============================================= Section A =============================================#
		table_data = [
			[{:content => "A. DATOS DEL ESTABLECIMIENTO - EMPRESA Y USUARIO", size: subtitle_size, :colspan => 6, font_style: :bold}],
			[
				{:content => "INSTITUCIÓN DEL SISTEMA O NOMBRE DE LA EMPRESA", size: subtitle_size, align: :center, valign: :center},
				{:content => "RUC", size: subtitle_size, align: :center, valign: :center},
				{:content => "CIIU", size: subtitle_size, align: :center, valign: :center},
				{:content => "ESTABLECIMIENTO DE SALUD", size: subtitle_size, align: :center, valign: :center},
				{:content => "NÚMERO DE HISTORIA CLÍNICA", size: subtitle_size, align: :center, valign: :center},
				{:content => "NÚMERO DE ARCHIVO", size: subtitle_size, align: :center, valign: :center}
			],
			[
				{:content => company.present? ? company.name : "----", size: subtitle_size},
				{:content => company.present? ? company.ruc : "----", size: subtitle_size},
				{:content => "861", size: subtitle_size},
				{:content => company.present? ? company.name : "----", size: subtitle_size},
				{:content => "##{patient.ci}" , size: subtitle_size},
				{:content => medical_record.created_at.strftime("%Y"), size: subtitle_size}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [130, 60, 60,115,115, 60],
			:row_colors => ["cccefe", "c8fdcd", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 

		table_data = [
			[
				{:content => "PRIMER APELLIDO ", size: subtitle_size, align: :center, valign: :center},
				{:content => "SEGUNDO APELLIDO ", size: subtitle_size, align: :center, valign: :center},
				{:content => "PRIMER NOMBRE ", size: subtitle_size, align: :center, valign: :center},
				{:content => "SEGUNDO NOMBRE ", size: subtitle_size, align: :center, valign: :center},
				{:content => "SEXO", size: subtitle_size, align: :center, valign: :center},
				{:content => "CARGO / OCUPACIÓN", size: subtitle_size, align: :center, valign: :center}
			],
			[
				{:content => patient.last_name.present? ? patient.last_name : "----", size: subtitle_size},
				{:content => patient.last_name_1.present? ? patient.last_name_1 : "----", size: subtitle_size},
				{:content => patient.name.present? ? patient.name : "----", size: subtitle_size},
				{:content => patient.name_1.present? ? patient.name_1 : "----", size: subtitle_size},
				{:content => patient.gender.present? ? patient.gender_to_s : "----", size: subtitle_size},
				{:content => patient.position.present? ? patient.position.code : "----", size: subtitle_size},
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [100, 100, 100,100, 40, 100],
			:row_colors => ["c8fdcd", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 

		pdf.move_down 5
		#============================================= Section B =============================================#

		table_data = [
			[{:content => "B. MOTIVO DE CONSULTA", size: subtitle_size, font_style: :bold}],
			[
				{:content => medical_record.reason.present? ? medical_record.reason : "----", size: subtitle_size, :colspan => 5, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [540],
			:row_colors => ["cccefe", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 

		pdf.move_down 5
		#============================================= Section C =============================================#
		table_data = [
			[{:content => "C. ANTECEDENTES PERSONALES", size: subtitle_size, font_style: :bold}],
			[
				{:content => "ANTECEDENTES CLÍNICOS Y QUIRÚRGICOS", size: subtitle_size, :colspan => 5}
			],
			[
				{:content => medical_record.clinical_history.present? ? medical_record.clinical_history : "----", size: subtitle_size, :colspan => 5, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [540],
			:row_colors => ["cccefe", "c8fdcd", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)

		toxic_habits_1 = medical_record.medical_record_toxic_habits.find_by_toxic_habit_id(1)
		toxic_habits_2 = medical_record.medical_record_toxic_habits.find_by_toxic_habit_id(2)
		toxic_habits_3 = medical_record.medical_record_toxic_habits.find_by_toxic_habit_id(3)

		table_data = [
			[
				{:content => "HÁBITOS TÓXICOS", size: subtitle_size, :colspan => 7},
				{:content => "ESTILO DE VIDA", size: subtitle_size, :colspan => 5}
			],
			[
				{:content => "CONSUMOS NOCIVOS", size: small_title_size},
				{:content => "SI", size: small_title_size},
				{:content => "NO", size: small_title_size},
				{:content => "TIEMPO DE CONSUMO (meses)", size: small_title_size},
				{:content => "CANTIDAD", size: small_title_size},
				{:content => "EX CONSUMIDOR", size: small_title_size},
				{:content => "TIEMPO DE ABSTINENCIA (meses)", size: small_title_size},
				{:content => "ESTILO", size: small_title_size},
				{:content => "SI", size: small_title_size},
				{:content => "NO", size: small_title_size},
				{:content => "¿CUÁL?", size: small_title_size},
				{:content => "TIEMPO / CANTIDAD", size: small_title_size},
			],
			[
				{:content => "TABACO", size: small_title_size},
				{:content => toxic_habits_1.active.present? ? "X" : "", size: small_title_size},
				{:content => toxic_habits_1.active.present? ? "" : "X", size: small_title_size},
				{:content => toxic_habits_1.time.present? ? toxic_habits_1.time : "----", size: small_title_size},
				{:content => toxic_habits_1.quantity.present? ? toxic_habits_1.quantity : "----", size: small_title_size},
				{:content => toxic_habits_1.ex_consumer.present? ? "SI" : "NO", size: small_title_size},
				{:content => toxic_habits_1.abstinence_time.present? ? toxic_habits_1.abstinence_time : "----", size: small_title_size},
				{:content => "ACTIVIDAD FÍSICA", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
			],
			[
				{:content => "ALCOHOL", size: small_title_size},
				{:content => toxic_habits_2.active.present? ? "X" : "", size: small_title_size},
				{:content => toxic_habits_2.active.present? ? "" : "X", size: small_title_size},
				{:content => toxic_habits_2.time.present? ? toxic_habits_2.time : "----", size: small_title_size},
				{:content => toxic_habits_2.quantity.present? ? toxic_habits_2.quantity : "----", size: small_title_size},
				{:content => toxic_habits_2.ex_consumer.present? ? "SI" : "NO", size: small_title_size},
				{:content => toxic_habits_2.abstinence_time.present? ? toxic_habits_2.abstinence_time : "----", size: small_title_size},
				{:content => "MEDICACIÓN HABITUAL", size: small_title_size, rowspan: 3},
				{:content => "", size: small_title_size, rowspan: 3},
				{:content => "", size: small_title_size, rowspan: 3},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
			],
			[
				{:content => "OTRAS DROGAS:", size: small_title_size},
				{:content => "", size: small_title_size, rowspan: 2},
				{:content => "", size: small_title_size, rowspan: 2},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size}
			],
			[
				{:content => "----", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "", size: small_title_size}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [60, 20, 20, 60, 60, 60, 60, 60, 20, 20, 50, 50],
			:row_colors => ["c8fdcd", "cbfeff", "ffffff", "ffffff", "ffffff", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)

		table_data = [
			[{:content => "INCIDENTES", size: subtitle_size}],
			[
				{:content => "Describir los principales incidentes suscitados", size: small_title_size, :colspan => 5}
			],
			[
				{:content => medical_record.incident.present? ? medical_record.incident : "----", size: subtitle_size, :colspan => 5, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [540],
			:row_colors => ["c8fdcd", "ffffff", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)

		table_data = [
			[{:content => "ACCIDENTES DE TRABAJO (DESCRIPCIÓN)", :colspan => 7, size: subtitle_size}],
			[
				{:content => "FUE CALIFICADO POR EL INSTITUTO DE SEGURIDAD SOCIAL CORRESPONDIENTE:", size: small_title_size},
				{:content => "SI", size: small_title_size},
				{:content => "ESPECIFICAR:", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "NO", size: small_title_size},
				{:content => "FECHA:", size: small_title_size},
				{:content => "", size: small_title_size}
			],
			[
				{:content => medical_record.work_accidents.present? ? medical_record.work_accidents : "----", size: subtitle_size, :colspan => 7, height: 50}
			],
			[{:content => "ENFERMEDADES PROFESIONALES ", :colspan => 7, size: subtitle_size}],
			[
				{:content => "FUE CALIFICADO POR EL INSTITUTO DE SEGURIDAD SOCIAL CORRESPONDIENTE:", size: small_title_size},
				{:content => "SI", size: small_title_size},
				{:content => "ESPECIFICAR:", size: small_title_size},
				{:content => "", size: small_title_size},
				{:content => "NO", size: small_title_size},
				{:content => "FECHA:", size: small_title_size},
				{:content => "", size: small_title_size}
			],
			[
				{:content => medical_record.occupational_diseases.present? ? medical_record.occupational_diseases : "----", size: subtitle_size, :colspan => 7, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [280, 20, 50, 70, 20, 50, 50],
			:row_colors => ["c8fdcd", "ffffff", "ffffff", "c8fdcd", "ffffff", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)

		pdf.move_down 30
		pdf.start_new_page
		#============================================= Section D =============================================#

		table_data = [
			[{:content => "D. ANTECEDENTES FAMILIARES (DETALLAR EL PARENTESCO)", size: subtitle_size, :colspan => 16, font_style: :bold}],
			[
				{:content => "1. ENFERMEDAD CARDIO-VASCULAR", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_family_histories.find_by_family_history_id(1).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "2. ENFERMEDAD METABÓLICA", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_family_histories.find_by_family_history_id(2).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "3. ENFERMEDAD NEUROLÓGICA", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_family_histories.find_by_family_history_id(3).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "4. ENFERMEDAD ONCOLÓGICA", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_family_histories.find_by_family_history_id(4).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "5. ENFERMEDAD INFECCIOSA", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_family_histories.find_by_family_history_id(5).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "6. ENFERMEDAD HEREDITARIA / CONGÉNITA", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_family_histories.find_by_family_history_id(6).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "7. DISCAPACIDADES", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_family_histories.find_by_family_history_id(7).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "8. OTROS", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_family_histories.find_by_family_history_id(8).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"}
			],
			[
				{:content => medical_record.family_history_description.present? ? medical_record.family_history_description : "----", size: subtitle_size, :colspan => 16, height: 50}
			]
		]
		pdf.table(
			table_data, 
			# :column_widths => [95, 30, 105, 30, 115, 30, 105, 30],
			:row_colors => ["cccefe", "c8fdcd", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 
		pdf.move_down 5
		#============================================= Section E =============================================#
		table_data = [
			[{:content => "E. FACTORES DE RIESGOS DEL PUESTO DE TRABAJO", size: subtitle_size, font_style: :bold}],
			[
				{:content => "----", size: subtitle_size, :colspan => 5, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [540],
			:row_colors => ["cccefe", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)
		pdf.move_down 5

		#============================================= Section F =============================================#
		table_data = [
			[{:content => "F. ENFERMEDAD ACTUAL", size: subtitle_size, font_style: :bold}],
			[
				{:content => medical_record.current_disease.present? ? medical_record.current_disease : "----", size: subtitle_size, :colspan => 5, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [540],
			:row_colors => ["cccefe", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)
		pdf.move_down 5

		#============================================= Section G =============================================#
		table_data = [
			[{:content => "G. REVISIÓN DE ÓRGANOS Y SISTEMAS", size: subtitle_size, :colspan => 10, font_style: :bold}],
			[
				{:content => "1. PIEL-ANEXOS", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_system_revisions.find_by_system_revision_id(1).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "3. RESPIRATORIO ", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_system_revisions.find_by_system_revision_id(3).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "5. DIGESTIVO ", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_system_revisions.find_by_system_revision_id(5).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "7. MÚSCULO ESQUELÉTICO ", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_system_revisions.find_by_system_revision_id(7).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "9. HEMO LINFÁTICO", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_system_revisions.find_by_system_revision_id(9).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
			],
			[
				{:content => "2. ÓRGANOS DE LOS SENTIDOS", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_system_revisions.find_by_system_revision_id(2).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "4. CARDIO-VASCULAR", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_system_revisions.find_by_system_revision_id(4).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "6. GENITO-URINARIO", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_system_revisions.find_by_system_revision_id(6).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "8. ENDOCRINO", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_system_revisions.find_by_system_revision_id(8).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "10. NERVIOSO", size: small_title_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_system_revisions.find_by_system_revision_id(10).active? ? "X" : "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
			],
			[
				{:content => medical_record.revision_description.present? ? medical_record.revision_description : "----", size: subtitle_size, :colspan => 10, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [80, 28, 80, 28, 80, 28, 80, 28, 80, 28],
			:row_colors => ["cccefe", "c8fdcd", "c8fdcd", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)
		pdf.move_down 5

		#============================================= Section H =============================================#
		table_data = [
			[{:content => "H. CONSTANTES VITALES Y ANTROPOMETRÍA ", size: subtitle_size, :colspan => 10, font_style: :bold}],
			[
				{:content => "PRESIÓN ARTERIAL (mmHg)", size: small_title_size, align: :center, valign: :center},
				{:content => "TEMPERATURA (°C)", size: small_title_size, align: :center, valign: :center},
				{:content => "FRECUENCIA CARDIACA (Lat/min)", size: small_title_size, align: :center, valign: :center},
				{:content => "SATURACIÓN DE OXÍGENO (O2%)", size: small_title_size, align: :center, valign: :center},
				{:content => "FRECUENCIA RESPIRATORIA (fr/min)", size: small_title_size, align: :center, valign: :center},
				{:content => "PESO (Kg)", size: small_title_size, align: :center, valign: :center},
				{:content => "TALLA (cm)", size: small_title_size, align: :center, valign: :center},
				{:content => "ÍNDICE DE MASA CORPORAL (Kg/m2)", size: small_title_size, align: :center, valign: :center},
				{:content => "PERÍMETRO ABDOMINAL (cm)", size: small_title_size, align: :center, valign: :center},
			],
			[
				{:content => medical_record.medical_record_vital_constants.find_by_vital_constant_id(1).value.present? ? medical_record.medical_record_vital_constants.find_by_vital_constant_id(1).value.to_s : "----", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_vital_constants.find_by_vital_constant_id(2).value.present? ? medical_record.medical_record_vital_constants.find_by_vital_constant_id(2).value.to_s : "----", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_vital_constants.find_by_vital_constant_id(3).value.present? ? medical_record.medical_record_vital_constants.find_by_vital_constant_id(3).value.to_s : "----", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_vital_constants.find_by_vital_constant_id(4).value.present? ? medical_record.medical_record_vital_constants.find_by_vital_constant_id(4).value.to_s : "----", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_vital_constants.find_by_vital_constant_id(5).value.present? ? medical_record.medical_record_vital_constants.find_by_vital_constant_id(5).value.to_s : "----", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_vital_constants.find_by_vital_constant_id(6).value.present? ? medical_record.medical_record_vital_constants.find_by_vital_constant_id(6).value.to_s : "----", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_vital_constants.find_by_vital_constant_id(7).value.present? ? medical_record.medical_record_vital_constants.find_by_vital_constant_id(7).value.to_s : "----", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_vital_constants.find_by_vital_constant_id(8).value.present? ? medical_record.medical_record_vital_constants.find_by_vital_constant_id(8).value.to_s : "----", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.medical_record_vital_constants.find_by_vital_constant_id(9).value.present? ? medical_record.medical_record_vital_constants.find_by_vital_constant_id(9).value.to_s : "----", size: subtitle_size, align: :center, valign: :center}
			],
		]

		pdf.table(
			table_data, 
			:column_widths => [70, 60, 70, 60, 80, 30, 40, 70, 60],
			:row_colors => ["cccefe", "c8fdcd", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)
		pdf.move_down 265
		pdf.start_new_page
		#============================================= Section I =============================================#
		table_data = [
			[{:content => "I. EXAMEN FÍSICO REGIONAL", size: subtitle_size, :colspan => 15, font_style: :bold}],
			[{:content => "REGIONES ", size: subtitle_size, :colspan => 15}],
			[
				{:content => "1. Piel", size: 7, :rowspan => 3, :rotate => 70, :width => 32, align: :center, valign: :bottom},
				{:content => "a. Cicatrices", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(1).active.present? ? "X" : "" , size: 7, :background_color => "ffffff", :width => 15},
				{:content => "3. Oído", size: 7, :rowspan => 3, :rotate => 70, :width => 35, align: :center, valign: :bottom},
				{:content => "a. C. auditivo externo", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(9).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "5. Nariz", size: 7, :rowspan => 4, :rotate => 70, :width => 30, align: :center, valign: :bottom},
				{:content => "a. Tabique", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(17).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "8. Tórax", size: 7, :rowspan => 2, :rotate => 70, :width => 40, align: :center, valign: :bottom},
				{:content => "a. Pulmones", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(25).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "11. Pelvis", size: 7, :rowspan => 2, :rotate => 70, :width => 45, align: :center, valign: :bottom},
				{:content => "a. Pelvis", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(32).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15}
			],
			[
				{:content => "b. Tatuajes", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(2).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Pabellón ", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(10).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Cornetes", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(18).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Parrilla costal", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(26).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Genitales", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(33).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15}
			],
			[
				{:content => "c. Piel y faneras", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(3).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "c. Tímpanos", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(11).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "c. Mucosas", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(19).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "9. Abdomen", size: 7, :rowspan => 2, :rotate => 70, :width => 40, align: :center, valign: :bottom},
				{:content => "a. Vísceras", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(27).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "12. Extremidades", size: 7, :rowspan => 3, :rotate => 70, :width => 45, align: :center, valign: :bottom},
				{:content => "a. Vascular", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(34).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15}
			],
			[
				{:content => "2. Ojos", size: 7, :rowspan => 5, :rotate => 70, :width => 32, align: :center, valign: :bottom},
				{:content => "a. Párpados", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(4).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "4. Oro faringe", size: 7, :rowspan => 5, :rotate => 70, :width => 35, align: :center, valign: :bottom},
				{:content => "a. Labios", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(12).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "d. Senos paranasales", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(20).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Pared abdominal", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(28).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Miembros superiores", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(35).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15}
			],
			[
				{:content => "b. Conjuntivas ", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(5).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Lengua", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(13).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "6. Cuello", size: 7, :rowspan => 2, :rotate => 70, :width => 30, align: :center, valign: :bottom},
				{:content => "a. Tiroides / masas", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(21).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "10. Columna", size: 7, :rowspan => 4, :rotate => 70, :width => 40, align: :center, valign: :bottom},
				{:content => "a. Flexibilidad", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(29).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "c. Miembros inferiores", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(36).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15}
			],
			[
				{:content => "c. Pupilas", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(6).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "c. Faringe", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(14).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Movilidad", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(22).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Desviación", size: 7, :rowspan => 2},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(30).active.present? ? "X" : "", size: 7, :rowspan => 2, :background_color => "ffffff", :width => 15},
				{:content => "13. Neurológico", size: 7, :rowspan => 4, :rotate => 70, :width => 45, align: :center, valign: :bottom},
				{:content => "a. Fuerza ", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(37).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15}
			],
			[
				{:content => "d. Córnea", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(7).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "d. Amígdalas", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(15).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "7. Tórax", size: 7, :rowspan => 2, :rotate => 70, :width => 30, align: :center, valign: :bottom},
				{:content => "a. Mamas", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(23).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Sensibilidad", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(38).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15}
			],
			[
				{:content => "e. Motilidad", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(8).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "e. Dentadura", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(16).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "b. Corazón", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(24).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "c. Dolor", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(31).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15},
				{:content => "c. Marcha", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(39).active.present? ? "X" : "", size: 7, :background_color => "ffffff", :width => 15}
			],
			[
				{:content => "SI EXISTE EVIDENCIA DE PATOLOGÍA MARCAR CON 'X' Y DESCRIBIR EN LA SIGUIENTE SECCIÓN COLOCANDO EL NUMERAL", size: small_title_size, :colspan => 12, :background_color => "ffffff"},
				{:content => "d. Reflejos", size: 7},
				{:content => medical_record.medical_record_physical_exams.find_by_physical_exam_id(40).active.present? ? "X" : "", size: 7, :background_color => "ffffff"}
			],
			[
				{:content => medical_record.exam_description.present? ? medical_record.exam_description : "----", size: 7, :colspan => 15, height: 50}
			]

		]

		pdf.table(
			table_data, 
			:row_colors => ["cccefe", "c8fdcd", "cbfeff", "cbfeff", "cbfeff", "cbfeff", "cbfeff", "cbfeff", "cbfeff", "cbfeff", "cbfeff", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)
		pdf.move_down 5
		#============================================= Section J =============================================#
		table_data = [
			[{:content => "J. RESULTADOS DE EXÁMENES GENERALES Y ESPECÍFICOS DE ACUERDO AL RIESGO Y PUESTO DE TRABAJO (IMAGEN, LABORATORIO Y OTROS)", :colspan => 3, size: subtitle_size, font_style: :bold}],
			[
				{:content => "EXAMEN", size: subtitle_size, align: :center, valign: :center},
				{:content => "FECHA aaaa/mm/dd", size: subtitle_size, align: :center, valign: :center},
				{:content => "RESULTADO", size: subtitle_size, align: :center, valign: :center},
			],
			[
				{:content => "----", size: subtitle_size, align: :center, valign: :center},
				{:content => "", size: subtitle_size, align: :center, valign: :center},
				{:content => "", size: subtitle_size, align: :center, valign: :center},
			],
			[
				{:content => "----", size: subtitle_size, :colspan => 3, height: 20}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [100, 80, 360],
			:row_colors => ["cccefe", "c8fdcd", "ffffff", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)
		pdf.move_down 5
		#============================================= Section K =============================================#
		table_data = [
			[{:content => "K. DIAGNÓSTICO     PRE = PRESUNTIVO DEF = DEFINITIVO", size: subtitle_size, :colspan => 5, font_style: :bold}],
			[
				{:content => "", size: subtitle_size, align: :center, valign: :center},
				{:content => "DESCRIPCIÓN", size: subtitle_size, align: :center, valign: :center},
				{:content => "CIE", size: subtitle_size, align: :center, valign: :center},
				{:content => "PRE", size: subtitle_size, align: :center, valign: :center},
				{:content => "DEF", size: subtitle_size, align: :center, valign: :center}
			]
		]

		medical_record.medical_record_diagnostics.each_with_index do |medical_record_diagnostic, index|
			table_data << [
				{:content => (index + 1).to_s, size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record_diagnostic.diagnostic.name, size: subtitle_size, valign: :center},
				{:content => medical_record_diagnostic.diagnostic.code, size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record_diagnostic.pre.present? ? "X" : "", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record_diagnostic.pre.present? ? "" : "X", size: subtitle_size, align: :center, valign: :center}
			]
		end


		pdf.table(
			table_data, 
			:column_widths => [15, 385, 60, 40, 40],
			:row_colors => ["cccefe", "c8fdcd", "ffffff", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)
		pdf.move_down 5
		#============================================= Section L =============================================#
		table_data = [
			[{:content => "L. APTITUD MÉDICA PARA EL TRABAJO", :colspan => 8, size: subtitle_size, font_style: :bold}],
			[
				{:content => "APTO", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.aptitude == 1 ? "X" : "", size: subtitle_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "APTO EN OBSERVACIÓN", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.aptitude == 2 ? "X" : "", size: subtitle_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "APTO CON LIMITACIONES", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.aptitude == 3 ? "X" : "", size: subtitle_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "APTO", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.aptitude == 4 ? "X" : "", size: subtitle_size, align: :center, valign: :center, :background_color => "ffffff"},
			],
			[
				{:content => "Observación", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.aptitude_obs.present? ? medical_record.aptitude_obs : "----", size: subtitle_size, :colspan => 7, :background_color => "ffffff"},
			],
			[
				{:content => "Limitación", size: subtitle_size, align: :center, valign: :center},
				{:content => medical_record.aptitude_limit.present? ? medical_record.aptitude_limit : "----", size: subtitle_size, :colspan => 7, :background_color => "ffffff"},
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [120, 20, 120, 20, 120, 20, 100, 20],
			:row_colors => ["cccefe" , "c8fdcd", "cbfeff", "cbfeff", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)
		pdf.move_down 5
		#============================================= Section M =============================================#
		table_data = [
			[{:content => "M. RECOMENDACIONES Y/O TRATAMIENTO", size: subtitle_size, font_style: :bold}],
			[
				{:content => medical_record.recommendations.present? ? medical_record.recommendations : "----", size: subtitle_size, :colspan => 5, height: 50}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [540],
			:row_colors => ["cccefe", "ffffff"],
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		)
		pdf.move_down 5

		pdf.text "CERTIFICO QUE LO ANTERIORMENTE EXPRESADO EN RELACIÓN A MI ESTADO DE SALUD ES VERDAD. SE ME HA INFORMADO LAS MEDIDAS PREVENTIVAS A TOMAR PARA DISMINUIR O MITIGAR LOS RIESGOS RELACIONADOS CON MI ACTIVIDAD LABORAL.", size: 7

		pdf.move_down 5
		#============================================= Section N =============================================#

		table_data = [
			[
				{:content => "N. DATOS DEL PROFESIONAL", size: subtitle_size, :colspan => 10, font_style: :bold},
				{:content => "O. FIRMA DEL USUARIO", size: subtitle_size, font_style: :bold}
			],
			[
				{:content => "FECHA aaaa-mm-dd", size: small_title_size, align: :center, valign: :center},
				{:content => "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "HORA", size: small_title_size, align: :center, valign: :center},
				{:content => "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "NOMBRE Y APELLIDO", size: small_title_size, align: :center, valign: :center},
				{:content => "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "CÓDIGO", size: small_title_size, align: :center, valign: :center},
				{:content => "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "FIRMA Y SELLO", size: small_title_size, align: :center, valign: :center},
				{:content => "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"},
				{:content => "", size: small_title_size, align: :center, valign: :center, :background_color => "ffffff"}
			]
		]

		pdf.table(
			table_data, 
			:column_widths => [45, 40, 30, 30, 40, 60, 40, 30, 50, 60, 115],
			:row_colors => ["cccefe", "c8fdcd", "ffffff", "ffffff"], 
			:cell_style => { :inline_format => true, :border_color => "808080",:border_width => border_width }
		) 

		pdf.move_down 5

		if medical_record.edit? || medical_record.finished? || medical_record.comments?
			watermark(pdf, "Documento Incompleto")
		end
	  
	end
end