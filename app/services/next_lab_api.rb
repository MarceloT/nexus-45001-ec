class NextLabApi

  def initialize(import_credential_token)
    @import_credential_token = import_credential_token
  end

  def get_api_orders(value, type)
    case type
    when "order"
      remote_route = "#{Rails.configuration.exchange_api_url}#{Rails.configuration.exchange_api_orders}#{value}?sync=true"
    when "patient"
      remote_route = "#{Rails.configuration.exchange_api_url}#{Rails.configuration.exchange_api_orders_by_patient}#{value}"
    when "doctor"
      remote_route = "#{Rails.configuration.exchange_api_url}#{Rails.configuration.exchange_api_orders_by_doctor}#{value}"
    when "date_range"
      remote_route = "#{Rails.configuration.exchange_api_url}#{Rails.configuration.exchange_api_orders_by_date_range}?#{value}"
    end
    response = ApplicationRecord.call_rest_api(remote_route, @import_credential_token)
    return response
  end

  def get_api_doctors
    remote_route = "#{Rails.configuration.exchange_api_url}#{Rails.configuration.exchange_api_doctors}"
    response = ApplicationRecord.call_rest_api(remote_route, @import_credential_token)
    return response
  end

end