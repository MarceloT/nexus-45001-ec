module ApplicationHelper
  def link_to_show_redirect(url)
    link_to '<i class="material-icons icon-16pt">insert_drive_file</i>'.html_safe , url
  end

  def link_to_pdf(url)
    link_to '<i class="material-icons icon-16pt">picture_as_pdf</i>'.html_safe, url, :target => "_blank"
  end

  def link_to_edit(url)
    link_to '<i class="material-icons icon-16pt">edit</i>'.html_safe , url,  data: { modal: true }
  end

  def link_to_show(url)
    link_to '<i class="material-icons icon-16pt">insert_drive_file</i>'.html_safe , url,  data: { modal: true }
  end

  def link_to_show_print(url)
    link_to '<i class="material-icons icon-16pt">picture_as_pdf</i>'.html_safe, url,  data: { modal: true }
  end  

  def link_to_delete(url)
    link_to '<i class="material-icons icon-16pt">delete_forever</i>'.html_safe , url, method: :delete, data: { confirm: 'Esta seguro de eliminar?' }
  end

  def select_array(array, select_item)
    element = array.find { |el| el[1] == select_item.to_s }
    element.present? ? element[0] : "Sin asignar"
  end

  def show_avatar(user)
    if user.photo?
      image_tag(user.photo_url, class: 'avatar-circle')
    else
      render partial: 'shared/avatar', locals: {user: user}
    end
  end

end
