class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  before_action :get_patients, only: [:new, :create, :edit, :update]
  before_action :get_type_project, only: [:new, :create, :edit, :update, :show, :get_dataset]

  respond_to :html

  def index
    @sidebar_rigth_app = true
  end

  def get_dataset
    items = Project.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id_to_s,
        name: i.name,
        date: i.due_date,
        status: i.status_to_s,
        owner: i.owner.full_name,
        edit: "#{helpers.link_to_show_redirect(project_path(i.id))} #{helpers.link_to_edit(edit_project_path(i.id))}"
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    @sidebar_rigth_app = true
    respond_with(@project)
  end

  def new
    @project = Project.new
    @project.company_id = @company.id
    @project.created_by_id = current_user.id
    respond_modal_with @project
  end

  def edit
    respond_modal_with @project
  end

  def create
    @project = Project.create(project_params)
    respond_modal_with @project, location: projects_path
  end

  def update
    @project.update(project_params)
    respond_modal_with @project, location: projects_path
  end

  def destroy
    @project.destroy
    respond_with(@project)
  end

  def get_type_project
    @type_project = [
      ["Plan Anual de Capacitación", "1"],
      ["Plan Anual de Trabajo SST", "2"],
      ["Plan de Capacitación de Seguridad Vial", "3"],
      ["Plan de Mejoramiento", "4"],
      ["Plan de Trabajo COPASST", "5"]
    ]
  end

  private
    def set_project
      @project = Project.find(params[:id])
    end

    def project_params
      params.require(:project).permit(:name, :description, :object, :scope, :due_date, :status, :owner_id, :project_type_id, :company_id, :created_by_id)
    end
end
