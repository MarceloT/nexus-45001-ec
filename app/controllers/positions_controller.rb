class PositionsController < ApplicationController
  before_action :set_position, only: [:show, :edit, :update, :destroy]
  before_action :get_medical_profiles, :get_areas, :get_country, only: [:new, :create, :edit, :update]
  before_action :get_gender, only: [:get_dataset]
  respond_to :html, :json

  # GET /positions
  # GET /positions.json
  def index
    @sidebar_rigth_app = true
  end

  def diagram
    
  end

  def get_dataset
    items = Position.where(:company_id => @company.id).or(Position.where(:company_id => nil)).datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: i.name,
        code: i.code,
        gender: helpers.select_array(@genders, i.gender),
        age_min: i.age_min,
        age_max: i.age_max,
        active: i.active.present? ? "Si" : "No",
        edit: "#{helpers.link_to_show(position_path(i.id))} #{helpers.link_to_edit(edit_position_path(i.id))}"
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  # GET /positions/1
  # GET /positions/1.json
  def show
    respond_modal_with @position
  end

  # GET /positions/new
  def new
    @position = Position.new
    @position.company = @company if @company.present?
    @position.active = true
    respond_modal_with @position
  end

  # GET /positions/1/edit
  def edit
    respond_modal_with @position
  end

  # POST /positions
  # POST /positions.json
  def create
    @position = Position.create(position_params)
    Rails.logger.info @position.errors.inspect
    respond_modal_with @position, location: positions_path
  end

  # PATCH/PUT /positions/1
  # PATCH/PUT /positions/1.json
  def update
    @position.update(position_params)
    respond_modal_with @position, location: positions_path
  end

  # DELETE /positions/1
  # DELETE /positions/1.json
  def destroy
    @position.destroy
    respond_to do |format|
      format.html { redirect_to positions_url, notice: 'Position was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_position
      @position = Position.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def position_params
      params.require(:position).permit(:name, :code, :gender, :age_min, :age_max, :active, :number, :mission, :company_id, :area_id, :medical_profile_id)
    end
end
