require 'jasper-bridge'
class PatientsController < ApplicationController
  include Jasper::Bridge
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
  before_action :get_ci_type, :get_areas, :get_positions, :get_civil_states, :get_blood_type, :get_country, :get_provinces, :get_cities, :get_profession, :get_work_centers, only: [:new, :create, :edit, :update, :show, :import_nl_patient]
  before_action :get_gender, only: [:get_dataset, :show, :new, :create, :edit, :update]

  before_action :get_medical_types, only: [:show]
  

  respond_to :html

  def index
    @sidebar_rigth_app = true
    
    @patients = Patient.all

    if @company.present?
      @patients = @patients.where(:company_id => @company.id)
    end

    patients_by_gender = @patients.group(:gender).count
    @male_count   = patients_by_gender[1].to_i
    @female_count = patients_by_gender[2].to_i
    @none_count   = patients_by_gender[nil].to_i

    patients_by_civil_status = @patients.group(:civil_status).count
    @sol_count = patients_by_civil_status[1].to_i
    @cas_count = patients_by_civil_status[2].to_i
    @div_count = patients_by_civil_status[3].to_i
    @sep_count = patients_by_civil_status[4].to_i
    @viu_count = patients_by_civil_status[5].to_i
    @n_count   = patients_by_civil_status[nil].to_i


    respond_with(@patients)
  end

  def reports
    @sidebar_rigth_app = true
  end

  def print
    @patient = Patient.find(9)
    respond_to do |format|
      format.html {send_doc(@patient.to_xml, '/patient', 'patients.jasper', "patients", "pdf")}
      format.xml {render xml: @patient.to_xml}
    end
  end

  def get_dataset
    items = Patient.where(:company_id => @company.id).includes(:position)
    items = items.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      position_name = i.position.present? ? i.position.name : ''
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        full_name: i.full_name,
        name_1: i.full_name,
        last_name: i.full_name,
        last_name_1: i.full_name,
        ci: i.ci,
        position: helpers.truncate(position_name, :length => 50, :separator => ' '),
        gender: helpers.select_array(@genders, i.gender)[0],
        age: i.age,
        active: i.active.present? ? "Si" : "No",
        edit: "#{helpers.link_to_show(patient_path(i.id))} #{helpers.link_to_edit(edit_patient_path(i.id))}",
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_modal_with @patient
  end

  def new
    @patient = Patient.new
    @patient.active = true
    @patient.ci_type = 2
    @patient.company_id = @company.id if @company.present?
    respond_modal_with @patient
  end

  def edit
    respond_modal_with @patient
  end

  def create
    @patient = Patient.create(patient_params)
    respond_modal_with @patient, location: patients_path
  end

  def update
    @patient.update(patient_params)
    location = params[:url].present? ? params[:url] : root_path
    respond_modal_with @patient, location: location
  end

  def destroy
    # @patient.destroy
    @patient.active = false
    @patient.save
    respond_with(@patient)
  end

  def import_nl_patient
    # Call to api with TOKEN
    begin
      # Getting patient from API
      api_patient = Patient.import_from_api(params[:code], session[:import_credential_token])
      # Matching patient to Company
      @patient = Patient.new(api_patient.to_h)
      @patient.active = true
      @patient.company_id = @company.id if @company.present?
      render json: { patient: render_to_string('patients/_form', layout: false, locals: { patient: @patient }) }
    rescue RestClient::NotFound => enf
      render json: { company: enf }, status: 404
    rescue TypeError => te
      render json: { company: te }, status: 520
    rescue ActiveRecord::RecordNotUnique => nu
      render json: { company: nu }, status: 403
    rescue Exception => e
      Rails.logger.info "=================================================>"
      Rails.logger.info e.inspect
      render json: { company: e }, status: 500
    end
  end

  private
    def set_patient
      @patient = Patient.find(params[:id])
    end

    def patient_params
      params.require(:patient).permit(:name, :name_1, :last_name, :last_name_1, :ci, :ci_type, :gender, 
        :birth_date, :phone_1, :phone_2, :email, :civil_status, :nationality, :direction, :profession, 
        :photo, :blood_type, :active, :description, :company_id, :area_id, :position_id, :legacy_id,
        :work_center_id, :date_in, :date_out, :height, :weight, :province, :city)
    end
end
