class RisksController < ApplicationController
  before_action :set_risk, only: [:show, :edit, :update, :destroy]
  before_action :get_risk_categories, only: [:new, :create, :edit, :update]
  respond_to :html

  def index
    @sidebar_rigth_app = true
    # @risks = Risk.all
    # respond_with(@risks)
  end

  def get_dataset
    items = Risk.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        code: i.code.to_s,
        name: i.name,
        category_id: i.risk_category_id,
        category: "<div class='alert' style='background-color: #{i.risk_category.color}; margin-bottom: 0px; color: #fff;padding: 1px 5px;'>#{i.risk_category.name}</div>".html_safe,
        active: i.active.present? ? "Si" : "No",
        edit: helpers.link_to_edit(edit_risk_path(i.id))
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_with(@risk)
  end

  def new
    @risk = Risk.new
    @risk.active = true
    respond_modal_with @risk
  end

  def edit
    respond_modal_with @risk
  end

  def create
    @risk = Risk.create(risk_params)
    respond_modal_with @risk, location: risks_path
  end

  def update
    @risk.update(risk_params)
    respond_modal_with @risk, location: risks_path
  end

  def destroy
    # @risk.destroy
    @risk.active = false
    @risk.save
    respond_with(@risk)
  end

  private
    def set_risk
      @risk = Risk.find(params[:id])
    end

    def risk_params
      params.require(:risk).permit(:name, :code, :description, :risk_category_id)
    end
end
