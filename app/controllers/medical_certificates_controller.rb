class MedicalCertificatesController < ApplicationController
  before_action :set_medical_certificate, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @medical_certificates = MedicalCertificate.all
    respond_with(@medical_certificates)
  end



   def show
    respond_to do |f|
      f.html {respond_modal_with @medical_certificate}
      f.js {respond_modal_with @medical_certificate}
      f.pdf {send_data MedicalCertificate.print_data(@medical_certificate), filename:'receta.pdf', type: "application/pdf", disposition: :inline}
    end
  end

  def new
    @medical_certificate = MedicalCertificate.new
    @medical_certificate.patient_id = params[:patient_id] if params[:patient_id].present?
    @medical_certificate.created_by_id = current_user.id
    @medical_certificate.company_id = @company.id
    @medical_certificate.date = Time.now
    @medical_certificate.starts_date = Time.now
    @medical_certificate.ends_date = Time.now + 2.days
    @medical_certificate.days = 3
    @medical_certificate.medical_record_id = params[:medical_record_id] if params[:medical_record_id].present?
    respond_modal_with @medical_certificate
  end

  def edit
    respond_modal_with @medical_certificate
  end

  def create
    @medical_certificate = MedicalCertificate.create(medical_certificate_params)
    
    if @medical_certificate.medical_record_id.present?
      location = patient_record_medical_check_path(@medical_certificate.patient_id, @medical_certificate.medical_record_id)
    else
      location = medical_certificates_patient_record_path(@medical_certificate.patient_id)
    end

    respond_modal_with @medical_certificate, location: location
  end

  def update
    @medical_certificate.update(medical_certificate_params)

    
    respond_modal_with @medical_certificate, location: medical_certificates_patient_record_path(@medical_certificate.patient_id)
  end


  def destroy
    @medical_certificate.destroy
    respond_with(@medical_certificate)
  end

  private
    def set_medical_certificate
      @medical_certificate = MedicalCertificate.find(params[:id])
    end

    def medical_certificate_params
      params.require(:medical_certificate).permit(:date, :patient_id, :medical_record_id, :reason, :diagnostic_description, :days, :starts_date, :ends_date, :created_by_id, :document_id, :type_certificate, :company_id)
    end
end
