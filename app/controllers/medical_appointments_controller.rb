class MedicalAppointmentsController < ApplicationController
  before_action :set_medical_appointment, only: [:show, :edit, :update, :destroy]
  before_action :get_doctors, :get_patients, :get_ci_type, :get_positions, :get_profession,
    :get_companies, :get_areas, :get_medical_types, :get_exams, :get_gender, only: [:create, :new, :edit, :update, :index, :show, :today]

  respond_to :html

  def index
    @sidebar_rigth_app = true
    @medical_appointments = MedicalAppointment.where(company_id: @company.id)
    @medical_appointments_for_today = @medical_appointments.where('DATE(date) = ?', Date.today)
    #@medical_appointments_for_today = @medical_appointments.where(company_id: @company.id)
    @medical_appointments_for_today_render = @medical_appointments_for_today.order("(time) ASC").limit(3)
    if params[:patient_id].present?
      @medical_appointments = @medical_appointments.where(:patient_id => params[:patient_id])
      @patient_filter = true
    end
    if params[:doctor_id].present?
      @medical_appointments = @medical_appointments.where(:doctor_id => params[:doctor_id])
      @doctor_filter = true
    end
    if params[:medical_type_id].present?
      @medical_appointments = @medical_appointments.where(:medical_type_id => params[:medical_type_id])
      @medical_type_filter = true
    end
    #@medical_appointments = @medical_appointments.where(company_id: @company.id)
    @medical_appointments = @medical_appointments.map{|a|
      {
        id: a.id,
        title: a.patient.full_name,
        start: Time.parse("#{a.date.to_date} #{a.time.strftime("%H:%M")}"),
        end: a.date,
        update_url: medical_appointment_path(a),
        edit_url: edit_medical_appointment_path(a),
        url: "",
        color: a.medical_type_color
      }
    }

    @today_left_class = "col-md-12"
    @today_right_class = "col-md-12 text-center"

    respond_to do |format|
      format.html { @medical_appointments }
      format.json 
    end
  end

  def today
    medical_appointments_for_today = MedicalAppointment.where('DATE(date) = ?', Date.today)
    #medical_appointments_for_today = medical_appointments.where(company_id: @company.id)
    @medical_appointments_for_today_render = medical_appointments_for_today.order("(time) ASC")

    @today_left_class = "col-md-11"
    @today_right_class = "col-md-1 text-right"

    respond_modal_with @medical_appointments_for_today_render
  end

  def show
    # respond_with(@medical_appointment)
    respond_to do |format|
      format.html {respond_modal_with @medical_appointment}
      format.js {respond_modal_with @medical_appointment}
      format.pdf do
        render pdf: @medical_appointment.patient.full_name, font_size: 20, disable_links: true, lowquality: true
      end
    end
  end

  def new
    @medical_appointment = MedicalAppointment.new
    # [["Ingreso", "0"],["Resultados Adjuntos", "1"],["Validado", "2"],["Finalizado", "3"],["Anulado", "4"]]
    @medical_appointment.status = 0
    @medical_appointment.medical_type_id = 1
    @medical_appointment.date = params[:start].present? ? params[:start].to_date : Time.now
    @medical_appointment.time = params[:start].present? ? params[:start].to_datetime.strftime("%H:%M") : Time.now.strftime("%H:00") 

    @medical_appointment.date = Time.now unless @medical_appointment.date.present?
    @medical_appointment.company_id = @company.id

    if params[:patient_id].present?
      Rails.logger.info "-====>>>>>>> params[:patient_id]: #{params.inspect}"
      @medical_appointment.patient = Patient.find(params[:patient_id])
      if @medical_appointment.patient && params[:position_id].present? 
        @patient = @medical_appointment.patient
        @patient.position_id = params[:position_id]
        @patient.save(:validate => false)
      end
      @medical_appointment.date = params[:date]
      @medical_appointment.time = params[:time]
      @medical_appointment.doctor_id = params[:doctor_id]
      @medical_appointment.reason = params[:reason]
    elsif params[:ci].present?
      Rails.logger.info "-====>>>>>>> on ci"
      patient = Patient.new
      patient.active = true
      patient.ci_type = 2
      patient.ci = params[:ci]
      patient.save(:validate => false)
      @medical_appointment.patient_id = patient.id
      Rails.logger.info "-====>>>>>>> @medical_appointment.patient.ci: #{@medical_appointment.patient.ci}"
    else
      @medical_appointment.patient = Patient.new
      @medical_appointment.patient.active = true
      @medical_appointment.patient.ci_type = 2
    end

    respond_modal_with @medical_appointment
  end

  def edit
    @medical_appointment.time = @medical_appointment.time.to_datetime.strftime("%H:%M")
    @medical_appointment.company_id = @company.id
    respond_modal_with @medical_appointment
  end

  def create
    @medical_appointment = MedicalAppointment.create(medical_appointment_params)
    respond_modal_with @medical_appointment, location: medical_appointments_path
  end

  def update
    # [["Ingreso", "0"],["Resultados Adjuntos", "1"],["Validado", "2"],["Finalizado", "3"],["Anulado", "4"]]
    @medical_appointment.update(medical_appointment_params)
    if params[:save]
      location = medical_appointments_path
    elsif params[:save_and_generate]
      location = medical_appointments_path
      @medical_appointment.status = 3
      medical_record = MedicalRecord.new
      medical_record.doctor_id = @medical_appointment.doctor_id
      medical_record.patient_id = @medical_appointment.patient_id
      medical_record.medical_type_id = @medical_appointment.medical_type_id
      medical_record.reason = @medical_appointment.reason
      medical_record.date = Time.now
      if medical_record.save
        @medical_appointment.medical_appointment_exams.each do |appointment_exam|
          MedicalRecordExam.create({date: DateTime.now, medical_record_id: medical_record.id, exam_id: appointment_exam.id})
        end
        @medical_appointment.medical_record_id = medical_record.id
        @medical_appointment.save
        # location = medical_record_path(medical_record.id)        
        location = patient_record_medical_check_path(medical_record.patient_id, medical_record.id)
      else
        location = medical_appointments_path
      end
      
    elsif params[:cancel]
      
      @medical_appointment.status = 0
      @medical_appointment.save
      location = medical_appointments_path
    end
        
        
    respond_modal_with @medical_appointment, location: location
  end

  def destroy
    # @medical_appointment.destroy
    # @medical_appointment
    respond_with(@medical_appointment)
  end

  private
    def set_medical_appointment
      @medical_appointment = MedicalAppointment.find(params[:id])
    end

    def medical_appointment_params
      params.require(:medical_appointment).permit(:date, :time, :medical_type_id, :duration, :doctor_id, :patient_id, :reason, :company_id,
        patient_attributes:[:id, :name, :last_name, :ci, :ci_type, :position_id, :gender_id, :birth_date, :company_id, :area_id],
        medical_appointment_exams_attributes: [:id, :exam_id, :description, :_destroy],)
    end
end
