class IndicatorsController < ApplicationController
  respond_to :html

  before_action :get_medical_types

  def index

    # @sidebar_rigth_app = true

    @medical_records = MedicalRecord.where(initial: false, status: 3).order(updated_at: :asc).group_by(&:patient)

    xlsx_path = "#{Rails.root}/public/hco_template/certificate.xlsx"

    # Libreconv.convert(xlsx_path, xlsx_path.gsub(".xlsx", ".pdf"),'/Applications/LibreOffice.app/Contents/MacOS/soffice', 'pdf:writer_pdf_Export')

    respond_to do |f|
      f.html {respond_modal_with @medical_certificate}
      f.js {respond_modal_with @medical_certificate}
      f.pdf {send_data MedicalRecord.print_data(MedicalRecord.last), filename:'receta.pdf', type: "application/pdf", disposition: :inline}
    end

  end

  def absenteeism
    @sidebar_rigth_app = true

    year   = "2020"
    months = [
      ["Enero", "1"], ["Febrero", "2"], ["Marzo", "3"], ["Abril", "4"], ["Mayo", "5"], ["Junio", "6"],
      ["Julio", "7"], ["Agosto", "8"], ["Septiembre", "9"], ["Octubre", "10"], ["Noviembre", "11"], ["Diciembre", "12"]
    ]

    work_hours  = Patient.where(:active => true).count * 8 * 5 * 4
    # lost_hours  = 0
    # total_hours = 0

    # by type consulta medica
    @medical_types_data = {}
    months.each do |m|
      @medical_types_data["#{m[0]}"] = []
      @medical_types.each do |mt|
        # TODO: filter by company
        lost_hours = MedicalRecord.where(:medical_type_id => mt[1])
                                  .where("EXTRACT(MONTH FROM date) = ?", m[1]).count

        type_values = {
          :type_record => "#{mt[0]}",
          :work_hours => 0,
          :lost_hours => lost_hours,
          :total_hours => 0
        }
        @medical_types_data["#{m[0]}"] << type_values
      end
    end
    @medical_types_data
  end
end
