class PersonalProtectionEntriesController < ApplicationController
  before_action :set_personal_protection_entry, only: [:show, :edit, :update, :destroy]
  before_action :get_patients, :get_personal_protections, :get_work_centers, only: [:new, :create, :edit, :update]

  respond_to :html

  def index
    @sidebar_rigth_app = true
    @personal_protection_entries = PersonalProtectionEntry.all
    respond_with(@personal_protection_entries)
  end

  def get_dataset
    items = PersonalProtectionEntry.where(company_id: @company.id).datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id_to_s,
        user: i.user.name,
        patient: i.patient.full_name,
        date: i.date.strftime(@date_format),
        personal_protection: i.personal_protections.map{|p| p.name},
        edit: "#{helpers.link_to_edit(edit_personal_protection_entry_path(i.id))} #{helpers.link_to_show_print(personal_protection_entry_path(i.id))}"
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_to do |f|
      f.html {respond_modal_with @personal_protection_entry}
      f.js {respond_modal_with @personal_protection_entry}
      f.pdf {send_data PersonalProtectionEntry.print_data(@personal_protection_entry), filename:'acta_de_entrega.pdf', type: "application/pdf", disposition: :inline}
    end
  end

  def new
    @personal_protection_entry = PersonalProtectionEntry.new
    @personal_protection_entry.company_id = @company.id
    @personal_protection_entry.date = Time.now
    @personal_protection_entry.user_id = current_user.id
    @personal_protection_entry.personal_protection_entry_personal_protections.new
    respond_modal_with @personal_protection_entry
  end

  def edit
    respond_modal_with @personal_protection_entry
  end

  def create
    @personal_protection_entry = PersonalProtectionEntry.create(personal_protection_entry_params)
    respond_modal_with @personal_protection_entry, location: personal_protection_entries_path
  end

  def update
    @personal_protection_entry.update(personal_protection_entry_params)
    respond_modal_with @personal_protection_entry, location: personal_protection_entries_path
  end

  def destroy
    @personal_protection_entry.destroy
    respond_with(@personal_protection_entry)
  end

  def get_personal_protections
    @personal_protections = PersonalProtection.where(:company_id => @company.id)
  end

  private
    def set_personal_protection_entry
      @personal_protection_entry = PersonalProtectionEntry.find(params[:id])
    end

    def personal_protection_entry_params
      params.require(:personal_protection_entry).permit(
        :origin_work_center_id, :destination_work_center_id, :patient_id, :date, :signature, :company_id, :user_id, :description,
        personal_protection_entry_personal_protections_attributes: [:id, :personal_protection_id, :quantity, :max_date, :description, :_destroy]
        )
    end
end
