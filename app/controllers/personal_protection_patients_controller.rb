class PersonalProtectionPatientsController < ApplicationController
  before_action :set_personal_protection_patient, only: [:show, :edit, :update, :destroy]
  before_action :get_patients, :get_personal_protections, only: [:new, :create, :edit, :update]

  respond_to :html

  def index
    @sidebar_rigth_app = true
    @personal_protection_patients = PersonalProtectionPatient.all
    respond_with(@personal_protection_patients)
  end

  def show
    respond_modal_with @personal_protection_patient
  end

  def new
    @personal_protection_patient = PersonalProtectionPatient.new
    @personal_protection_patient.company_id = @company.id
    @personal_protection_patient.personal_protection_id = params[:personal_protection_id]
    @personal_protection_patient.date = Time.now
    respond_modal_with @personal_protection_patient
  end

  def edit
    respond_modal_with @personal_protection_patient
  end

  def create
    @personal_protection_patient = PersonalProtectionPatient.create(personal_protection_patient_params)
    respond_modal_with @personal_protection_patient, location: personal_protections_path
  end

  def update
    @personal_protection_patient.update(personal_protection_patient_params)
    respond_modal_with @personal_protection_patient, location: personal_protections_path
  end

  def get_personal_protections
    @personal_protections = PersonalProtection.where(:company_id => @company.id)
  end

  private
    def set_personal_protection_patient
      @personal_protection_patient = PersonalProtectionPatient.find(params[:id])
    end

    def personal_protection_patient_params
      params.require(:personal_protection_patient).permit(:personal_protection_id, :patient_id, :date, :signature, :company_id)
    end
end
