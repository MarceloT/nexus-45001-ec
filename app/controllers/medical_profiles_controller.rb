class MedicalProfilesController < ApplicationController
  before_action :set_medical_profile, only: [:show, :edit, :update, :destroy]
  before_action :get_exams, only: [:create, :new, :edit, :update, :get_exams, :index]
  
  respond_to :html

  def index
    @sidebar_rigth_app = true
    @medical_profiles = MedicalProfile.all
    respond_with(@medical_profiles)
  end

  def get_exams_1

    return @exams.to_json
  end

  def get_dataset
    items = MedicalProfile.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: i.name,
        code: i.code,
        exams: i.exams.any? ? i.exams.map{|e| e.full_name}.join(", ") : "Sin asignar",
        active: i.active.present? ? "Si" : "No",
        edit: helpers.link_to_edit(edit_medical_profile_path(i.id))
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_with(@medical_profile)
  end

  def new
    @medical_profile = MedicalProfile.new
    @medical_profile.active = true
    respond_modal_with @medical_profile
  end

  def edit
    respond_modal_with @medical_profile
  end

  def create
    @medical_profile = MedicalProfile.create(medical_profile_params.except(:exam_ids))
    @medical_profile.update(medical_profile_params)
    respond_modal_with @medical_profile, location: medical_profiles_path
  end

  def update
    @medical_profile.update(medical_profile_params)
    respond_modal_with @medical_profile, location: medical_profiles_path
  end

  def destroy
    @medical_profile.destroy
    respond_with(@medical_profile)
  end

  private
    def set_medical_profile
      @medical_profile = MedicalProfile.find(params[:id])
    end

    def medical_profile_params
      params.require(:medical_profile).permit(:name, :code, :active, :description, :company_id, :medical_profile_id, exam_ids: [])
    end
end
