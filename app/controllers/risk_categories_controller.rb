class RiskCategoriesController < ApplicationController
  before_action :set_risk_category, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @sidebar_rigth_app = true
    # @risk_categories = RiskCategory.all
    # respond_with(@risk_categories)
  end

  def get_dataset
    items = RiskCategory.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: i.name,
        color: "<div class='alert' style='background-color: #{i.color}; margin-bottom: 0px;'></div>".html_safe,
        active: i.active.present? ? "Si" : "No",
        edit: helpers.link_to_edit(edit_risk_category_path(i.id))
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_with(@risk_category)
  end

  def new
    @risk_category = RiskCategory.new
    respond_modal_with @risk_category
  end

  def edit
    respond_modal_with @risk_category
  end

  def create
    @risk_category = RiskCategory.create(risk_category_params)
    respond_modal_with @risk_category, location: risk_categories_path
  end

  def update
    @risk_category.update(risk_category_params)
    respond_modal_with @risk_category, location: risk_categories_path
  end

  def destroy
    # @risk_category.destroy
    @risk_category.active = false
    @risk_category.save
    respond_with(@risk_category)
  end

  private
    def set_risk_category
      @risk_category = RiskCategory.find(params[:id])
    end

    def risk_category_params
      params.require(:risk_category).permit(:name, :color, :active, :description)
    end
end
