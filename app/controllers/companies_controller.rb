class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @sidebar_rigth_app = true
  end

  def get_dataset
    items = Company.order(:name).datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: i.name,
        code: i.code,
        active: i.active.present? ? "Si" : "No",
        edit: helpers.link_to_edit(edit_company_path(i.id))
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_with(@company)
  end

  def new
    @company = Company.new
    respond_modal_with @company
  end

  def edit
    respond_modal_with @company
  end

  def create
    @company = Company.create(company_params)
    Area.create(name: @company.name, code: @company.code, active: true, parent_id: 0, company_id: @company.id)
    respond_modal_with @company, location: companies_path
  end

  def update
    @company.update(company_params)
    respond_modal_with @company, location: companies_path
  end

  def save_current_company
    @company.active = false
    @company.save
    new_company = Company.find(params[:company_id])
    new_company.active = true
    new_company.save
    redirect_to root_path
  end

  def destroy
    @company.destroy
    respond_with(@company)
  end

  def import_nl_source
    # Call to api with TOKEN
    begin
      # Getting source from API
      api_source = Company.import_from_api(params[:code], session[:import_credential_token])

      # Matching Source to Company
      Rails.logger.info api_source.to_h.inspect
      @company = Company.new(api_source.to_h)
      render json: { company: render_to_string('companies/_form', layout: false, locals: {company: @company, hide_footer: true}) }
    rescue RestClient::NotFound => enf
      render json: { company: enf }, status: 404
    rescue TypeError => te
      render json: { company: te }, status: 520
    rescue ActiveRecord::RecordNotUnique => nu
      render json: { company: nu }, status: 403
    rescue Exception => e
      Rails.logger.info "=================================================>"
      Rails.logger.info e.inspect
      render json: { company: e }, status: 500
    end
  end

  private
    def set_company
      @company = Company.find(params[:id])
    end

    def company_params
      params.require(:company).permit(:name, :code, :active, :mission, :vision, :logo, :legacy_id)
    end
end
