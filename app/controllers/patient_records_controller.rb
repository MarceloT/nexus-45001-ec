class PatientRecordsController < ApplicationController
  before_action :set_patient, only: [:show, :edit, :update, :destroy, :vaccines, :recipes, :laboratories, :medical_history, :medical_checks, :medical_certificates]
  before_action :get_ci_type, :get_areas, :get_positions, :get_civil_states, :get_blood_type, :get_country, :get_provinces, :get_cities, :get_profession, only: [:new, :create, :edit, :update, :show, :import_nl_patient, :vaccines, :recipes, :laboratories, :medical_history, :medical_checks, :medical_certificates]
  before_action :get_gender, only: [:get_dataset, :show, :new, :create, :edit, :update]
  before_action :get_medical_types, only: [:show]

  before_action :get_personal_histories, :get_habits, 
                :get_lifestyles, :get_family_histories, :get_system_revisions, :get_vital_constants, 
                :get_exams, :get_diagnostics, :get_physical_exams, 
                :get_trauma_types, :get_trauma_curvature_types, only: [:medical_history]

  def index
    @sidebar_rigth_app = true
  end

  def get_dataset
    items = Patient.where(:active => true, company_id: @company.id).includes(:health_cycle)
    items = items.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      position_name = i.area.present? ? i.area.name : ''
      {
        class_name: i.class.to_s.underscore,
        id: "##{i.ci}",
        full_name: "<i class='material-icons icon-16pt'>person</i>#{i.full_name}",
        name_1: i.full_name,
        last_name: i.full_name,
        last_name_1: i.full_name,
        ci: i.ci,
        company: i.company.present? ? "<i class='material-icons icon-16pt'>business</i>#{i.company.name}" : 'Sin Asignar',
        work_center: i.work_center.present? ? "<i class='material-icons icon-16pt'>location_on</i>#{i.work_center.name}" : 'Sin Asignar',
        gender: helpers.select_array(@genders, i.gender)[0],
        age: i.age,
        complete: i.health_cycle.complete.present? ? "Si" : "No",
        edit: helpers.link_to('<i class="material-icons icon-16pt">view_list</i> Abrir'.html_safe , patient_record_path(i.id))
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    @sidebar_rigth_app = true
  end

  def vaccines
    @sidebar_rigth_app = true
  end

  def recipes
    @sidebar_rigth_app = true
    @recipies = Recipe.where(:patient_id => @patient.id)
  end

  def laboratories
    @sidebar_rigth_app = true
  end

  def medical_checks
    @sidebar_rigth_app = true
    @medical_records   = @patient.medical_records.where(:initial => false).to_a.group_by_year(&:date)
  end

  def medical_history
    @sidebar_rigth_app  = true
    @medical_record     = @patient.medical_records.where(:initial => true).last
    @medical_record     = MedicalRecord.create({:initial => true, :patient_id => @patient.id, :company_id => @company.id, :date => Time.now}) unless @medical_record.present?

    @medical_record.build_medical_record_obstetric_gineco_background unless @medical_record.medical_record_obstetric_gineco_background.present?
    
    @available_sections = ['c', 'd']

    @medical_record.toxic_habit_ids    = [1, 2, 3] unless @medical_record.toxic_habit_ids.any?
    @medical_record.lifestyle_ids      = [1, 2, 2, 2] unless @medical_record.lifestyle_ids.any?
    @medical_record.family_history_ids = [1, 2, 3, 4, 5, 6, 7, 8] unless @medical_record.family_history_ids.any?
  end

  def medical_certificates
    @sidebar_rigth_app = true
    @medical_certificates = MedicalCertificate.where(:patient_id => @patient.id)
  end

  private

    def set_patient
      @patient = Patient.find(params[:id])
    end

end
