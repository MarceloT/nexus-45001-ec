class RecipesController < ApplicationController
  before_action :set_recipe, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @recipes = Recipe.all
    respond_with(@recipes)
  end

  def show
    respond_to do |f|
      f.html {respond_modal_with @recipe}
      f.js {respond_modal_with @recipe}
      f.pdf {send_data Recipe.print_data(@recipe), filename:'receta.pdf', type: "application/pdf", disposition: :inline}
    end
  end

  def new
    @recipe = Recipe.new
    @recipe.patient_id = params[:patient_id] if params[:patient_id].present?
    @recipe.doctor_id = current_user.id
    @recipe.medical_record_id = params[:medical_record_id] if params[:medical_record_id].present?
    respond_modal_with @recipe
  end

  def edit
    respond_modal_with @recipe
  end

  def create
    @recipe = Recipe.create(recipe_params)
    if @recipe.medical_record_id.present?
      location = patient_record_medical_check_path(@recipe.patient_id, @recipe.medical_record_id)
    else
      location = recipes_patient_record_path(@medical_certificate.patient_id)
    end
    respond_modal_with @recipe, location: location
  end

  def update
    @recipe.update(recipe_params)
    respond_modal_with @recipe, location: recipes_patient_record_path(@recipe.patient_id)
  end

  def destroy
    @recipe.destroy
    respond_with(@recipe)
  end

  private
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    def recipe_params
      params.require(:recipe).permit(:diagnostic_description, :medicines, :indications, :medical_record_id, :patient_id, :doctor_id)
    end
end
