class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]
  before_action :get_patients, only: [:new, :create, :edit, :update]

  respond_to :html

  def index
    @tasks = Task.all
    respond_with(@tasks)
  end

  def show
    respond_with(@task)
  end

   def new
    @task = Task.new
    @task.project_id = params[:project_id]
    @task.number = @task.project.generate_task_number
    @task.status = :active
    @task.start_date = Time.now
    @task.due_date = Time.now + 1.week
    respond_modal_with @task
  end

  def edit
    @task.status = :complete if @task.progress.to_i == 100 && !@task.overdue?
    @task.status = :active   if @task.progress.to_i < 100 && !@task.overdue?
    respond_modal_with @task
  end

  def create
    @task = Task.create(task_params)
    respond_modal_with @task, location: project_path(@task.project_id)
  end

  def update
    @task.update(task_params)
    respond_modal_with @task, location: project_path(@task.project_id)
  end

  def destroy
    @task.destroy
    respond_with(@task)
  end

  private
    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:name, :description, :start_date, :due_date, :number, :progress, :status, :owner_id, :project_id)
    end
end
