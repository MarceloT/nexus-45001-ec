class ExamsController < ApplicationController
  before_action :set_exam, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @sidebar_rigth_app = true
    # @exams = Exam.order(:name)
  end

  def get_dataset
    items = Exam.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: i.name,
        code: i.code,
        active: i.active.present? ? "Si" : "No",
        edit: helpers.link_to_edit(edit_exam_path(i.id))
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_with(@exam)
  end

  def new
    @exam = Exam.new
    respond_modal_with @exam
  end

  def edit
    respond_modal_with @exam
  end

  def create
    @exam = Exam.create(exam_params)
    respond_modal_with @exam, location: exams_path
  end

  def update
    @exam.update(exam_params)
    respond_modal_with @exam, location: exams_path
  end

  def destroy
    @exam.destroy
    respond_with(@exam)
  end

  private
    def set_exam
      @exam = Exam.find(params[:id])
    end

    def exam_params
      params.require(:exam).permit(:name, :code, :active, :description)
    end
end
