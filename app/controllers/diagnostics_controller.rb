class DiagnosticsController < ApplicationController
  before_action :set_diagnostic, only: [:show, :edit, :update, :destroy]

  # GET /diagnostics
  # GET /diagnostics.json
  def index
    @sidebar_rigth_app = true
    # @diagnostics = Diagnostic.all
  end

  def get_dataset
    items = Diagnostic.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: i.name,
        code: i.code,
        edit: helpers.link_to_edit(edit_diagnostic_path(i.id))
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_with(@diagnostic)
  end

  def new
    @diagnostic = Diagnostic.new
    respond_modal_with @diagnostic
  end

  def edit
    respond_modal_with @diagnostic
  end

  def create
    @diagnostic = Diagnostic.create(diagnostic_params)
    respond_modal_with @diagnostic, location: diagnostics_path
  end

  def update
    @diagnostic.update(diagnostic_params)
    respond_modal_with @diagnostic, location: diagnostics_path
  end

  # DELETE /diagnostics/1
  # DELETE /diagnostics/1.json
  def destroy
    @diagnostic.destroy
    respond_to do |format|
      format.html { redirect_to diagnostics_url, notice: 'Diagnostic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diagnostic
      @diagnostic = Diagnostic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diagnostic_params
      params.require(:diagnostic).permit(:name, :code)
    end
end
