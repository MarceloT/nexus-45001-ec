require "application_responder"
require 'jasper-bridge'
class ApplicationController < ActionController::Base
  include Jasper::Bridge
  before_action :authenticate_user!
  before_action :set_date_format
  self.responder = ApplicationResponder
  respond_to :html

  before_action :get_current_company, :get_companies

  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource)
    begin
      # save user token
      user = current_user
      response = RestClient.post "#{Rails.configuration.exchange_api_url}#{Rails.configuration.exchange_api_login}", {username: "demo", password: "ecua2020"}
      api_credential = JSON.parse(response.body)
      session[:import_credential_token] = api_credential["token"]
      # go to desired path or root
    rescue => error
      Rails.logger.info "====>>>> error: #{error}"
    end
      stored_location_for(resource) || root_path
  end

  def get_current_company
    @company = Company.where(:active => true).last
    @company = Company.last unless @company.present?   
  end

  def get_companies
    @all_companies = Company.order(:name)
    @companies = @all_companies.where(:id => @company.id) if @company.present?
  end

  def get_exams
    @all_exams = Exam.order(:name)
    @exams = @all_exams
  end

  def get_areas
    @all_areas = Area.where(:active => true).order(:name)
    @areas = @all_areas.where(:company_id => @company.id) if @company.present?
  end

  def get_processes
    @all_processes = CompanyProcess.where(:active => true).order(:name)
    @processes = @all_processes.where(:company_id => @company.id) if @company.present?
  end

  def get_activities
    @activities = Activity.all
  end

  def get_positions
    @all_positions = Position.where(:active => true).order(:name)
    @positions = @all_positions.where(:company_id => @company.id).or(@all_positions.where(:company_id => nil)) if @company.present?
  end

  def get_patients
    @all_patients = Patient.where(:active => true).order(:name)
    @patients = @all_patients.where(:company_id => @company.id) if @company.present?
  end

  def get_medical_profiles
    @all_medical_profiles = MedicalProfile.where(:active => true).order(:name)
    @medical_profiles = @all_medical_profiles.where(:company_id => @company.id) if @company.present?
  end

  def get_user
    @all_user = User.where(:active => true).order(:name)
  end

  def get_work_centers
    @work_centers = WorkCenter.where(:company_id => @company.id) if @company.present?
  end

  def get_doctors
    @doctors = Role.find(2).users
  end
  
  def get_ci_type
    @ci_types = [["Cédula de ciudadanía (CC)", "1"],["Cédula de identidad (CI)", "2"],["Tarjeta pasaporte (TP)", "3"],["Otros", "4"]]
  end

  def get_api_doctors
    nl_api   = NextLabApi.new(session[:import_credential_token])
    api_doctors = nl_api.get_api_doctors
    @api_doctors = Array.new
    api_doctors.each do |api_doctor|
      @api_doctors << [api_doctor["nom_med"], api_doctor["legacy_id"]]
    end
  end

  def get_gender
    @genders = [["Masculino", "1"],["Femenino", "2"],["No definido", "3"]]
  end

  def get_roles
    # @roles = [["Administrador", "1"],["Doctor", "2"],["Técnico", "3"]]
    @roles = [["Doctor", "2"],["Técnico", "3"]]
  end

  def get_civil_states
    @civil_states = [["Soltero", "1"],["Casado", "2"],["Divorsiado", "3"],["Separado", "4"],["Viudo", "5"],["Unión Libre", "6"]]
  end

  def get_blood_type
    @blood_types = [["O negativo", "1"],["O positivo", "2"],["A negativo", "3"],["A positivo", "4"],["B negativo", "5"],["B positivo", "6"],["AB negativo", "7"],["AB positivo", "8"]]
  end

  def get_profession
    @professions = [["Sin estudios", "1"],["Primarios", "2"],["Secundarios", "3"],["Profesionales", "4"],["Medio-superiores", "5"],["Superiores", "6"],["Post Grado", "7"]]
  end

  def get_medical_types
    @medical_types = [
                      ["Medicina Ocupacional - Preocupacional", "0", "#48BA16"],
                      ["Medicina Ocupacional - Periódica", "1", "#51A7C5"],
                      ["Medicina Ocupacional - Reintegro", "2", "#ffc107"],
                      ["Medicina Ocupacional - Retiro", "3", "#dc3545"],
                      ["Valoración Especialidades", "4", "#59009a"],
                      ["Control Médico", "5", "#dc7345"]
                    ]
  end

  def get_medical_record_statues
    @medical_types = [["Preocupacional", "0"],["Periódica", "1"],["Reintegro", "2"],["Retiro", "3"],["Laboratorio", "4"]]
  end

  def get_medical_status
    @medical_status = [["Nuevo", "0"],["En proceso", "1"],["Finalizado", "2"]]
  end

  def get_appoitment_status
    @appoitment_status = [["Ingreso", "0"],["Resultados Adjuntos", "1"],["Validado", "2"],["Finalizado", "3"],["Anulado", "4"]]
  end

  def get_trauma_types
    @trauma_types = [['Ocupacional','ocupacional']]
  end

  def get_trauma_curvature_types
    @trauma_curvature_types = [['Normal','normal']]
  end

  def get_country
    @countries = COUNTRY
  end

  def get_provinces
    @provinces = PROVINCES
  end

  def get_cities
    @cities = CITIES
  end

  def get_order_status
    @status = [["Pendiente", "1"],["Aprobado", "2"],["Anulado", "3"]]
  end

  # Medical Records

  def get_personal_histories
    @personal_histories = PersonalHistory.select(:id, :name).all
  end

  def get_habits
    @toxic_habits = ToxicHabit.select(:id, :name).all
  end

  def get_lifestyles
    @lifestyles = Lifestyle.select(:id, :name).all
  end

  def get_family_histories
    @family_histories = FamilyHistory.select(:id, :name).all
  end

  def get_system_revisions
    @system_revisions = SystemRevision.select(:id, :name).all
  end

  def get_vital_constants
    @vital_constants = VitalConstant.select(:id, :name).all
  end

  def get_diagnostics
    @diagnostics = Diagnostic.select(:id, :name, :code).all
  end

  def get_physical_exams
    @physical_exams = PhysicalExam.select(:id, :name).all
  end

  def get_risk_categories
    @risk_categories = RiskCategory.select(:id, :name, :color).order(:id).all
  end

  def get_risks
    @risks = Risk.all
  end

  def get_risk_evaluation_values
    @reference_values = [["(0) No aplica", "0"],
                         ["(0.1) Practicamente imposible (posibilidad 1 en 1 000.000)", "0.1"],
                         ["(0.5) 'Extremadamente remota pero concebible, no ha pasado en años'", "0.5"],
                         ["(1) Sería una coincidencia remotamente posiblem, se sabe que ha ocurrido", "1.0"],
                         ["(3) Sería una consecuencia o coincidencia rara", "3.0"],
                         ["(6) Es completamante posible, no sería nada extraño, 50% posible", "6.0"],
                         ["(10) Es resultado más posible y esperado, si se presenta la situación de Riesgo", "10.0"]]

    @medium_values    = [["(0) No aplica", "0"],
                         ["(1) Pequeñas heridas, contusiones, golpes, pequeños daños", "1.0"],
                         ["(5) Lesiones con baja no graves", "5.0"],
                         ["(15) Lesiones extremadamente graves (amputación, invalidez permanente)", "15.0"],
                         ["(25) Muerte, daños de 10.000 a 50.000 dólares", "25.0"],
                         ["(50) Varias muertes daños desde 50.001 a 100.000 dólares", "50.0"],
                         ["(100) Catástrofe, numerosas muertes, grandes daños", "100.0"]]
    @exposition_values =[["(0) No aplica", "0"],
                         ["(0.5) Remotamente posible (no se conoce que haya ocurrido)", "0.5"],
                         ["(1) Raramente (se ha sabido que ha ocurrido)", "1.0"],
                         ["(2) Irregularmente (1 vez/mes - 1 vez/año)", "2.0"],
                         ["(3) Ocasionalmente (1 vez/semana - 1 vez/año)", "3.0"],
                         ["(6) Frecuentemente (1 vez al día)", "6.0"],
                         ["(10) Continuamente (o muchas veces al día)", "10.0"]]
    
    @probability_values = [["Baja (B)", "0.0"],
                         ["Media (M)", "1.0"],
                         ["Alta (A)", "2.0"]]

    @consequence_values = [["Ligeramente Dañino (LD)", "0.0"],
                         ["Dañino (D)", "1.0"],
                         ["Extremadamente Dañino (LD)", "2.0"]]
  end

  def set_date_format
    @date_format = "%d-%m-%Y"
    @date_time_format = "%d-%m-%Y %H:%M"
    @sidebar_rigth_app = false
  end

  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
  end
end
