class CommissionsController < ApplicationController
  before_action :set_commission, only: [:show, :edit, :update, :destroy]
  before_action :get_work_centers, :get_patients, only: [:new, :create, :edit, :update]
  before_action :get_type_commissions, only: [:new, :create, :edit, :update, :show, :get_dataset]

  respond_to :html

  def index
    @sidebar_rigth_app = true
  end

  def get_dataset
    items = Commission.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        work_center: i.work_center.name,
        type_commission: helpers.select_array(@type_commissions, i.type_commission),
        date: i.date.strftime(@date_format),
        active: i.active.present? ? "Si" : "No",
        edit: "#{helpers.link_to_show(commission_path(i.id))} #{helpers.link_to_edit(edit_commission_path(i.id))}"
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_modal_with @commission
  end

  def new
    @commission = Commission.new
    @commission.type_commission = 1
    @commission.president = 1
    @commission.active = true
    @commission.nro_members = 0
    @commission.date = Time.now
    @commission.company_id = @company.id
    respond_modal_with @commission
  end

  def edit
    respond_modal_with @commission
  end

  def create
    @commission = Commission.create(commission_params)
    respond_modal_with @commission, location: commissions_path
  end

  def update
    @commission.update(commission_params)
    respond_modal_with @commission, location: commissions_path
  end

  def destroy
    @commission.destroy
    respond_with(@commission)
  end

  def get_type_commissions
    @type_commissions = [["COPASST", "1"],["SCOPASST", "2"],["Delegado de Seg.", "3"]]
  end

  private
    def set_commission
      @commission = Commission.find(params[:id])
    end

    def commission_params
      params.require(:commission).permit(:work_center_id, :company_id, :nro_members, :date, :active, :type_commission, :member_1, :member_2, :member_3, :member_4, :member_5, :member_6, :member_7, :member_8, :member_9, :member_10, :member_11, :member_12, :president, :delagate)
    end
end
