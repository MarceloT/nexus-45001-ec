class AreasController < ApplicationController
  before_action :set_area, only: [:show, :edit, :update, :destroy]
  before_action :get_patients, :get_areas, only: [:new, :create, :edit, :update]

  respond_to :html

  def index
    @sidebar_rigth_app = true
    @main_area = Area.where(:company_id => @company.id, :parent_id => 0).last
    respond_with(@main_area)
  end

  def show
    respond_with(@area)
  end

  def new
    @area = Area.new
    respond_modal_with @area
  end

  def edit
    respond_modal_with @area
  end

  def create
    @area = Area.create(area_params)
    respond_modal_with @area, location: areas_path
  end

  def update
    @area.update(area_params)
    respond_modal_with @area, location: areas_path
  end

  def destroy
    @area.destroy
    # respond_with(@area)
  end

  private
    def set_area
      @area = Area.find(params[:id])
    end

    def area_params
      params.require(:area).permit(:id, :name, :code, :active, :description, :parent_id, :user_id, :company_id)
    end
end
