class MedicalResultsController < ApplicationController
  include ActionView::Helpers::NumberHelper
  respond_to :html

  def index
    @sidebar_rigth_app = true

    @health_cycles = HealthCycle.where(:company_id => @company.id, year: "2020", active: true)
    complete = @health_cycles.where(:complete => true).count
    incomplete = @health_cycles.where(:complete => false).count
    total = @health_cycles.count
    complete_percentage = total > 0 ? ((complete * 100) / total) : 0
    incomplete_percentage = total > 0 ? ((incomplete * 100) / total) : 0

    @health_cycle_data = {
      :complete => complete,
      :incomplete => incomplete,
      :total => total,
      :complete_percentage => complete_percentage,
      :incomplete_percentage => incomplete_percentage
    }
    @work_centers = WorkCenter.where(:company_id => @company.id).order(:name)
   
    # Work center data
    @work_centers_data = []
    @work_centers.each do |wc|
      complete = wc.health_cycles.where(:complete => true, year: "2020", active: true).count
      incomplete = wc.health_cycles.where(:complete => false, year: "2020", active: true).count
      total = complete + incomplete
      percentage = total > 0 ? ((complete * 100) / total) : 0
      p "===>> #{percentage}"
      @work_centers_data << {
        :work_center => wc.name,
        :complete => complete,
        :incomplete => incomplete,
        :total => total,
        :percentage => percentage,
        :color_class => percentage >= 80 ? "bg-success" : percentage >= 50 ? "bg-primary" : "bg-danger"
      }
    end

    @areas = Area.where(:company_id => @company.id).where("parent_id != 0").order(:name)

    # Areas data
    @areas_data = []
    @areas.each do |a|
      complete = a.health_cycles.where(:complete => true, year: "2020", active: true).count
      incomplete = a.health_cycles.where(:complete => false, year: "2020", active: true).count
      total = complete + incomplete
      percentage = total > 0 ? ((complete * 100) / total) : 0
      @areas_data << {
        :area => a.name,
        :complete => complete,
        :incomplete => incomplete,
        :total => total,
        :percentage => percentage,
        :color_class => percentage >= 80 ? "bg-success" : percentage >= 50 ? "bg-primary" : "bg-danger"
      }
    end
  end

  def show
    health_cycle = HealthCycle.find(params[:id])
    health_cycle.complete = params[:health_cycle][:complete]
    health_cycle.save
  end

  def distribution
    @sidebar_rigth_app = true

    @patient_data = Patient.where(:company_id => @company.id, active: true)

    total = @patient_data.count
    male = @patient_data.where(:gender => 1).count
    female = @patient_data.where(:gender => 2).count
    
    male_pecentage = total > 0 ? (male.to_f / total.to_f * 100.0) : 0
    female_pecentage = total > 0 ? (female.to_f / total.to_f * 100.0 ) : 0

    @gender_data = {
      :male => male,
      :female => female,
      :male_pecentage => number_to_percentage(male_pecentage, precision: 0, format: "%n"),
      :female_pecentage => number_to_percentage(female_pecentage, precision: 0, format: "%n"),
      :total => total
    }

  end

  def diseases
    @sidebar_rigth_app = true

    @health_cycles = HealthCycle.where(:company_id => @company.id, year: "2020", active: true)
    complete = @health_cycles.where(:complete => true).count
    incomplete = @health_cycles.where(:complete => false).count
    total = complete + incomplete
    complete_percentage = total > 0 ? ((complete * 100) / total) : 0
    incomplete_percentage = total > 0 ? ((incomplete * 100) / total) : 0

    @disease_data = Patient.by_disease(1)
  end

end
