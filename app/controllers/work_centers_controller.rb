class WorkCentersController < ApplicationController
  before_action :set_work_center, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @sidebar_rigth_app = true
  end

  def get_dataset
    items = WorkCenter.where(:company_id => @company.id).order(:name).datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: i.name,
        description: i.description,
        active: i.active.present? ? "Si" : "No",
        edit: helpers.link_to_edit(edit_work_center_path(i.id))
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_with(@work_center)
  end

  def new
    @work_center = WorkCenter.new
    @work_center.active = true
    @work_center.company_id = @company.id
    respond_modal_with @work_center
  end

  def edit
    respond_modal_with @work_center
  end

  def create
    @work_center = WorkCenter.create(work_center_params)
    respond_modal_with @work_center, location: work_centers_path
  end

  def update
    @work_center.update(work_center_params)
    respond_modal_with @work_center, location: work_centers_path
  end

  def destroy
    @work_center.destroy
    respond_with(@work_center)
  end

  private
    def set_work_center
      @work_center = WorkCenter.find(params[:id])
    end

    def work_center_params
      params.require(:work_center).permit(:name, :description, :active, :company_id, :address)
    end
end
