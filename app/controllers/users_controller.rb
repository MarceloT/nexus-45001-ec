class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :get_roles, only: [:new, :create, :edit, :update, :destroy]
  

  # GET /users
  # GET /users.json
  def index
    @sidebar_rigth_app = true
    # @users = User.all
  end


  def get_dataset
    items = User.all
    items = items.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: i.name,
        login: i.login,
        email: i.email,
        role: i.roles.any? ? i.roles.map{|r| r.name}.join(", ") : "Sin asignar",
        last_sign_in_at: i.last_sign_in_at.present? ? helpers.time_ago_in_words(i.last_sign_in_at) : "No disponible",
        active: i.active.present? ? "Si" : "No",
        edit: helpers.link_to_edit(edit_user_path(i.id))
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  # GET /users/1
  # GET /users/1.json
  def show
    respond_modal_with @user
  end

  # GET /users/new
  def new
    @user = User.new
    @user.active = true
    respond_modal_with @user
  end

  # GET /users/1/edit
  def edit
    respond_modal_with @user
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.create(user_params)
    respond_modal_with @user, location: users_path
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user.update(user_params)
    location = params[:url].present? ? params[:url] : root_path
    respond_modal_with @user, location: location
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:login, :password, :name, :email, :role_ids, :active, :photo)
    end
end
