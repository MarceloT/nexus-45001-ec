class PersonalProtectionsController < ApplicationController
  before_action :set_personal_protection, only: [:show, :edit, :update, :destroy]
  before_action :get_types, only: [:new, :create, :edit, :update, :show, :get_dataset]
  respond_to :html

  def index
    @sidebar_rigth_app = true
    @personal_protection_patients = PersonalProtectionPatient.where(company_id: @company.id)
  end

  def get_dataset
    items = PersonalProtection.where(company_id: @company.id).datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: i.name,
        type: helpers.select_array(@types, i.protection_type_id),
        stock: i.stock,
        edit: "#{helpers.link_to_edit(edit_personal_protection_path(i.id))} #{helpers.link_to '<i class="material-icons icon-16pt">forward</i>'.html_safe , new_personal_protection_patient_path(:personal_protection_id => i.id),  data: { modal: true } }"
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_with(@personal_protection)
  end

  def new
    @personal_protection = PersonalProtection.new
    @personal_protection.company_id = @company.id
    @personal_protection.stock = 0
    respond_modal_with @personal_protection
  end

  def edit
    respond_modal_with @personal_protection
  end

  def create
    @personal_protection = PersonalProtection.create(personal_protection_params)
    respond_modal_with @personal_protection, location: personal_protections_path
  end

  def update
    @personal_protection.update(personal_protection_params)
    respond_modal_with @personal_protection, location: personal_protections_path
  end

  def destroy
    @personal_protection.destroy
    respond_with(@personal_protection)
  end

  def get_types
    @types = [["Cabeza", "1"],["Ojos", "2"],["Sis. Respiratorio", "3"],["Oído", "4"],["Manos", "5"],["Cuerpo", "6"],["Pies", "7"],["Otros", "8"]]
  end

  private
    def set_personal_protection
      @personal_protection = PersonalProtection.find(params[:id])
    end

    def personal_protection_params
      params.require(:personal_protection).permit(:name, :protection_type_id, :company_id, :stock, :image, :features, :details)
    end
end
