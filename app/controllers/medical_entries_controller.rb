class MedicalEntriesController < ApplicationController
  respond_to :html
  before_action :get_api_doctors, :get_areas, :get_positions

  def list
    @sidebar_rigth_app = true
    @from_date = params[:from_date] || Time.now
    @to_date = params[:to_date] || Time.now

    nl_api = NextLabApi.new(session[:import_credential_token])

    if params[:nro_order].present?
      @orders = [nl_api.get_api_orders(params[:nro_order].strip, 'order')]
    elsif params[:ci].present?
      @orders = nl_api.get_api_orders(params[:ci].strip, 'patient')
    else
      @orders = nl_api.get_api_orders("from_date=#{@from_date}&to_date=#{@to_date}", 'date_range')
    end
    
    # Group by Patient
    patients = []
    patients = @orders.group_by{|h| h["patient"]["ci"]} if @orders.any?

    @order_by_patients = []
    orders = []
    full_name = ""

    patients.each do |patient_orders|
      Rails.logger.info "======>>>> patient_orders: #{patient_orders.last}"
      orders = []  
      full_name = ""
      patient_orders.last.each do |order|
        Rails.logger.info "======>>>> order: #{order}"
        full_name = order["patient"]["full_name"]

        requests = []
        laboratory = false

        order["requests"].each do |request|
          medical_record = MedicalRecord.where(:nro_ord => order["nro_order"], :cod_ana => request["exam"]["cod_ana"]).last
          if request["rnd_pet"] == true
            requests << {
              :name => request["exam"]["nom_exam"],
              :code => request["exam"]["cod_ana"],
              :type => "record",
              :status => medical_record.present? ? medical_record.status : "none",
              :pdf_url => medical_record.present? ? medical_record.pdf_demo : nil
            }
          elsif request["rnd_pet"] == false
            laboratory = true
          end
        end

        if laboratory
          requests << {
            :name => "Laboratorio",
            :type => "laboratory",
            :status => "audited",
            :pdf_url => nil
          }
        end

        orders << {
          :nro_ord => order["nro_order"],
          :date => order["date"],
          :requests => requests
        }
      end

      patient = {
        :patient => {
          :full_name => full_name,
          :orders => orders
        }
      }

      @order_by_patients << patient  

    end
  end

  def index
    @sidebar_rigth_app = true
    api_orders = Array.new
    @medical_entries = Array.new

    nl_api = NextLabApi.new(session[:import_credential_token])
    
    if params[:order].present?
      # Call order service by Nro
      @order = OpenStruct.new(nl_api.get_api_orders(params[:order], 'order').to_h)
      @active_services = MedicalService.where(active: true).where('company_id IS NULL OR company_id = ?', @company.id).order(:name)

      Rails.logger.info "========> @order: #{@order.inspect}"
      company = Company.find_or_initialize_by(legacy_id: @order.source["code"]) if @order.source.present?
      if company 
        company.name = @order.source["name"]
        company.code = @order.source["code"]
        company.save(validate: false)
      end

      # Add Laboratorio Item and save results
      # Remove results related with Laboratorio
      @requests = []
      item = 1
      lab_exits = false

      # import patient from order
      patient = Patient.find_or_initialize_by(legacy_id: @order.patient["legacy_id"]) if @order.patient.present?
      if patient 
        Rails.logger.info "===========> Patient: #{@order.patient.inspect}"
        patient.ci = @order.patient["ci"]
        patient.ci_type = 2
        patient.name = @order.patient["name"]
        patient.name_1 = @order.patient["name_1"]
        patient.last_name = @order.patient["last_name"]
        patient.last_name_1 = @order.patient["last_name_1"]
        patient.gender = @order.patient["gender"]
        patient.birth_date = @order.patient["birth_date"]
        patient.email = @order.patient["email_pac"]
        patient.company_id = company.id if company.present?
        patient.active = true
        patient.save(validate: false)
      end

      if @order.present? && @order.requests.present? && @order.requests.any?

        lab_requests = []

        @order.requests.each do |r|
          if r["rnd_pet"] == true
            medical_entry = MedicalEntry.find_by(nro_ord: @order.nro_order, cod_ana: r["exam"]["cod_ana"])
            @requests << {
                idx: item,
                des_ana: r["exam"]["nom_exam"],
                cod_ana: r["exam"]["cod_ana"],
                sts_pet: r["sts_pet"],
                service: medical_entry.present? && medical_entry.medical_service.present? ? medical_entry.medical_service.name : nil,
                created_at: medical_entry.present? ? medical_entry.created_at : nil,
                updated_at: medical_entry.present? ? medical_entry.updated_at : nil,
                pdf_url: medical_entry.present? ? medical_entry_path(medical_entry) : new_medical_entry_path(nro_ord: @order.nro_order, cod_pac: @order.patient["legacy_id"], cod_ana: r["exam"]["cod_ana"], des_ana: r["exam"]["nom_exam"], cod_pet: r["cod_pet"]),
                is_saved: medical_entry.present?
              }
            item += 1
          elsif r["rnd_pet"] == false 
            lab_requests << r["exam"]
          end
        end

        if lab_requests.any?
          @lab_request = {
            idx: 0,
            des_ana: "Laboratorio",
            service: lab_requests,
            sts_pet: "",
            created_at: nil,
            updated_at: nil,
            pdf_url: nil,
            is_saved: false
          }
        end
      end
    end

    respond_to do |f|
      f.html {}
      f.js
    end
  end

  def show
    @medical_entry = MedicalEntry.find(params[:id])
    respond_modal_with @medical_entry
  end

  def register_values
    nro_order = params[:nro_order]
    nl_api = NextLabApi.new(session[:import_credential_token])
    @order = OpenStruct.new(nl_api.get_api_orders(nro_order, 'order').to_h)

    patient = Patient.find_or_initialize_by(legacy_id: @order.patient["legacy_id"])

    Rails.logger.info "==>> @order.requests: #{@order.requests}" 
    @order.requests.each do |r|
      if r["rnd_pet"] == false
        r["exam"]["determinations"].each do |det|
          Rails.logger.info "==>> det: #{det}"
          ep = ExamPatient.where(:patient_id => patient.id, :determination_id => det["cod_det"].to_i).last
          
          if !ep.present?
            ep = ExamPatient.new
          end
          exam = Exam.find_by_code(r["exam"]["cod_ana"])
          
          ep.exam_id    = exam.id if exam.present? 
          
          ep.patient_id = patient.id
          ep.determination_id = det["cod_det"].to_i
          ep.determination = det["nom_det"]
          ep.result = det["res_det"]
          ep.unit = det["uni_det"]
          ep.ref_value = det["val_ref"]
          ep.nro_ord = nro_order
          ep.active = true

          Rails.logger.info "==>> ep: #{ep.inspect}"
          ep.save

        end
      end
    end
    
  end

  def new
    patient = Patient.find_by(legacy_id: params[:cod_pac]).id
    @medical_entry = MedicalEntry.new
    @medical_entry.nro_ord = params[:nro_ord]
    @medical_entry.cod_ana = params[:cod_ana]
    @medical_entry.patient_id = patient
    @medical_entry.doctor_id = nil
    @medical_entry.medical_service_id = params[:service_id]
    @medical_entry.company_id = @company.id
    @medical_entry.created_by_id = current_user.id
    exam = Exam.find_by(code: params[:cod_ana])
    if exam.nil?
      exam = Exam.new
      exam.code = params[:cod_ana]
      exam.name = params[:des_ana]
      exam.active = true
      exam.save if exam.valid?
    end
    if @medical_entry.save
      # CREATE Medical Record
      @medical_record = MedicalRecord.find_or_initialize_by(legacy_id: params[:cod_pet])
      unless @medical_record.id.present?
        if @medical_entry.medical_service.id <= 3
          @medical_record.medical_type_id = @medical_entry.medical_service.id - 1
        else
          @medical_record.medical_type_id = 4
        end
        @medical_record.date = Time.now
        @medical_record.nro_ord = params[:nro_ord]
        @medical_record.cod_ana = params[:cod_ana]
        @medical_record.patient_id = patient
        @medical_record.doctor_id = nil
        @medical_record.company_id = @company.id
        if @medical_record.valid?
          @medical_record.save
        else
          Rails.logger.info "=============================================== MEDICAL_RECORD"
          Rails.logger.info @medical_record.errors.messages.inspect
        end
        if @medical_record.id.present?
          MedicalRecordExam.find_or_create_by(medical_record_id: @medical_record.id, exam_id: exam.id)
        end
      end
    else
      Rails.logger.info "=============================================== MEDICAL_ENTRY"
      Rails.logger.info @medical_entry.errors.messages.inspect
    end


    respond_modal_with @medical_entry
  end

  
end
