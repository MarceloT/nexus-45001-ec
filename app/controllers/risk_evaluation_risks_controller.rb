class RiskEvaluationRisksController < ApplicationController
  before_action :set_risk_evaluation_risk, only: [:show, :edit, :update, :destroy]
  before_action :get_risk_evaluation_values, only: [:edit, :update]
  before_action :get_controls, only: [:new, :create, :edit, :update]
  respond_to :html
  # GET /risk_evaluation_risks
  # GET /risk_evaluation_risks.json
  def index
    @risk_evaluation_risks = RiskEvaluationRisk.all
  end

  # GET /risk_evaluation_risks/1
  # GET /risk_evaluation_risks/1.json
  def show
  end

  # GET /risk_evaluation_risks/new
  def new
    @risk_evaluation_risk = RiskEvaluationRisk.new
  end

  # GET /risk_evaluation_risks/1/edit
  def edit

    if @risk_evaluation_risk.risk_evaluation.evaluation_type_id.to_i == 1
      @risk_evaluation_risk.reference_value = @risk_evaluation_risk.reference_value.to_i
      @risk_evaluation_risk.medium_value = @risk_evaluation_risk.medium_value.to_i
    end

    respond_modal_with @risk_evaluation_risk
  end

  # POST /risk_evaluation_risks
  # POST /risk_evaluation_risks.json
  def create
    @risk_evaluation_risk = RiskEvaluationRisk.new(risk_evaluation_risk_params)

    respond_to do |format|
      if @risk_evaluation_risk.save
        format.html { redirect_to @risk_evaluation_risk, notice: 'Risk evaluation risk was successfully created.' }
        format.json { render :show, status: :created, location: @risk_evaluation_risk }
      else
        format.html { render :new }
        format.json { render json: @risk_evaluation_risk.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /risk_evaluation_risks/1
  # PATCH/PUT /risk_evaluation_risks/1.json
  def update
    @risk_evaluation_risk.update(risk_evaluation_risk_params)
    respond_modal_with @risk_evaluation_risk, location: "none"
    # respond_to do |f|
    #   f.html {}
    #   f.js
    # end
  end

  # DELETE /risk_evaluation_risks/1
  # DELETE /risk_evaluation_risks/1.json
  def destroy
    @risk_evaluation_risk.destroy
    respond_to do |format|
      format.html { redirect_to risk_evaluation_risks_url, notice: 'Risk evaluation risk was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def get_controls
    @controls = [
      ["Eliminación", "1"],
      ["Sustitución", "2"],
      ["Controles de ingeniría", "3"],
      ["Señalización advertencias y controles administrativos", "4"],
      ["Equipo de proteccíon personal", "5"]
    ]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_risk_evaluation_risk
      @risk_evaluation_risk = RiskEvaluationRisk.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def risk_evaluation_risk_params
      params.require(:risk_evaluation_risk).permit(:m_count, :f_count, :s_count, :description, :reference_value, :medium_value, :exposition_value, :result, :result_gr, :risk_id, :risk_category_id, :risk_evaluation_id)
    end
end
