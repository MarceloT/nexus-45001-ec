class RiskEvaluationsController < ApplicationController
  before_action :set_risk_evaluation, only: [:show, :edit, :update, :destroy]
  before_action :get_areas, :get_positions, :get_risks, only: [:new, :create, :edit, :update]
  before_action :get_risk_categories, only: [:show]
  before_action :get_patients, :get_processes, :get_activities, only: [:new, :create, :edit, :update]
  before_action :get_type_evaluation, only: [:new, :create, :edit, :update, :show, :get_dataset]
  before_action :get_risk_evaluation_values, only: [:get_dataset_details]

  respond_to :html

  def index
    @sidebar_rigth_app = true
    # @risk_evaluations = RiskEvaluation.all
    # respond_with(@risk_evaluations)
  end

  def get_dataset
    items = RiskEvaluation.where(:company_id => @company.id)
    items = items.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: "<strong>Actividad: </strong>#{i.activity.name}<br/><strong>Proceso: </strong>#{i.process_id.present? ? i.process.name : "Sin Asignar"}<br/><strong>Puesto de Trabajo: </strong>#{i.position_id.present? ? i.position.name : "Sin Asignar"}",
        date: i.date.strftime(@date_format),
        owner: i.owner_id.present? ? i.owner.full_name : "Sin Asignar",
        type: helpers.select_array(@type_evaluation, i.evaluation_type_id),
        edit: "#{helpers.link_to_show_redirect(risk_evaluation_path(i.id))} #{helpers.link_to_edit(edit_risk_evaluation_path(i.id))}"
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def get_dataset_details
    items = RiskEvaluationRisk.where(:risk_evaluation_id => params[:evaluation_id], :risk_category_id => params[:category_id] )
    if params[:val_id].present? && params[:val_id].to_s != "Todos"
      items = items.where(:result_gr => params[:val_id].to_s)
    end
    # items = items.datatable_filter(params['search']['value'], params['columns'])
    # items_filtered = items.count

    items_filtered = items
    items = items.datatable_order(2, 'asc')
    items = items.page(1).per(100)
    nro   = 0
    items = items.map {|i| 
      nro += 1
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        nro: nro,
        risk_category: i.risk.risk_category.name,
        name: i.risk.name,
        m_count: i.m_count,
        f_count: i.f_count,
        s_count: i.s_count,
        reference_value: i.risk_evaluation.evaluation_type_id.to_i == 1 ? i.reference_value : helpers.select_array(@probability_values, i.reference_value) ,
        medium_value: i.risk_evaluation.evaluation_type_id.to_i == 1 ? i.medium_value : helpers.select_array(@consequence_values, i.medium_value),
        exposition_value: i.exposition_value,
        result: i.risk_evaluation.evaluation_type_id.to_i == 1 ? "<span id='result-text-row' class='badge #{i.result_gr_class}'>#{i.result_gr_to_s} (#{i.result})</span>" : "<span id='result-text-row' class='badge #{i.result_m2_class}'>#{i.result_m2_to_s}</span>",
        edit: helpers.link_to_edit(edit_risk_evaluation_risk_path(i.id))
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    @sidebar_rigth_app = true
    respond_with(@risk_evaluation)
  end

  def new
    @risk_evaluation = RiskEvaluation.new
    @risk_evaluation.company_id = @company.id if @company.present?
    @risk_evaluation.date = Time.now
    @risk_evaluation.created_by_id = current_user.id
    respond_modal_with @risk_evaluation
  end

  def edit
    @risk_evaluation.created_by_id = current_user.id
    respond_modal_with @risk_evaluation
  end

  def create
    @risk_evaluation = RiskEvaluation.create(risk_evaluation_params)
    # Rails.logger.info "=====>> @risk_evaluation.errors.full_messages: #{@risk_evaluation.errors.full_messages}"
    @risk_evaluation.created_by_id = current_user.id
    @risk_evaluation.company_id = @company.id
    update_risk_categories if @risk_evaluation.id.present?
    respond_modal_with @risk_evaluation, location: @risk_evaluation.valid? ? risk_evaluation_path(@risk_evaluation) : risk_evaluations_path
  end

  def update
    @risk_evaluation.update(risk_evaluation_params)
    update_risk_categories if @risk_evaluation.present?
    respond_modal_with @risk_evaluation, location: risk_evaluation_path(@risk_evaluation)
  end

  def destroy
    # @risk_evaluation.destroy
    respond_with(@risk_evaluation)
  end

  def get_type_evaluation
    @type_evaluation = [
      ["William T. Fine", "1"],
      ["INSST / INSHT", "2"]
    ]
  end

  def update_risk_categories
    @risk_evaluation.risk_evaluation_risks.each do |r|
      r.risk_category_id = r.risk.risk_category_id
      r.result_gr = "Bajo" unless r.result_gr.present?
      r.save
    end
  end

  private
    def set_risk_evaluation
      @risk_evaluation = RiskEvaluation.find(params[:id])
    end

    def risk_evaluation_params
      params.require(:risk_evaluation).permit(:name, :company_id, :process_id, :sub_process_id, :position_id, :area_id, :date, :occupational_manager, :evaluation_manager, :company_evaluation, :description, :members, :activity_id, :created_by_id, :owner_id, :evaluation_type_id, risk_ids: [])
    end
end
