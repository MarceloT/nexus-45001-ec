class CompanyProcessesController < ApplicationController
  before_action :set_company_process, only: [:show, :edit, :update, :destroy]
  before_action :get_patients, :get_processes, only: [:new, :create, :edit, :update]

  respond_to :html

  def index
    @sidebar_rigth_app = true
    @company_process = CompanyProcess.where(:parent_id => nil).last
    respond_with(@company_process)
  end

  def show
    respond_with(@company_process)
  end

  def new
    @company_process = CompanyProcess.new
    @company_process.company_id = @company.id
    @company_process.active = true
    respond_modal_with @company_process
  end

  def edit
    respond_modal_with @company_process
  end

  def create
    @company_process = CompanyProcess.create(company_process_params)
    respond_modal_with @company_process, location: company_processes_path
  end

  def update
    @company_process.update(company_process_params)
    respond_modal_with @company_process, location: company_processes_path
  end

  def destroy
    @company_process.destroy
    respond_with(@company_process)
  end

  private
    def set_company_process
      @company_process = CompanyProcess.find(params[:id])
    end

    def company_process_params
      params.require(:company_process).permit(
        :name, :description, :owner_id, :company_id, :parent_id, :level, :process_in_id, 
        :process_out_id, :process_type, :active,
        activities_attributes: [:id, :number, :name, :active])
    end
end
