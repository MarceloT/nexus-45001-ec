class MedicalServicesController < ApplicationController
  before_action :set_medical_service, only: [:show, :edit, :update, :destroy]
  before_action :get_exams, only: [:show, :edit, :update, :create, :new]

  respond_to :html

  def index
    @sidebar_rigth_app = true
  end

  def get_dataset
    items = MedicalService.where("company_id = ? OR company_id IS NULL", @company.id)
    items = items.datatable_filter(params['search']['value'], params['columns'])
    items_filtered = items.count
    items = items.datatable_order(params['order']['0']['column'].to_i,
                                  params['order']['0']['dir'])
    items = items.page(((params['start'].to_i + 1)/params['length'].to_i)+1).per(params['length'].to_i)
    items = items.map {|i| 
      {
        class_name: i.class.to_s.underscore,
        id: i.id,
        name: i.name,
        exam: "#{i.exam.code} - #{i.exam.name}",
        jasper_template_url: i.jasper_template_url,
        active: i.active.present? ? "Si" : "No",
        edit: "#{helpers.link_to_show(medical_service_path(i.id))} #{helpers.link_to_edit(edit_medical_service_path(i.id))}",
      }
    }
    render json: { items: items,
                   draw: params['draw'].to_i,
                   recordsTotal: items.count,
                   recordsFiltered: items_filtered }
  end

  def show
    respond_modal_with(@medical_service)
  end

  def new
    @medical_service = MedicalService.new
    @medical_service.active = true
    @medical_service.company_id = @company.id if @company.present?
    respond_modal_with(@medical_service)
  end

  def edit
    respond_modal_with(@medical_service)
  end

  def create
    @medical_service = MedicalService.new(medical_service_params)
    @medical_service.save
    respond_modal_with @medical_service, location: medical_services_path
  end

  def update
    @medical_service.update(medical_service_params)
    location = params[:url].present? ? params[:url] : root_path
    respond_modal_with @medical_service, location: location
  end

  def destroy
    #@medical_service.destroy
    @medical_service.active = false
    @medical_service.save
    respond_with(@medical_service)
  end

  private
    def set_medical_service
      @medical_service = MedicalService.find(params[:id])
    end

    def medical_service_params
      params.require(:medical_service).permit(:company_id, :name, :exam_id, :active, :jasper_template_url)
    end
end
