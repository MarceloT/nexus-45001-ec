class MedicalChecksController < ApplicationController
  before_action :set_patient, :get_civil_states, :get_blood_type, only: [:index, :show, :preview, :result_file]

  before_action :set_medical_record, only: [:show, :audit, :edit, :update, :destroy, :preview, :result_file]
  before_action :get_doctors, :get_patients, :get_ci_type, :get_positions, :get_companies, :get_areas, :get_medical_types, only: [:create, :new, :edit, :update, :index, :audits, :show, :audit]
  before_action :get_medical_status, only: [:index, :audits]
  
  before_action :get_personal_histories, :get_gender, :get_habits, 
                :get_lifestyles, :get_family_histories, :get_system_revisions, :get_vital_constants, 
                :get_exams, :get_diagnostics, :get_physical_exams, :get_profession, 
                :get_trauma_types, :get_trauma_curvature_types, only: [:show, :audit, :index, :new, :create]

  respond_to :html

  def index
    @sidebar_rigth_app = true

    @medical_records   = @patient.medical_records.where(:initial => false).order(date: :asc).to_a.group_by_year(&:date)

    respond_to do |f|
      f.html {}
      f.js
    end
  end

  def audits
    @sidebar_rigth_app = true
    @medical_records = MedicalRecord.all

    if params["/medical_records"].present?
      if params["/medical_records"][:patient_id].present?
        @medical_records = @medical_records.where(:patient_id => params["/medical_records"][:patient_id])
        @patient_filter = true
        Rails.logger.info "=====>>>>@patient_filter: #{@patient_filter}"
      end

      if params["/medical_records"][:medical_type_id].present?
        @medical_records = @medical_records.where(:medical_type_id => params["/medical_records"][:medical_type_id])
        @medical_type_filter = true
      end

      if params["/medical_records"][:doctor_id].present?
        @medical_records = @medical_records.where(:doctor_id => params["/medical_records"][:doctor_id])
        @doctor_filter = true
      end

      if params["/medical_records"][:year].present?
        @medical_records = @medical_records.where('extract(year from date) = ?', params["/medical_records"][:year])
        @year_filter = true
      end

      if params["/medical_records"][:audit].present? && params["/medical_records"][:audit] == 'true'
        @medical_records = @medical_records.where(status: :finished)  
      end
    end
    
    @medical_records = @medical_records.to_a.group_by_day(&:date)

    respond_to do |f|
      f.html {}
      f.js
    end
  end

  def load_records
    @medical_records = MedicalRecord.group_by_day(:created_at).count
  end

  def show
    @sidebar_rigth_app = true
    @patient = @medical_record.patient
    @medical_record.toxic_habit_ids = [1, 2, 3] unless @medical_record.toxic_habit_ids.any?
    @medical_record.lifestyle_ids = [1, 2, 2, 2] unless @medical_record.lifestyle_ids.any?
    @medical_record.family_history_ids = [1, 2, 3, 4, 5, 6, 7, 8] unless @medical_record.family_history_ids.any?
    @medical_record.system_revision_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] unless @medical_record.system_revision_ids.any?
    @medical_record.vital_constant_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9] unless @medical_record.vital_constant_ids.any?
    @medical_type_to_s = helpers.select_array(@medical_types, @medical_record.medical_type_id)
    # doctor could be nil on ecuamerican imported, then add current user
    if @medical_record.doctor.nil?
      @medical_record.update_attribute(:doctor_id, 2)
    end

    sections_control

    @initial_history = @medical_record.get_initial_history
    unless @medical_record.medical_record_accident_deseases.any?
      @medical_record.medical_record_accident_deseases = [
        MedicalRecordAccidentDesease.new(accident_type: 'Accidente'),
        MedicalRecordAccidentDesease.new(accident_type: 'Enfermedad')]
    end

    if @medical_record.exams.first && @medical_record.exams.first.code == '917'
      @medical_record.build_spe_ophthalmolytic unless @medical_record.spe_ophthalmolytic.present?
    end

    respond_with(@medical_record)

    respond_to do |f|
      f.html {}
      f.js
    end
  end

  def audit
    @sidebar_rigth_app = true
    @patient = @medical_record.patient
    @medical_record.toxic_habit_ids = [1, 2, 3] unless @medical_record.toxic_habit_ids.any?
    @medical_record.lifestyle_ids = [1, 2, 2, 2] unless @medical_record.lifestyle_ids.any?
    @medical_record.family_history_ids = [1, 2, 3, 4, 5, 6, 7, 8] unless @medical_record.family_history_ids.any?
    @medical_record.system_revision_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] unless @medical_record.system_revision_ids.any?
    @medical_record.vital_constant_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9] unless @medical_record.vital_constant_ids.any?
    @medical_type_to_s = helpers.select_array(@medical_types, @medical_record.medical_type_id)
    # doctor could be nil on ecuamerican imported, then add current user
    if @medical_record.doctor.nil? && @medical_record.medical_type_id == 4
      @medical_record.update_attribute(:doctor_id, 2)
    end
    
    sections_control

    @initial_history = @medical_record.get_initial_history

    respond_with(@medical_record)

    respond_to do |f|
      f.html {}
      f.js
    end
  end

  def new
    @medical_record = MedicalRecord.new
    @medical_record.medical_type_id = 1
    @medical_record.date = Time.now

    if params[:patient_record_id].present?
      Rails.logger.info "-====>>>>>>> params[:patient_record_id]: #{params.inspect}"
      @medical_record.patient = Patient.find(params[:patient_record_id])
      if @medical_record.patient && params[:position_id].present? 
        @patient = @medical_record.patient
        @patient.position_id = params[:position_id]
        @patient.save(:validate => false)
      end
    elsif params[:ci].present?
      Rails.logger.info "-====>>>>>>> on ci"
      patient = Patient.new
      patient.active = true
      patient.ci_type = 2
      patient.ci = params[:ci]
      patient.save(:validate => false)
      @medical_record.patient_id = patient.id
      Rails.logger.info "-====>>>>>>> @medical_record.patient.ci: #{@medical_record.patient.ci}"
    else
      @medical_record.patient = Patient.new
      @medical_record.patient.active = true
      @medical_record.patient.ci_type = 2
    end

    # Set current doctor
    @medical_record.doctor_id = 2

    if @patient.present? && @patient.position.present? && @patient.position.medical_profile.present?
      @medical_record.exam_ids = @patient.position.medical_profile.exam_ids unless @medical_record.exam_ids.any?
    end

    respond_modal_with @medical_record
  end

  def edit
  end

  def preview
    # PdfWatermark.conbinate_watermark_pdf(Rails.root.join(odt_file_path.gsub(".odt", ".pdf")), Rails.root.join(watermark_file_path))  
    @certificate = params[:certificate].present?

    Rails.logger.info "===== = == = @certificate: #{@certificate}"

    if @medical_record.medical_type_id == 0
      @pdf_url_demo = "/pdfs/msp/inicial.pdf"
    elsif @medical_record.medical_type_id == 1
      @mo_result = true
    elsif @medical_record.medical_type_id == 2
      @pdf_url_demo = "/pdfs/msp/reintegro.pdf"
    elsif @medical_record.medical_type_id == 3
      @pdf_url_demo = "/pdfs/msp/retiro.pdf"
    elsif @medical_record.medical_type_id == 4
      @pdf_url_demo = "/pdfs/msp/evaluacion.pdf"
    elsif @medical_record.medical_type_id == 5
      @pdf_url_demo = "/pdfs/msp/control.pdf"
    end

    @can_finish = params[:on_edit].present?
    
    respond_to do |f|
      f.html {respond_modal_with @medical_record}
      f.js {respond_modal_with @medical_record}
      f.pdf {send_data MedicalRecord.print_data(@medical_record, @certificate), filename:'medical_record.pdf', type: "application/pdf", disposition: :inline}
    end
    
  end

  def result_file

    if @medical_record.exams.first && @medical_record.exams.first.code == '917'
      jasper_name      = "spe_ophthalmolytic.jasper"
    else
      jasper_name      = "evaluation_template.jasper"
    end

    evaluation_file  = "evaluation_#{@medical_record.id}"

    respond_to do |format|
      format.pdf {send_doc(@medical_record.to_xml, '/evaluation', jasper_name, evaluation_file, "pdf")}
      format.xml {render xml: @medical_record.to_xml}
    end
  end

  def create
    @medical_record = MedicalRecord.create(medical_record_params)
    # @medical_record.date = Time.now unless @medical_record.valid?
    respond_modal_with @medical_record, location: @medical_record.valid? ? patient_record_medical_check_path(@medical_record.patient.id,@medical_record.id) : patient_record_medical_checks_path(@medical_record.patient.id)
  end

  def update
    if params[:medical_record][:status] == "comments" && params[:medical_record][:comments] == ""
      @medical_record.errors.add(:comments, "No debe estar vacío")
      #@redirect = true
      #@redirect_url = audit_medical_record_path(@medical_record)
    else
      @medical_record.update(medical_record_params)

      @redirect = @medical_record.finished?
      @redirect_url = medical_records_path
    end
    Rails.logger.info "===========================================> @medical_record.errors"
    Rails.logger.info @medical_record.errors.messages.inspect
    # respond_with(@medical_record)
    
    respond_to do |f|
      f.html {}
      f.js
    end
  end

  def destroy
    @medical_record.destroy
    respond_with(@medical_record)
  end

  # Auto save methods
  def new_personal_history
    if params[:personal_history].present?
      personal_history_name = params[:personal_history][:name]
      medical_record_id = params[:personal_history][:medical_record_id]
      personal_history = PersonalHistory.create(:name => personal_history_name) if personal_history_name.present?
      MedicalRecordPersonalHistory.create(:medical_record_id => medical_record_id, :personal_history_id => personal_history.id)
    end
    render json: { personal_history_id: personal_history.id}
  end

  def remove_personal_history
    if params[:personal_history].present?
      personal_history_id = params[:personal_history][:personal_history_id]
      medical_record_id = params[:personal_history][:medical_record_id]
      mrph = MedicalRecordPersonalHistory.where(:medical_record_id => medical_record_id, :personal_history_id => personal_history_id).last
      mrph.destroy if mrph
    end
    render json: { create: "ok"}
  end

  def patient_records
    @patients = Patient.where(:company_id => @company.id).includes(:position)
  end

  private
    def set_patient
      @patient = params[:patient_record_id].present? ? Patient.find(params[:patient_record_id]) : nil
    end

    def set_medical_record
      @medical_record = MedicalRecord.find(params[:id])
    end

    def sections_control
      if @patient.present? && @patient.position.present? && @patient.position.medical_profile.present?
        @medical_record.exam_ids = @patient.position.medical_profile.exam_ids unless @medical_record.exam_ids.any?
      end
      @medical_record.physical_exam_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40] unless @medical_record.physical_exam_ids.any?
      @available_sections = ('a'..'o').to_a
      if @medical_record.medical_type_id == 0
        @available_sections =  ['a','b','c','r','d','e', 's','f','g','h','i','j','k','l','m','n','o']
      elsif @medical_record.medical_type_id == 2
        @available_sections = ['a','b','f','h','i','j','k','l','m','n','o']
      elsif @medical_record.medical_type_id == 3
        @available_sections = ['a','c','h','i','j','k','t','m','n','o']
      elsif @medical_record.medical_type_id == 4
        if @medical_record.exams.any?
          @medical_type_to_s = @medical_record.exams.first.name
          if @medical_record.exams.first.code == '917'
            @available_sections = ['a','p','k','m']
          elsif @medical_record.exams.first.code == '931'
            @available_sections = ['a','b','q','k']
          end
        end
      elsif @medical_record.medical_type_id == 5
        @available_sections = ['a','b', 'j','k', 'm']
      end
      @available_sections << 'u'
      @available_sections << 'v'
    end

    def medical_record_params
      params.require(:medical_record).permit(:medical_type_id, :date, :reason, :current_disease, :recommendations, :patient_id, 
        :doctor_id, :doctor_signature, :aptitude, :aptitude_obs, :aptitude_limit, :legacy_id, :status,
        :ophthalmolytic_obs, :far_correction, :far_left_eye, :far_right_eye, :near_correction, :near_left_eye, :near_right_eye,
        
        :trauma_type, :sitting_posture, :standing_posture, :forced_posture, :repetitive_movements, :manual_heavy_cargo, :avg_wieght, :y_distance,
        :dorsal_kyphosis, :lumbar_kyphosis, :dorsal_scoliosis, :lumbar_scoliosis, :cervical_lordosis_curvature, :dorsal_kyphosis_curvature,
        :lumbar_kyphosis_curvature,
        
        personal_history_ids: [], 
        medical_record_toxic_habits_attributes: [:id, :toxic_habit_id, :active, :time, :quantity, :ex_consumer, :abstinence_time, :_destroy], 
        medical_record_lifestyles_attributes: [:id, :toxic_habit_id, :active, :description, :time, :_destroy],
        medical_record_family_histories_attributes: [:id, :family_history_id, :active, :description, :_destroy],
        medical_record_system_revisions_attributes: [:id, :system_revision_id, :active, :description, :_destroy],
        medical_record_vital_constants_attributes: [:id, :vital_constant, :value, :_destroy],
        medical_record_exams_attributes: [:id, :exam_id, :date, :result, :_destroy],
        medical_record_diagnostics_attributes: [:id, :diagnostic_id, :pre, :result, :_destroy],
        medical_record_employment_histories_attributes: [:company_name, :job, :activities, :job_time, :physical_risk, :mechanical_risk, :chemical_risk,
          :biological_risk, :ergonomic_risk, :psychosocial_risk, :comments],
        medical_record_accident_deseases_attributes: [:medical_record_id, :type, :qualified, :qualified_reason, :quelified_date],
        medical_record_physical_exams_attributes: [:id, :physical_exam_id, :active, :description, :_destroy],
        patient_attributes:[:id, :name, :last_name, :ci, :ci_type, :position_id, :gender_id, :birth_date, :company_id, :area_id,:last_name_1, :name_1, :gender])
    end
end
