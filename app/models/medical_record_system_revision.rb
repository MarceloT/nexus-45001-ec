class MedicalRecordSystemRevision < ApplicationRecord
  belongs_to :medical_record
  belongs_to :system_revision
end
