class MedicalRecordDiagnostic < ApplicationRecord
  belongs_to :medical_record
  belongs_to :diagnostic
end
