class MedicalRecordPersonalHistory < ApplicationRecord
  belongs_to :medical_record
  belongs_to :personal_history
end
