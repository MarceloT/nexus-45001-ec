class MedicalRecord < ApplicationRecord
  include ApplicationHelper
  enum status: { edit: 0, finished: 1, comments: 2, audited: 3 }
  validates :patient_id, presence: true
  # validates :medical_record_exams, presence: { message: "Debe seleccionar al menos uno" }
  #validates :doctor_id, presence: true
  
  belongs_to :patient, optional: true
  accepts_nested_attributes_for :patient
  
  belongs_to :doctor, class_name: "User", foreign_key: "doctor_id", optional: true

  has_many :recipes
  has_many :medical_certificates

  has_many :medical_record_personal_histories, foreign_key: "medical_record_id", :dependent => :destroy
  has_many :personal_histories, through: :medical_record_personal_histories, source: :personal_history

  has_many :medical_record_toxic_habits, foreign_key: "medical_record_id", :dependent => :destroy
  has_many :toxic_habits, through: :medical_record_toxic_habits, source: :toxic_habit
  accepts_nested_attributes_for :medical_record_toxic_habits, :allow_destroy => true

  has_many :medical_record_lifestyles, foreign_key: "medical_record_id", :dependent => :destroy
  has_many :lifestyles, through: :medical_record_lifestyles, source: :lifestyle
  accepts_nested_attributes_for :medical_record_lifestyles, :allow_destroy => true

  has_many :medical_record_family_histories, foreign_key: "medical_record_id", :dependent => :destroy
  has_many :family_histories, through: :medical_record_family_histories, source: :family_history
  accepts_nested_attributes_for :medical_record_family_histories, :allow_destroy => true

  has_many :medical_record_system_revisions, foreign_key: "medical_record_id", :dependent => :destroy
  has_many :system_revisions, through: :medical_record_system_revisions, source: :system_revision
  accepts_nested_attributes_for :medical_record_system_revisions, :allow_destroy => true

  has_many :medical_record_vital_constants, foreign_key: "medical_record_id", :dependent => :destroy
  has_many :vital_constants, through: :medical_record_vital_constants, source: :vital_constant
  accepts_nested_attributes_for :medical_record_vital_constants, :allow_destroy => true

  has_many :medical_record_exams, foreign_key: "medical_record_id", :dependent => :destroy
  has_many :exams, through: :medical_record_exams, source: :exam
  accepts_nested_attributes_for :medical_record_exams, :allow_destroy => true

  has_many :medical_record_diagnostics, foreign_key: "medical_record_id", :dependent => :destroy
  has_many :diagnostics, through: :medical_record_diagnostics, source: :diagnostic
  accepts_nested_attributes_for :medical_record_diagnostics, :allow_destroy => true

  has_many :medical_record_employment_histories, foreign_key: "medical_record_id", :dependent => :destroy
  accepts_nested_attributes_for :medical_record_employment_histories, :allow_destroy => true

  has_many :medical_record_accident_deseases, foreign_key: "medical_record_id", :dependent => :destroy
  accepts_nested_attributes_for :medical_record_accident_deseases, :allow_destroy => true

  has_many :medical_record_physical_exams, foreign_key: "medical_record_id", :dependent => :destroy
  has_many :physical_exams, through: :medical_record_physical_exams, source: :physical_exam
  accepts_nested_attributes_for :medical_record_physical_exams, :allow_destroy => true

  has_one :medical_record_obstetric_gineco_background, foreign_key: "medical_record_id", :dependent => :destroy
  accepts_nested_attributes_for :medical_record_obstetric_gineco_background

  has_one :spe_ophthalmolytic, foreign_key: "medical_record_id", :dependent => :destroy
  accepts_nested_attributes_for :spe_ophthalmolytic

  has_one :spe_traumatology, foreign_key: "medical_record_id", :dependent => :destroy
  accepts_nested_attributes_for :spe_traumatology

  def generate_code
    # "##{self.id} - #{self.reason}"
    # if self.patient.present?
    #   "##{self.patient.ci} - #{self.patient.full_name}"
    # end

    if self.patient.present?
      "##{self.patient.ci}"
    end
  end

  class << self
    #def remote_records_by_patient(code_patient, patient, company)
    #  
    #  api_orders   = call_rest_api(remote_route, import_credential_token)
    #  api_orders.each do |order|
    #    medical_record = MedicalRecord.new(order.to_h)
    #    medical_record.patient = patient
    #    
    #    user = User.new(order.doctor.to_h)
    #    user.password = order.doctor.clave
    #    user.save!
    #    medical_record.doctor = user

    #    medical_record.company = company

    #    medical_record.save!

    #  end
    #end
  end

  def initial?
    self.medical_type_id == 0
  end

  def periodic?
    self.medical_type_id == 1
  end

  def refund?
    self.medical_type_id == 2
  end

  def retirement?
    self.medical_type_id == 2
  end


  def record_name
    medical_record = self
    medical_types = [
                      ["Medicina Ocupacional - Preocupacional", "0", "#48BA16"],
                      ["Medicina Ocupacional - Periódica", "1", "#51A7C5"],
                      ["Medicina Ocupacional - Reintegro", "2", "#ffc107"],
                      ["Medicina Ocupacional - Retiro", "3", "#dc3545"],
                      ["Valoración Especialidades", "4", "#59009a"],
                      ["Control Médico", "5", "#dc7345"]
                    ]

    if medical_record.legacy_id.present? && !medical_record.legacy_id.blank?
      if medical_record.medical_type_id <= 3
        "##{medical_record.nro_ord} - #{select_array(medical_types, medical_record.medical_type_id)}"
      else
        "##{medical_record.nro_ord} - #{medical_record.exams.first.name.upcase}"
      end
      
    else
      "##{medical_record.id_to_s} - #{select_array(medical_types, medical_record.medical_type_id)}"
    end
    
  end

  def id_to_s
    self.id.to_s.rjust(5, "0")
  end

  def get_initial_history
    MedicalRecord.where(patient_id: self.patient_id, initial: true).order(updated_at: :desc).first
  end

  def pdf_demo
    if self.medical_type_id == 0
      pdf_url_demo = "/pdfs/msp/inicial.pdf"
    elsif self.medical_type_id == 1
      pdf_url_demo = "/pdfs/msp/periodica.pdf"
    elsif self.medical_type_id == 2
      pdf_url_demo = "/pdfs/msp/reintegro.pdf"
    elsif self.medical_type_id == 3
      pdf_url_demo = "/pdfs/msp/retiro.pdf"
    elsif self.medical_type_id == 4
      pdf_url_demo = "/pdfs/msp/evaluacion.pdf"
    elsif self.medical_type_id == 5
      pdf_url_demo = "/pdfs/msp/control.pdf"
    end
  end

  def self.print_data(medical_record, certificate = false)
    pdf = Prawn::Document.new

    if certificate.present?
      PdfTemplate.certificate(pdf, medical_record)
    elsif medical_record.periodic?
      PdfTemplate.periodic(pdf, medical_record)
    end
  
    pdf.render
  end


  def to_xml
    require "builder"
    medical_record = self
    patient        = self.patient
    spe            = self.spe_ophthalmolytic

    xml = ::Builder::XmlMarkup.new
    xml.instruct!

    if self.exams.first && self.exams.first.code == '917'
      xml.evaluation do
        xml.id patient.id
        xml.full_name patient.full_name
        xml.ci "123"
        xml.civil_status patient.civil_status.present? ? patient.civil_status : "N/A"
        xml.age patient.age
        xml.gender patient.gender
        xml.position patient.position.present? ? patient.position.name : "N/A"

        xml.record_id patient.id_to_s
        xml.order_id medical_record.id_to_s

        xml.doctor "Miguel Intriago"
        xml.company "Movistar"

        xml.ophthalmolytic_obs spe.ophthalmolytic_obs

        xml.far_correction spe.far_correction.present? ? "Con correción" : "Sin correción"
        xml.far_left_eye spe.far_left_eye
        xml.far_right_eye spe.far_right_eye

        xml.near_correction spe.near_correction.present? ? "Con correción" : "Sin correción"
        xml.near_left_eye spe.near_left_eye
        xml.near_right_eye spe.near_right_eye

        xml.tonometry_left_eye spe.tonometry_left_eye
        xml.tonometry_right_eye spe.tonometry_right_eye

        xml.color_test_left_eye spe.color_test_left_eye
        xml.color_test_right_eye spe.color_test_right_eye

        xml.perimetry_left_eye spe.perimetry_left_eye
        xml.perimetry_right_eye spe.perimetry_right_eye

        xml.stereopsis_left_eye spe.stereopsis_left_eye
        xml.stereopsis_right_eye spe.stereopsis_right_eye

        xml.ocular_movements spe.ocular_movements
        xml.biomicroscopy spe.biomicroscopy
        xml.eye_background spe.eye_background

        xml.diagnostics medical_record.diagnostics.map{|d| "#{d.code}-#{d.name}"}.join(", ")
        xml.recommendations medical_record.recommendations

      end
    else
      xml.evaluation do
        xml.id patient.id
        xml.full_name patient.full_name
        xml.ci "123"
        xml.civil_status patient.civil_status.present? ? patient.civil_status : "N/A"
        xml.age patient.age
        xml.gender patient.gender
        xml.position patient.position.present? ? patient.position.name : "N/A"

        xml.record_id patient.id_to_s
        xml.order_id medical_record.id_to_s

        xml.doctor "Miguel Intriago"
        xml.company "Movistar"
      end
    end

  end
end
