class CompanyProcess < ApplicationRecord
	belongs_to :parent, class_name: 'CompanyProcess', optional: true
  has_many :processes, class_name: 'CompanyProcess', foreign_key: 'parent_id'
  belongs_to :mananger, class_name: 'Patient', foreign_key: 'owner_id', optional: true

  has_many :activities, class_name: 'Activity', foreign_key: 'process_id'
  accepts_nested_attributes_for :activities
end
