class PersonalProtectionPatient < ApplicationRecord
	validates :personal_protection_id, presence: true
	validates :patient_id, presence: true
	
	belongs_to :personal_protection
  belongs_to :patient

	def id_to_s
    self.id.to_s.rjust(5, "0")
  end
end