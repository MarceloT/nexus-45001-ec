class MedicalRecordFamilyHistory < ApplicationRecord
  belongs_to :medical_record
  belongs_to :family_history
end
