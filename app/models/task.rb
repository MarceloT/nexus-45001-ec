class Task < ApplicationRecord
	belongs_to :project
	belongs_to :owner, class_name: "Patient"

	enum status: [:active, :complete]

  validates :name, :due_date, :project_id, presence: true

  scope :complete, -> { where(status: 1) }
  scope :active, -> { where(status: 0)}
  scope :overdue, -> { where("due_date < ? AND status = ?", Date.today, 0)}

  def overdue?
    self.due_date < Date.today ? true : false
  end

  def status_to_s
    self.overdue? ? "Atrasado" : self.status.to_s == "active" ? "Activo" : "Finalizado"
  end

  def status_class_color
    self.overdue? ? "badge-soft-danger" : self.status.to_s == "active" ? "badge-soft-primary" : "badge-soft-success"
  end
  
end
