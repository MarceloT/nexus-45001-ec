class Commission < ApplicationRecord
  validates :work_center_id, presence: true
	belongs_to :work_center

  def member(nro)
    Patient.find(self["member_#{nro}"])
  end

  def get_delegate
    Patient.find(self.delagate)
  end
  
	DATATABLE_COLUMNS = %w[work_center_id date type_commission active].freeze
	class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{Commission::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end
end
