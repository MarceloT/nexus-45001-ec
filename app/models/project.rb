class Project < ApplicationRecord
  DATATABLE_COLUMNS = %w[id name date status owner_id active].freeze
	# has_many :notes
  has_many :tasks
  # has_many :tags, through: :tasks
  # has_many :comments, through: :tasks
  enum status: [:active, :complete]

  # has_many :user_projects, foreign_key: "collaboration_project_id"
  # has_many :collaborators, through: :user_projects
  belongs_to :owner, class_name: "Patient"
  belongs_to :company
  belongs_to :created_by, class_name: "User"

  scope :complete, -> { where(status: 1) }
  scope :active, -> { where(status: 0)}
  scope :overdue, -> { where("due_date < ? AND status = ?", Date.today, 0)}
  
  validates :name, :owner_id, :due_date, :status, presence: true 

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{Project::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end

  def collaborator_emails=(emails)
    email_array = emails.split(",").map{|email| email.strip}
    email_array.each do |email|
      collaborator = User.find_by(email: email)
      if collaborator == nil || collaborator == self.owner || self.collaborators.include?(collaborator)
        next
      end
      self.collaborators << collaborator
      self.save
    end
  end

  def collaborator_emails
    emails = self.collaborators.collect {|collaborator| collaborator.email}
    emails.join(", ")
  end

  def notes_attributes=(attributes)
    attributes.each do |k,v|
      if v["title"].blank? && v["content"].blank?
        next
      else
        self.notes.build(v)
      end
    end
  end

  def id_to_s
    self.id.to_s.rjust(5, "0")
  end

  def generate_task_number
    self.tasks.count + 1
  end

  def status_to_s
    self.status.to_s == "active" ? "Activo" : "Finalizado"
  end

  def active_tasks
    self.tasks.active 
  end

  def complete_tasks
    self.tasks.complete
  end

  def overdue_tasks
    self.tasks.overdue
  end

  def tasks_complete?
    tasks.any? { |task| task.status == "active"} ? false : true
  end

  def overdue?
    self.due_date < Date.today ? true : false
  end
end
