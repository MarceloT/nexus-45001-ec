class MedicalProfileExam < ApplicationRecord
  belongs_to :medical_profile, foreign_key: "medical_profile_id"
  belongs_to :exam, foreign_key: "exam_id"
end
