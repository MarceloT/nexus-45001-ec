class HealthCycle < ApplicationRecord
	include ReportsKit::Model
	belongs_to :patient
	belongs_to :company
	belongs_to :area, optional: true
	belongs_to :work_center, optional: true

	reports_kit do
    contextual_filter :for_year, ->(relation, context_params) { relation.where(year: context_params[:year]) }
  end
end
