class MedicalCertificate < ApplicationRecord
	belongs_to :patient
	belongs_to :created_by, class_name: "User"
  belongs_to :company

  def id_to_s
    self.id.to_s.rjust(5, "0")
  end

  def self.print_data(medical_certificate)

  	pdf = Prawn::Document.new
  	title = "Certificado Médico"
  	
  	PdfTemplate.header(pdf, title)
  
    pdf.text "Datos Generales:", :style => :bold    
    pdf.move_down 20

    table_data = [
      ["<b>Médico:</b>", "#{medical_certificate.created_by.name}", "<b>Empresa:</b>", "#{medical_certificate.company.name}"], 
      ["<b>Trabajador:</b>", "#{medical_certificate.patient.full_name}", "<b>Fecha de Emisión:</b>", "#{medical_certificate.date}"],
      ["<b>Desde:</b>", "#{medical_certificate.starts_date}", "<b>Hasta:</b>", "#{medical_certificate.ends_date}"]
    ]

    pdf.table(table_data, :column_widths => [135, 135, 135, 135], :cell_style => { :inline_format => true }) do |t|
      t.cells.border_width = 1
    end

    pdf.move_down 20
    pdf.text "Diagnóstico:", :style => :bold
    pdf.text "#{medical_certificate.diagnostic_description}"

    pdf.render
  end


end
