class MedicalEntry < ApplicationRecord
  belongs_to :medical_service, optional: true
  belongs_to :patient
  belongs_to :company

  belongs_to :medical_service

  def exam
    Exam.find_by(code: self.cod_ana)
  end

  def id_to_s
  	self.id.to_s.rjust(5, "0")
  end
end
