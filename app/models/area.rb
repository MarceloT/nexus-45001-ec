class Area < ApplicationRecord
	validates :name, presence: true
	belongs_to :parent, class_name: 'Area', optional: true
  has_many :areas, class_name: 'Area', foreign_key: 'parent_id'
  belongs_to :mananger, class_name: 'Patient', foreign_key: 'user_id', optional: true
  has_many :health_cycles

  def to_s
    name
  end
  
end
