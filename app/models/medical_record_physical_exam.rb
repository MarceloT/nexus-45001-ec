class MedicalRecordPhysicalExam < ApplicationRecord
  belongs_to :medical_record
  belongs_to :physical_exam

  def region
    if self.physical_exam_id.present?
      PhysicalExam.select(:region).find(self.physical_exam_id).region
    end
  end
end
