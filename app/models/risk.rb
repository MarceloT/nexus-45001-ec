class Risk < ApplicationRecord
  DATATABLE_COLUMNS = %w[id code name risk_category_id active].freeze
  belongs_to :risk_category
  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{Risk::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end
end
