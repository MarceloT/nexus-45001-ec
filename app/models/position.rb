class Position < ApplicationRecord
  DATATABLE_COLUMNS = %w[name code gender age_min age_max active].freeze
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true
  # validates :company_id, presence: true
  # validates :area_id, presence: true

  validates :age_min, numericality: { greater_than: 0, less_than_or_equal_to: :age_max }
  validates :age_max, numericality: { less_than_or_equal_to: 100 }

  belongs_to :company, optional: true
  belongs_to :area, optional: true
  belongs_to :medical_profile, optional: true

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{Position::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end

  def to_s
    name
  end
end
