class Recipe < ApplicationRecord
	belongs_to :patient
	belongs_to :doctor, class_name: "User", foreign_key: "doctor_id", optional: true
	
  def id_to_s
    self.id.to_s.rjust(5, "0")
  end

  def self.print_data(recipe)

  	pdf = Prawn::Document.new
    title = "Receta Médica"

  	PdfTemplate.header(pdf, title)
    
    pdf.text "Datos Generales:", :style => :bold    
    pdf.move_down 20

    pdf.text "Medicamentos:", :style => :bold
    pdf.text "#{recipe.medicines}"
    pdf.move_down 20
    pdf.text "Indicaciones:", :style => :bold
    pdf.text "#{recipe.indications}"
    pdf.move_down 20
    pdf.text "Diagnóstico:", :style => :bold
    pdf.text "#{recipe.diagnostic_description}"

    pdf.render
  end
end
