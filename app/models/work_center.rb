class WorkCenter < ApplicationRecord
  has_many :patients
  has_many :health_cycles
	validates :name, presence: true, uniqueness: true
	DATATABLE_COLUMNS = %w[name description active].freeze
	class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{WorkCenter::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end

  def to_s
    name
  end

end
