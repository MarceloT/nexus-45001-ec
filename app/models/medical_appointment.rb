class MedicalAppointment < ApplicationRecord
  validates :patient_id, presence: true
  validates :doctor_id, presence: true
  
  belongs_to :patient
  accepts_nested_attributes_for :patient

  belongs_to :medical_record, :optional => true

  belongs_to :doctor, class_name: "User", foreign_key: "doctor_id"

  has_many :medical_appointment_exams, foreign_key: "medical_appointment_id", :dependent => :destroy
  has_many :exams, through: :medical_appointment_exams, source: :exam
  accepts_nested_attributes_for :medical_appointment_exams, :allow_destroy => true

  def medical_type_color
    if self.medical_type_id == 0
      "#48BA16"
    elsif self.medical_type_id == 1
      "#51A7C5"
    elsif self.medical_type_id == 2
      "#ffc107"
    elsif self.medical_type_id == 3
      "#dc3545"
    elsif self.medical_type_id == 4
      "#59009a"
    elsif self.medical_type_id == 5
      "#dc7345"
    else
      "#ccc"
    end
  end
  
end
