class MedicalAppointmentExam < ApplicationRecord
	belongs_to :medical_appointment, optional: true
  belongs_to :exam
end
