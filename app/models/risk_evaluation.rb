class RiskEvaluation < ApplicationRecord
  DATATABLE_COLUMNS = %w[name position_id area_id date active].freeze
  # validates :name, presence: true, uniqueness: true
  validates :position_id, presence: true
  # validates :process_id, presence: true
  validates :risk_ids, presence: true

  belongs_to :position
  belongs_to :activity
  belongs_to :process, foreign_key: "process_id", class_name: "CompanyProcess", optional: true
  belongs_to :owner, class_name: "Patient"
  belongs_to :company
  belongs_to :created_by, class_name: "User"

  has_many :risk_evaluation_risks, foreign_key: "risk_evaluation_id", :dependent => :destroy
  has_many :risks, through: :risk_evaluation_risks, source: :risk
  accepts_nested_attributes_for :risk_evaluation_risks

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{RiskEvaluation::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end

  def b_count
    self.risk_evaluation_risks.where(:result_gr => "Bajo").count
  end

  def m_count
    self.risk_evaluation_risks.where(:result_gr => "Medio").count
  end

  def a_count
    self.risk_evaluation_risks.where(:result_gr => "Alto").count
  end

  def c_count
    self.risk_evaluation_risks.where(:result_gr => "Crítico").count
  end

end
