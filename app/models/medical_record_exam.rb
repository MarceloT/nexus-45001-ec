class MedicalRecordExam < ApplicationRecord
  belongs_to :medical_record, optional: true
  belongs_to :exam
end
