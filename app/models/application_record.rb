class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  class << self
    def call_rest_api(remote_route, import_credential_token)
    	begin
    		response = RestClient.get remote_route, {Authorization: "Bearer #{import_credential_token}"}
      	JSON.parse(response.body)	
    	rescue Exception => e
    		nil
    	end
    end
  end
end
