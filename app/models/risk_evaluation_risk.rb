class RiskEvaluationRisk < ApplicationRecord
  DATATABLE_COLUMNS = %w[id risk_evaluation_id risk_id].freeze
  belongs_to :risk_evaluation, optional: true
  belongs_to :risk
  belongs_to :risk_category, optional: true

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{RiskEvaluationRisk::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end

  def result_gr_class
    result = self.result
    if result >= 0 && result < 18
      "gp-1"
    elsif result > 18 && result <= 85
      "gp-2"
    elsif result > 85 && result <= 200
      "gp-3"
    elsif result > 200
      "gp-4"
    end
  end

  def result_gr_to_s
    self.result_gr.present? ? self.result_gr : "Bajo"
  end

  def result_m2_class
    result = self.result.to_i
    if result == 0
      "val-0"
    elsif result == 1
      "val-1"
    elsif result == 2
      "val-2"
    elsif result == 3
      "val-3"
    elsif result == 4
      "val-4"
    end
  end

  def result_m2_to_s
    result = self.result.to_i
    if result == 0
      "Riesgo Trivial (T)"
    elsif result == 1
      "Riesgo Tolerable (TO)"
    elsif result == 2
      "Riesgo Moderado (M)"
    elsif result == 3
      "Riesgo Importante (I)"
    elsif result == 4
      "Riesgo Intolerable (IN)"
    end
  end

end
