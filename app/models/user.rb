class User < ApplicationRecord
  DATATABLE_COLUMNS = %w[login email name last_sign_in_at active].freeze
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :trackable,
         :recoverable, :rememberable, :validatable, authentication_keys: [:login]

  validates :login, presence: true, uniqueness: true
  validates :name, presence: true, uniqueness: true
  # validates :role_ids, presence: true

  attr_writer :login_user

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{User::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end

  def login_user
    @login_user || self.login || self.email
  end

  def email_required?
    false
  end
end
