class RiskCategory < ApplicationRecord
  DATATABLE_COLUMNS = %w[name color active].freeze
  has_many :risks
  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{RiskCategory::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end
end
