class Company < ApplicationRecord
  DATATABLE_COLUMNS = %w[name code active].freeze
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{Company::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end

    def import_from_api(code, import_credential_token)
      migrated = Company.where(legacy_id: code).or(Company.where(code: code)).first
      if migrated.present?
        raise ActiveRecord::RecordNotUnique.new "Company already migrated"
      end
      remote_route = "#{Rails.configuration.exchange_api_url}#{Rails.configuration.exchange_api_sources}#{code}"
      ApplicationRecord.call_rest_api(remote_route, import_credential_token);
    end
  end
end
