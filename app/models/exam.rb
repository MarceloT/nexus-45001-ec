class Exam < ApplicationRecord
  DATATABLE_COLUMNS = %w[name code active].freeze
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true

  has_many :medical_profile_exams
  has_many :medical_profiles, through: :medical_profile_exams

  has_many :medical_profile_exams, foreign_key: "medical_profile_id"
  has_many :medical_profiles, through: :medical_profile_exams, source: :exam, foreign_key: "exam_id"

  has_many :medical_record_exams
  has_many :medical_records, through: :medical_record_exams

  has_many :medical_record_exams, foreign_key: "medical_record_id"
  has_many :medical_records, through: :medical_record_exams, source: :exam, foreign_key: "exam_id"

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{Exam::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end

  def full_name
    "#{self.code} #{self.name}"
  end
end
