class PersonalProtectionEntry < ApplicationRecord
	DATATABLE_COLUMNS = %w[id date].freeze
  validates :patient_id, presence: true
	
  belongs_to :patient
	belongs_to :user
  belongs_to :company

  has_many :personal_protection_entry_personal_protections, foreign_key: "personal_protection_entry_id", :dependent => :destroy
  has_many :personal_protections, through: :personal_protection_entry_personal_protections, source: :personal_protection
  accepts_nested_attributes_for :personal_protection_entry_personal_protections, :allow_destroy => true

	class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{PersonalProtectionEntry::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end

  def id_to_s
    self.id.to_s.rjust(5, "0")
  end

  def self.print_data(personal_protection_entry)
    pdf = Prawn::Document.new
    title = "Acta de Entrega EPP"

    PdfTemplate.header(pdf, title)
    
    pdf.text "Datos Generales:", :style => :bold    
    pdf.move_down 20

    table_data = [
      ["<b>Responsable:</b>", "#{personal_protection_entry.user.name}", "<b>Empresa:</b>", "#{personal_protection_entry.company.name}"], 
      ["<b>Entregado a:</b>", "#{personal_protection_entry.patient.full_name}", "<b>Fecha de Entrega:</b>", "#{personal_protection_entry.date}"], 
      ["<b>Observaciones:</b>", {:content => "#{personal_protection_entry.description}", :colspan => 3}]
    ]

    pdf.table(table_data, :column_widths => [135, 135, 135, 135], :cell_style => { :inline_format => true }) do |t|
      t.cells.border_width = 1
    end
    

    pdf.render
  end

end
