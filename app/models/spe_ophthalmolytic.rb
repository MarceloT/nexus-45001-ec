class SpeOphthalmolytic < ApplicationRecord
  belongs_to :medical_record

  validates :medical_record_id, presence: true, uniqueness: true


end
