class MedicalProfile < ApplicationRecord
  DATATABLE_COLUMNS = %w[name code active].freeze
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true

  has_many :medical_profile_exams, foreign_key: "medical_profile_id", :dependent => :destroy
  has_many :exams, through: :medical_profile_exams, source: :exam
  accepts_nested_attributes_for :exams

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{MedicalProfile::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end
  end
end
