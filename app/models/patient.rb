class Patient < ApplicationRecord
  include ActionView::Helpers::NumberHelper
  DATATABLE_COLUMNS = %w[id name name_1 last_name last_name_1 ci work_center_id area_id gender birth_date health_cycles.complete].freeze
  validates :name, presence: true
  validates :last_name, presence: true
  validates :ci, presence: true, uniqueness: true
  validates :ci_type, presence: true
  # validates :position_id, presence: true
  # validates :area_id, presence: true
  # validates :gender, presence: true

  belongs_to :position, optional: true
  belongs_to :work_center, optional: true
  belongs_to :company
  belongs_to :area, optional: true
  has_one :health_cycle
  has_many :medical_records
  has_many :medical_certificates
  has_many :exam_patients
  # Callbacks
  after_update :update_health_cycle
  after_save :create_health_cycle

  class << self
    def datatable_filter(search_value, search_columns)
      return all if search_value.blank?

      result = none
      search_columns.each do |key, value|
        filter = where("#{DATATABLE_COLUMNS[key.to_i]}::text ILIKE ?", "%#{search_value.to_s}%")
        result = result.or(filter) if value['searchable'] == "true"
      end
      result
    end

    def datatable_order(order_column_index, order_dir)
      order("#{Patient::DATATABLE_COLUMNS[order_column_index]} #{order_dir}")
    end

    def import_from_api(code, import_credential_token)
      migrated = Patient.where(legacy_id: code).or(Patient.where(ci: code)).first
      if migrated.present?
        raise ActiveRecord::RecordNotUnique.new "Patient already migrated"
      end
      remote_route = "#{Rails.configuration.exchange_api_url}#{Rails.configuration.exchange_api_patients}#{code}"
      call_rest_api(remote_route, import_credential_token)
    end
  end

  def full_name
    self.last_name.present? ? "#{self.name} #{self.last_name}" : "#{self.name}"
  end

  def full_name_ci
    if self.name.present? && self.last_name.present? && self.ci.present?
      "#{self.ci} - #{self.name} #{self.last_name}"
    elsif self.name.present? && self.last_name.present?
      "#{self.name} #{self.last_name}"
    elsif self.name.present?
      "#{self.name}"
    elsif self.ci
      "#{self.ci}"
    end
  end

  def age
    dob = self.birth_date
    now = Time.now.utc.to_date
    a_year = 0
    a_year = now.year - dob.year - (( now.month > dob.month || (now.month == dob.month && now.day >= dob.day) ) ? 0 : 1) if dob.present?
    if dob && (now.month - dob.month) < 0
      a_month = (now.month - dob.month) + 12
    elsif dob && (now.month - dob.month) > 0
      a_month = now.month - dob.month
    else
      a_month = 0
    end
    "#{a_year}a #{a_month}m"
  end

  def complete
    health_cycle.complete
  end

  def id_to_s
    self.id.to_s.rjust(5, "0")
  end

  def to_xml
    @patient = self
    xml = ::Builder::XmlMarkup.new
    xml.instruct!
    xml.patient do
      xml.id @patient.id
      xml.full_name @patient.full_name
      xml.ci @patient.ci
      xml.civil_status @patient.civil_status
      xml.age @patient.age
      xml.record_id "0001"
    end
  end

  def self.to_xml_jasper(patients, options = {})
    
    xml = ::Builder::XmlMarkup.new(:indent => options[:indent])
    xml.instruct! unless options[:skip_instruct]

    xml.instruct!
    xml.patients do
      patients.each do |patient|
        xml.patient do
          xml.name patient.name
          xml.last_name patient.last_name
          xml.id patient.id
        end
      end
    end
  end

  def update_health_cycle
    Rails.logger.info "##update_health_cycle"
    hc = health_cycle
    if hc.present?
      hc.active = active
      hc.save
    end
  end

  def create_health_cycle
    Rails.logger.info "##create_health_cycle"
    patient = self
    Rails.logger.info "##patient.health_cycle.present?: #{patient.health_cycle.present?}"
    if patient.health_cycle.present? == false
      health_cycle = HealthCycle.create(:year => "2020", :month => "1", :patient_id => patient.id, 
                       :area_id => patient.area_id, :work_center_id => patient.work_center_id,
                       :company_id => patient.company_id, complete: false, active: true)
      
      Rails.logger.info "## health_cycle: #{health_cycle.errors.full_messages}"
    end
  end

  def gender_to_s
    gender == 1 ? "H" : "M"
  end

  def self.by_gender(company_id)
    patient_data = Patient.where(:company_id => 3, active: true)
    total = patient_data.count
    male = patient_data.where(:gender => 1).count
    female = patient_data.where(:gender => 2).count

    male_pecentage = total > 0 ? (male.to_f / total.to_f * 100.0) : 0
    female_pecentage = total > 0 ? (female.to_f / total.to_f * 100.0 ) : 0

    number_male = ActionController::Base.helpers.number_to_percentage(male_pecentage, precision: 0, format: "%n")
    number_female = ActionController::Base.helpers.number_to_percentage(female_pecentage, precision: 0, format: "%n")


    { "Masculino: #{number_male}%" => number_male, "Femenino: #{number_female}%" => number_female}
  end

  def self.by_age(company_id)
    patient_data = Patient.where(:company_id => 3, active: true)
    total = patient_data.count
    
    # male = patient_data.where(:gender => 1).count
    # female = patient_data.where(:gender => 2).count

    # male_pecentage = total > 0 ? (male.to_f / total.to_f * 100.0) : 0
    # female_pecentage = total > 0 ? (female.to_f / total.to_f * 100.0 ) : 0

    number_18 = ActionController::Base.helpers.number_to_percentage(45, precision: 0, format: "%n")
    number_35 = ActionController::Base.helpers.number_to_percentage(10, precision: 0, format: "%n")
    number_50 = ActionController::Base.helpers.number_to_percentage(44, precision: 0, format: "%n")
    number_64 = ActionController::Base.helpers.number_to_percentage(1, precision: 0, format: "%n")
    
    { 
      "18 a 34: #{number_18}%" => number_18,
      "35 a 49: #{number_35}%" => number_35, 
      "50 a 64: #{number_50}%" => number_50,
      "Mas de 64: #{number_64}%" => number_64
    }
  end

  def self.by_work_center(company_id)
    patient_data = Patient.where(:company_id => 3, active: true)
    total = patient_data.count
    
    # male = patient_data.where(:gender => 1).count
    # female = patient_data.where(:gender => 2).count

    # male_pecentage = total > 0 ? (male.to_f / total.to_f * 100.0) : 0
    # female_pecentage = total > 0 ? (female.to_f / total.to_f * 100.0 ) : 0

    number_1 = ActionController::Base.helpers.number_to_percentage(75, precision: 0, format: "%n")
    number_2 = ActionController::Base.helpers.number_to_percentage(10, precision: 0, format: "%n")
    number_3 = ActionController::Base.helpers.number_to_percentage(15, precision: 0, format: "%n")
    
    { 
      "Quito: #{number_1}%" => number_1,
      "Guayaquil: #{number_2}%" => number_2, 
      "Cuenca: #{number_3}%" => number_3,
    }
  end

  def self.by_disease(company_id)
    number_1 = 40.0
    number_2 = 32.0
    number_3 = 22.0
    number_4 = 12.0
    number_5 = 12.0
    number_6 = 12.0
    number_7 = 10.0
    number_8 = 10.0
    number_9 = 6.0
    number_10 = 6.0
    { 
      "Sobrepeso: #{number_1}%" => number_1,
      "Hipertrigliceridemia: #{number_2}%" => number_2, 
      "Hiperuricemia: #{number_3}%" => number_3,
      "Hipercolesterolemia: #{number_4}%" => number_4,
      "Dislipidemia mixta: #{number_5}%" => number_5,
      "Obesidad: #{number_6}%" => number_6,
      "Miopía: #{number_7}%" => number_7,
      "Rinitis alérgica: #{number_8}%" => number_8,
      "Síndrome de ovario poliquístico: #{number_9}%" => number_9,
      "Gastritis: #{number_10}%" => number_10,
    }
  end

  def self.by_system_disease(company_id)
    
    number_1 = 8.0
    number_2 = 40.0
    number_3 = 2.0
    number_4 = 12.0
    number_5 = 4.0
    number_6 = 15.0
    number_7 = 2.0
    number_8 = 6.0


    { 
      "SANGRE Y ORGANOS HEMATOPOYÉTICOS: #{number_1}%" => number_1,
      "ENDOCRINAS, NUTRICIÓN Y METABÓLICAS: #{number_2}%" => number_2, 
      "OJO Y ANEXOS: #{number_3}%" => number_3,
      "PIEL Y TEJIDO CELULAR SUBCUTÁNEO: #{number_4}%" => number_4,
      "SISTEMA DIGESTIVO: #{number_5}%" => number_5,
      "SISTEMA RESPIRATORIO: #{number_6}%" => number_6,
      "SIST. OSTEOMUSCULAR Y TEJIDO CONJUNTIVO: #{number_7}%" => number_7,
      "SISTEMA GENITOURINARIO: #{number_8}%" => number_8
    }
  end

end
