class MedicalRecordVitalConstant < ApplicationRecord
  belongs_to :medical_record
  belongs_to :vital_constant
end
