/*
  / ____| |     | |         | |                      
 | |  __| | ___ | |__   __ _| | __   ____ _ _ __ ___ 
 | | |_ | |/ _ \| '_ \ / _` | | \ \ / / _` | '__/ __|
 | |__| | | (_) | |_) | (_| | |  \ V / (_| | |  \__ \
  \_____|_|\___/|_.__/ \__,_|_|   \_/ \__,_|_|  |___/
*/
// Create a store for link remote of import sources.
var import_source_href = "";
var import_patient_href = "";
toastr.options.closeButton = true;
toastr.options.closeHtml = '<button><i class="icon-off"></i></button>';
toastr.options.closeMethod = 'fadeOut';
toastr.options.closeDuration = 300;
toastr.options.closeEasing = 'swing';
toastr.options.progressBar = true;
/*
  / __ \                           | | |
 | |  | |_ __    ___  ___ _ __ ___ | | |
 | |  | | '_ \  / __|/ __| '__/ _ \| | |
 | |__| | | | | \__ \ (__| | | (_) | | |
  \____/|_| |_| |___/\___|_|  \___/|_|_|
*/
$(window).scroll(function() {
  let count = $(window).scrollTop();
  if (count > 85){
  	$("#sticky-header").addClass('fix-section');
  	width_sidebar = $("#default-drawer").width();
  	$("#sticky-header").css('width', 'calc(100% - ' + width_sidebar + 'px)');
  	$("#to_sticky").appendTo("#sticky-header");
  }
  else{
  	$("#sticky-header").removeClass('fix-section');
  	$("#sticky-header").css('width', 'auto');
  	$("#sticky-header #to_sticky").appendTo("#to_sticky-container");
  }
});
/*$('.container-fluid.page__container').scroll(function() {
  let count = $(this).scrollTop();
  console.log(count);
});*/

/*
 | \ | |         | |         | |  / _(_)    | |   | |    
 |  \| | ___  ___| |_ ___  __| | | |_ _  ___| | __| |___ 
 | . ` |/ _ \/ __| __/ _ \/ _` | |  _| |/ _ \ |/ _` / __|
 | |\  |  __/\__ \ ||  __/ (_| | | | | |  __/ | (_| \__ \
 |_| \_|\___||___/\__\___|\__,_| |_| |_|\___|_|\__,_|___/
*/
$(document).on('nested:fieldAdded', function(event){
  // this field was just inserted into your form
  var field = event.field; 
  // it's a jQuery object already! Now you can find date input
  // var dateField = field.find('.select_2');
  // and activate datepicker on it
  $('#medical_record_diagnostics .select_2, #medical_appointment_exams .select_2, #personal_protection_entry_personal_protections .select_2, #medical_record_exams .select_2').select2({
    language: {
      noResults: function () {
        return "No se encontró resultados";
      }
    },
    placeholder: function(){
      $(this).data('placeholder');
    },
    containerCssClass: ':all:',
    theme: "bootstrap"
  }).on('change', function (evt) {
    console.log(evt);
  });

  console.log("on add new field");
  $('#medical_record_diagnostics .select_2_d, #medical_appointment_exams .select_2_d').select2({
    language: {
      noResults: function () {
        return "No se encontró resultados";
      }
    },
    placeholder: function(){
      $(this).data('placeholder');
    },
    containerCssClass: ':all:',
    theme: "bootstrap",
    dropdownParent: $('#modal .modal-body')
  }).on('change', function (evt) {
    console.log(evt);
  });

  $("#medical_record_diagnostics .date-selector, #medical_appointment_exams .date-selector, #personal_protection_entry_personal_protections .date-selector, #medical_record_exams .date-selector").flatpickr({
    enableTime: false,
    dateFormat: "Y-m-d",
  });    
  
})

/*
  _    _             _                           _       _          _                   
 | |  | |           | |                         | |     | |        (_)                  
 | |__| | ___   ___ | | __   _ __ ___   ___   __| | __ _| |  __   ___  _____      _____ 
 |  __  |/ _ \ / _ \| |/ /  | '_ ` _ \ / _ \ / _` |/ _` | |  \ \ / / |/ _ \ \ /\ / / __|
 | |  | | (_) | (_) |   <   | | | | | | (_) | (_| | (_| | |   \ V /| |  __/\ V  V /\__ \
 |_|  |_|\___/ \___/|_|\_\  |_| |_| |_|\___/ \__,_|\__,_|_|    \_/ |_|\___| \_/\_/ |___/
*/
$('#modal-holder').on('show.bs.modal', function (event) {
  let modal = $(this)
  // Import Sources
  let import_nl_source_link = modal.find('#import_nl_source_link');
  import_source_href = import_nl_source_link.prop('href');
  import_nl_source_link.bind('ajax:success', success_source_import);
  import_nl_source_link.bind('ajax:beforeSend', display_import_start);
  import_nl_source_link.bind('ajax:error', display_import_error);
  // Import Patients
  let import_nl_patient_link = modal.find('#import_nl_patient_link');
  import_patient_href = import_nl_patient_link.prop('href');
  import_nl_patient_link.bind('ajax:success', success_patient_import);
  import_nl_patient_link.bind('ajax:beforeSend', display_import_start);
  import_nl_patient_link.bind('ajax:error', display_import_error);
});

$('#modal-holder').on('hide.bs.modal', function (event) {
  setTimeout(function(){
    if($('.modal-backdrop.fade.show').length > 0) {
      $('.modal-backdrop.fade.show').remove();
    }
  }, 5);
});

/*
  __  __       _                     _   _                 
 |  \/  |     (_)                   | | (_)                
 | \  / | __ _ _ _ __      __ _  ___| |_ _  ___  _ __  ___ 
 | |\/| |/ _` | | '_ \    / _` |/ __| __| |/ _ \| '_ \/ __|
 | |  | | (_| | | | | |  | (_| | (__| |_| | (_) | | | \__ \
 |_|  |_|\__,_|_|_| |_|   \__,_|\___|\__|_|\___/|_| |_|___/
*/

$(function(){
  // Getting COD_ORI for importing a source
  $(document).delegate('#new_company #company_code', 'change', function(a,b,c) {
    let new_href = updateQueryStringParameter(import_source_href, 'code', $(this).val());
    $('#import_nl_source_link').prop('href', new_href)
  })
  // Getting COD_PAC for importing a source
  $(document).delegate('#new_patient #patient_ci', 'change', function(a,b,c) {
    let new_href = updateQueryStringParameter(import_patient_href, 'code', $(this).val());
    $('#import_nl_patient_link').prop('href', new_href)
  })
  // Handle ChackAll checkbox
  $(document).delegate('.js-toggle-check-all', 'click', function(e){
    let toggle_status = $(this).prop("checked");
    let target_container = $(this).data('target');
    $(target_container).find('.custom-checkbox').each(function(i,el){
      let input = $(this).find('input:checkbox');
      input.prop("checked", toggle_status);
    });
  });
  // Handle each chackbox
  $(document).delegate('.js-check-selected-row', 'click', function(e){
    let curr_el = $(this);
    while(!curr_el.hasClass('list')){
      curr_el = curr_el.parent();
    }
    let list_container = curr_el;
    let chackbox_toggler = list_container.parent().find('.js-toggle-check-all');
    let all_checked = true;
    list_container.find('input:checkbox').each(function(i, e){
      if (!$(this).is(":checked")) {
        all_checked = false;
        return false;
      }
    });
    chackbox_toggler.prop("checked", all_checked);
  });
  $('.container-fluid.page__container #to_sticky').width($('.container-fluid.page__container #to_sticky-container').width());

});

/*
   _____ _       _           _     __                  _   _                 
  / ____| |     | |         | |   / _|                | | (_)                
 | |  __| | ___ | |__   __ _| |  | |_ _   _ _ __   ___| |_ _  ___  _ __  ___ 
 | | |_ | |/ _ \| '_ \ / _` | |  |  _| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
 | |__| | | (_) | |_) | (_| | |  | | | |_| | | | | (__| |_| | (_) | | | \__ \
  \_____|_|\___/|_.__/ \__,_|_|  |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
*/

var updateQueryStringParameter = (uri, key, value) => {
  value = encodeURIComponent(value);
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}
var success_source_import = (event) => {
  const [response, status, xhr] = event.detail;
  $('#modal-holder .dynamic-content').html(response.company);
  toastr.success('Empresa encontrada');
}
var success_patient_import = (event) => {
  const [response, status, xhr] = event.detail;
  $('#modal-holder .dynamic-content').html(response.patient);
  toastr.success('Paciente encontrado');
}
var display_import_start = (event) => {
  //toastr.info('buscando registros para importar');
}
var display_import_error = (event) => {
  const [response, status, xhr] = event.detail;
  if (xhr.status === 403) {
    toastr.warning('Esta información ya fue registrada.');
  }
  if (xhr.status === 404) {
    toastr.warning('No se han encontrado registros para importar.');
  }
  if (xhr.status === 500) {
    toastr.error('¡Ha ocurrido un error al importar!');
  }
  if (xhr.status === 520) {
    toastr.info('¡Ingrese código o información de búsqueda!');
  }
}

$(".sidebar-toggle-action").click(function() {
  $(".sidebar-rigth-app").toggleClass("extend");

  if ($(".sidebar-rigth-app").hasClass("extend")){
    $(".sidebar-toggle-action").html("<");
    $(".sidebar-content").show();
  } else {
    $(".sidebar-toggle-action").html(">");
    $(".sidebar-content").hide();
  }
  
});

function addParameter(url, parameterName, parameterValue, atStart/*Add param before others*/){
  replaceDuplicates = true;
  if(url.indexOf('#') > 0){
    var cl = url.indexOf('#');
    urlhash = url.substring(url.indexOf('#'),url.length);
  } else {
    urlhash = '';
    cl = url.length;
  }
  sourceUrl = url.substring(0,cl);

  var urlParts = sourceUrl.split("?");
  var newQueryString = "";

  if (urlParts.length > 1)
  {
    var parameters = urlParts[1].split("&");
    for (var i=0; (i < parameters.length); i++)
    {
      var parameterParts = parameters[i].split("=");
      if (!(replaceDuplicates && parameterParts[0] == parameterName))
      {
        if (newQueryString == "")
          newQueryString = "?";
        else
          newQueryString += "&";
        newQueryString += parameterParts[0] + "=" + (parameterParts[1]?parameterParts[1]:'');
      }
    }
  }
  if (newQueryString == "")
    newQueryString = "?";

  if(atStart){
    newQueryString = '?'+ parameterName + "=" + parameterValue + (newQueryString.length>1?'&'+newQueryString.substring(1):'');
  } else {
    if (newQueryString !== "" && newQueryString != '?')
      newQueryString += "&";
    newQueryString += parameterName + "=" + (parameterValue?parameterValue:'');
  }
  return urlParts[0] + newQueryString + urlhash;
};

