//** CORE
//= require jquery/dist/jquery.min.js
//= require popper.js/dist/umd/popper.js
//= require bootstrap/dist/js/bootstrap.min.js
//= require jquery.dataTables.min
//= require simplebar/dist/simplebar.min.js
//= require toastr/build/toastr.min.js


//= require dom-factory/dist/dom-factory.js
//= require material-design-kit/dist/material-design-kit.js

//= require rails-ujs
//= require data_tables
//= require jsdelivr/jsdelivr
//= require select2/select2.full
//= require range_slider/ion.rangeSlider.min
//= require multi_select/multi-select.js
//= require multi_select/jquery.quicksearch.js
//= require canvasjs.min
//= require modal
//= require orgchart
//= require positions
//= require jquery_nested_form
//= require reports_kit/application

//= require fullcalendar/packages/core/main.js
//= require fullcalendar/packages/interaction/main.js
//= require fullcalendar/packages/daygrid/main.js
//= require fullcalendar/packages/timegrid/main.js
//= require fullcalendar/packages/list/main.js
//= require fullcalendar/packages/core/locales/es.js
 
//= require turbolinks

//** PLUGIN SCRIPTS (NODE_MODULES)
// PRO ONLY

//** APP SETTINGS
// PRO ONLY

//** PLUGIN WRAPPERS & INITS
// PRO ONLY
