
// Self Initialize DOM Factory Components
// domFactory.handler.autoInit();

// Connect button(s) to drawer(s)
var sidebarToggle = Array.prototype.slice.call(document.querySelectorAll('[data-toggle="sidebar"]'));
sidebarToggle.forEach(function(e) {
    e.addEventListener("click", function(e) {
        var t = e.currentTarget.getAttribute("data-target") || "#default-drawer",
            a = document.querySelector(t);
        a && a.mdkDrawer.toggle()
    })
});
var drawers = document.querySelectorAll(".mdk-drawer");
(drawers = Array.prototype.slice.call(drawers)).forEach(function(e) {
    e.addEventListener("mdk-drawer-change", function(e) {
        if (e.target.mdkDrawer) {
            document.querySelector("body").classList[e.target.mdkDrawer.opened ? "add" : "remove"]("has-drawer-opened");
            var t = document.querySelector('[data-target="#' + e.target.id + '"]');
            t && t.classList[e.target.mdkDrawer.opened ? "add" : "remove"]("active")
        }
    })
}), $(".sidebar .collapse").on("show.bs.collapse", function(e) {
    e.stopPropagation();
    var t = $(this).parents(".sidebar-submenu").get(0) || $(this).parents(".sidebar-menu").get(0);
    $(t).find(".open").find(".collapse").collapse("hide"), $(this).closest("li").addClass("open")
}), $(".sidebar .collapse").on("hidden.bs.collapse", function(e) {
    e.stopPropagation(), $(this).closest("li").removeClass("open")
}), $('[data-toggle="tooltip"]').tooltip(), $('[data-toggle="tab"]').on("hide.bs.tab", function(e) {
    $(e.target).removeClass("active")
});

// ENABLE TOOLTIPS
$('[data-toggle="tooltip"]').tooltip()

load_table = true

$(document).on("turbolinks:load",function(){
  if (load_table){
    load_tables(); 
    load_table = false
  }
  
  $(".loader-container").hide();
  console.log("load");  


})

$(document).on("turbolinks:before-render",function(){
  console.log("before-render");
  if (typeof myInterval != 'undefined' && typeof auto_save != 'undefined') {
    clearInterval(myInterval);
    clearInterval(auto_save);
  }
})

$(document).on("turbolinks:render",function(){
  console.log("render");

  // Load reports
  $('.reports_kit_report').each(function(index, el) {
    var el = $(el)
    console.log(el.find(".reports_kit_visualization").is(':empty'))
    if (el.find(".reports_kit_visualization").is(':empty')){
      var reportClass = el.data('report-class');
      new ReportsKit[reportClass]().render({ 'el': el });
    }
  });  
})

$(document).on("turbolinks:request-start",function(){
  console.log("request-start");
  load_table = true
  $(".loader-container").show();


})


$(document).on('page:fetch', function() {
  console.log("fetch");
  $(".loading-indicator").show();
});
$(document).on('page:change', function() {
  console.log("change");
  $(".loading-indicator").hide();
});

