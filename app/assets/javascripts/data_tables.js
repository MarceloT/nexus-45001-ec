function load_tables(){
  lng = {
    "sProcessing":     "",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Registros del _START_ al _END_ de un total de _TOTAL_",
    "sInfoEmpty":      "Registros del 0 al 0 de un total de 0",
    "sInfoFiltered":   "(total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Siguiente",
      "sPrevious": "Anterior"
    },
    "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  }

  // Companies
  $('#companies').DataTable({
    ajax: {
      url: '/companies_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Código', data: 'code', class: 'text-aling-left'},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "language": lng
  });

  // Work Centers
  $('#work_centers').DataTable({
    ajax: {
      url: '/work_centers_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Descipción', data: 'description', class: 'text-aling-left'},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "language": lng
  });

  // Positions
  $('#positions').DataTable({
    ajax: {
      url: '/positions_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Código', data: 'code', class: 'text-aling-left'},
      {title: 'Género', data: 'gender', class: 'text-aling-left', "searchable": false},
      {title: 'Edad Min', data: 'age_min', class: 'text-aling-left', "searchable": false},
      {title: 'Edad Max', data: 'age_max', class: 'text-aling-left', "searchable": false},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  // Patients
  $('#patients').DataTable({
    ajax: {
      url: '/patients_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'ID', data: 'id', class: 'text-aling-left', "visible": false},
      {title: 'Trabajor', data: 'full_name', class: 'text-aling-left'},
      {title: 'Segundo Nombre', data: 'full_name', class: 'text-aling-left', "visible": false},
      {title: 'Apellido P', data: 'full_name', class: 'text-aling-left', "visible": false},
      {title: 'Apellido M', data: 'full_name', class: 'text-aling-left' , "visible": false},
      {title: 'C.I/RUC', data: 'ci', class: 'text-aling-left', "searchable": true},
      {title: 'Puesto de Trabajo', data: 'position', class: 'text-aling-left', "searchable": false},
      {title: 'Sexo', data: 'gender', class: 'text-aling-left', "searchable": false},
      {title: 'Edad', data: 'age', class: 'text-aling-left', "searchable": false},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  // Patients
  $('#patient_records').DataTable({
    ajax: {
      url: '/patient_records_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nro.', data: 'id', class: 'text-aling-left'},
      {title: 'Trabajor', data: 'full_name', class: 'text-aling-left'},
      {title: 'Segundo Nombre', data: 'full_name', class: 'text-aling-left', "visible": false},
      {title: 'Apellido P', data: 'full_name', class: 'text-aling-left', "visible": false},
      {title: 'Apellido M', data: 'full_name', class: 'text-aling-left' , "visible": false},
      {title: 'C.I/RUC', data: 'ci', class: 'text-aling-left', "searchable": true},
      {title: 'Origen', data: 'company', class: 'text-aling-left', "searchable": false},
      {title: 'Centro Trabajo', data: 'work_center', class: 'text-aling-left', "searchable": false},
      {title: 'Sexo', data: 'gender', class: 'text-aling-left', "searchable": false},
      {title: 'Edad', data: 'age', class: 'text-aling-left', "searchable": false},
      {title: 'Completo', data: 'complete', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  // Medical Services
  $('#medical_services').DataTable({
    ajax: {
      url: '/medical_services_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Examen', data: 'exam', class: 'text-aling-left'},
      {title: 'Jasper', data: 'jasper_template_url', class: 'text-aling-left', "searchable": false},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  // Commissions
  $('#commissions').DataTable({
    ajax: {
      url: '/commissions_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Centro de Trabajo', data: 'work_center', class: 'text-aling-left'},
      {title: 'Tipo Comisión', data: 'type_commission', class: 'text-aling-left'},
      {title: 'Fecha', data: 'date', class: 'text-aling-left'},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  // Exam
  $('#exams').DataTable({
    ajax: {
      url: '/exams_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Código', data: 'code', class: 'text-aling-left'},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  // Diagnostics
  $('#diagnostics').DataTable({
    ajax: {
      url: '/diagnostics_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Código', data: 'code', class: 'text-aling-left'},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  // Personal Protections
  $('#personal_protections').DataTable({
    ajax: {
      url: '/personal_protections_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Categoria', data: 'type', class: 'text-aling-left', "searchable": false},
      {title: 'Cantidad', data: 'stock', class: 'text-aling-left'},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  // Personal Protections Entries
  $('#personal_protection_entries').DataTable({
    ajax: {
      url: '/personal_protection_entries_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nro', data: 'id', class: 'text-aling-left'},
      {title: 'Responsable', data: 'user', class: 'text-aling-left', "searchable": false},
      {title: 'Entregado a', data: 'patient', class: 'text-aling-left', "searchable": false},
      {title: 'Fecha', data: 'date', class: 'text-aling-left', "searchable": false},
      {title: 'Equipo de Proteción', data: 'personal_protection', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  


  function format ( d ) {
    // `d` is the original data object for the row
    return '<div>'+
              '<label>Exámenes asignados:</label>'+
              '<div style="width: 100%">'+
                d.exams +
              '</div>'+
            '</div>';
  }

  var table = $('#medical_profiles').DataTable({
    ajax: {
      url: '/medical_profiles_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {
        "className":      'details-control',
        "orderable":      false,
        "data":           null,
        "defaultContent": '', 
        "orderable": false
      },
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Código', data: 'code', class: 'text-aling-left'},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false, "orderable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "language": lng
  });


  function call_select(){
    $('select.select_2').select2({
      language: {
        noResults: function () {
          return "No se encontró resultados";
        }
      },
      placeholder: function(){
        $(this).data('placeholder');
      },
      containerCssClass: ':all:',
      theme: "bootstrap"
    });
  }

  $('#medical_profiles tbody').on('click', 'td.details-control', function () {
      var tr = $(this).closest('tr');
      var row = table.row( tr );

      if ( row.child.isShown() ) {
          // This row is already open - close it
          row.child.hide();
          tr.removeClass('shown');
      }
      else {
          // Open this row
          row.child( format(row.data()) ).show();
          tr.addClass('shown');
      }
      call_select();
  });


  // Patients
  $('#patients_history').DataTable({
    ajax: {
      url: '/patients_dataset',
      dataSrc: 'items',
    },
    serverSide: true,
    columns: [
      {title: 'Nombre', data: 'full_name', class: 'text-aling-left'},
      {title: 'C.I', data: 'ci', class: 'text-aling-left'},
      {title: 'Cargo', data: 'position', class: 'text-aling-left'},
      {title: 'Sexo', data: 'gender', class: 'text-aling-left', "searchable": false},
      {title: 'Edad', data: 'age', class: 'text-aling-left', "searchable": false},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "language": lng
  });

  // Patients
  $('#users').DataTable({
    ajax: {
      url: '/users_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nombre ', data: 'name', class: 'text-aling-left'},
      {title: 'Login', data: 'login', class: 'text-aling-left'},
      {title: 'Email', data: 'email', class: 'text-aling-left', "searchable": false},
      {title: 'Rol', data: 'role', class: 'text-aling-left', "searchable": false},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "language": lng
  });

  //Projects
  $('#projects').DataTable({
    ajax: {
      url: '/projects_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nro', data: 'id', class: 'text-aling-left', "visible": true},
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Fecha', data: 'date', class: 'text-aling-left'},
      {title: 'Estado', data: 'status', class: 'text-aling-left', "searchable": false},
      {title: 'Responsable', data: 'owner', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  // Risks
  $('#risk_categories').DataTable({
    ajax: {
      url: '/risk_categories_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Color', data: 'color', class: 'text-aling-left'},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "language": lng
  });

  $('#risks').DataTable({
    ajax: {
      url: '/risks_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Id', data: 'id', class: 'text-aling-left', "visible": false},
      {title: 'Código', data: 'code', class: 'text-aling-left'},
      {title: 'Nombre', data: 'name', class: 'text-aling-left'},
      {title: 'Tipo', data: 'category', class: 'text-aling-left'},
      {title: 'Activo', data: 'active', class: 'text-aling-left', "searchable": false},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "pageLength": 20,
    "lengthMenu": [ 10, 20, 50, 75, 100 ],
    "language": lng
  });

  $('#risk_evaluations').DataTable({
    ajax: {
      url: '/risk_evaluations_dataset',
      dataSrc: 'items',
    },
    processing: true,
    serverSide: true,
    columns: [
      {title: 'Matriz de Evaluación', data: 'name', class: 'text-aling-left'},
      {title: 'Fecha', data: 'date', class: 'text-aling-left'},
      {title: 'Metodología', data: 'type', class: 'text-aling-left'},
      {title: 'Responsable', data: 'owner', class: 'text-aling-left'},
      {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
    ],
    order: [['0', 'asc']],
    "language": lng
  });






  var evaluation_id = $("#evaluation_id").val();
  if (evaluation_id){


    var risks_details_table_1 = $('#risks_details_1').DataTable({
      ajax: {
        url: '/risk_evaluations_details_dataset',
        dataSrc: 'items',
        data: {
          evaluation_id: function() { return $('#evaluation_id').val() },
          category_id: function() { return $('#category_id').val() },
          val_id: function() { return $('#val_id').val() }
        }
      },
      "paging": false,
      "info": false,
      "searching": false,
      processing: true,
      serverSide: true,
      columns: [
        {title: 'Nro', data: 'nro', class: 'text-number text-aling-center', "searchable": false, "orderable": false},
        {title: 'Categoria', data: 'risk_category', class: 'text-aling-left', "searchable": false, "orderable": false},
        {title: 'H', data: 'm_count', class: 'text-number text-aling-left', "searchable": false, "orderable": false},
        {title: 'M', data: 'f_count', class: 'text-number text-aling-left', "searchable": false, "orderable": false},
        {title: 'D', data: 's_count', class: 'text-number text-aling-left', "searchable": false, "orderable": false},
        {title: 'Riesgo', data: 'name', class: 'text-aling-left', "orderable": false},
        {title: 'Prob.', data: 'reference_value', class: 'text-aling-left', "orderable": false},
        {title: 'Cons.', data: 'medium_value', class: 'text-aling-left', "orderable": false},
        {title: 'Exp.', data: 'exposition_value', class: 'text-aling-left', "orderable": false},
        {title: 'Nivel', data: 'result', class: 'text-aling-left', "orderable": false},
        {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
      ],
      "language": lng
    });

    var risks_details_table_2 = $('#risks_details_2').DataTable({
      ajax: {
        url: '/risk_evaluations_details_dataset',
        dataSrc: 'items',
        data: {
          evaluation_id: function() { return $('#evaluation_id').val() },
          category_id: function() { return $('#category_id').val() },
          val_id: function() { return $('#val_id').val() }
        }
      },
      "paging": false,
      "info": false,
      "searching": false,
      "ordering": false,
      processing: true,
      serverSide: true,
      columns: [
        {title: 'Nro', data: 'nro', class: 'text-number text-aling-center', "searchable": false, "orderable": false},
        {title: 'Factor', data: 'risk_category', class: 'text-aling-left', "searchable": false, "orderable": false},
        {title: 'H', data: 'm_count', class: 'text-number text-aling-center', "searchable": false, "orderable": false},
        {title: 'M', data: 'f_count', class: 'text-number text-aling-center', "searchable": false, "orderable": false},
        {title: 'D', data: 's_count', class: 'text-number text-aling-center', "searchable": false, "orderable": false},
        {title: 'Riesgo', data: 'name', class: 'text-aling-left', "orderable": false},
        {title: 'Probabilidad', data: 'reference_value', class: 'text-aling-left', "orderable": false},
        {title: 'Consecuencia.', data: 'medium_value', class: 'text-aling-left', "orderable": false},
        {title: 'Valoración', data: 'result', class: 'text-aling-left', "orderable": false},
        {title: 'Acciones', data: 'edit', class: 'text-aling-left', "searchable": false, "orderable": false}
      ],
      "language": lng
    });


    $('button').click( function() {
      console.log("on load click");
      risks_details_table_1.ajax.reload();
      risks_details_table_2.ajax.reload();
      return false;
    });

  }
}
