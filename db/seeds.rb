require 'faker'
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Users
user = User.create(login: 'admin', email: "admin@admin.com", password: "123456", name: "Administrador del Sistema")
user.add_role "admin"
user = User.create(login: 'med', email: "med@med.com", password: "123456", name: "Médico ocupacional")
user.add_role "doctor"
user = User.create(login: 'tec', email: "tec@tec.com", password: "123456", name: "Técnico ocupacional")
user.add_role "technical"

p "Load User"

# Company 1
company = Company.create(name: 'NTI', code: "NTI", active: true, mission: "", mission: "")
main_area = Area.create(name: company.name, code: company.code, active: true, parent_id: 0, company_id: company.id)
first_area = Area.create(name: 'Finaciero', code: "AF", active: true, parent_id: main_area.id, company_id: company.id)
Area.create(name: 'Compras', code: "FC", active: true, parent_id: first_area.id, company_id: company.id)
Area.create(name: 'Ventas', code: "FV", active: true, parent_id: first_area.id, company_id: company.id)
Area.create(name: 'Contable', code: "AC", active: true, parent_id: main_area.id, company_id: company.id)
Area.create(name: 'Sistemas', code: "AS", active: true, parent_id: main_area.id, company_id: company.id)
Area.create(name: 'Bodega', code: "AB", active: true, parent_id: main_area.id, company_id: company.id)

p "Load Companies and Areas"

# Examns
require 'csv'
datafile = Rails.root + 'db/examenes.csv'
CSV.foreach(datafile, headers: true) do |row|
  exam = Exam.where(:name => row[0]).last
  unless exam.present?
    e = Exam.new
    e.code = row[0]
    e.name = row[1]
    e.active = row[2]
    e.save if e.validate
  end
end

p "Load Examns"
MedicalService.create({company_id: 3, name: "Medicina Ocupacional - Preocupacional", active: true, jasper_template_url: "public/reports/medical_record_mov.odt", exam_id: 223})
MedicalService.create({company_id: 3, name: "Medicina Ocupacional - Periódica", active: true, jasper_template_url: "public/reports/medical_record_mov.odt", exam_id: 223})
MedicalService.create({company_id: 3, name: "Medicina Ocupacional - Reintegro", active: true, jasper_template_url: "public/reports/medical_record_mov.odt", exam_id: 223})
MedicalService.create({company_id: 3, name: "Medicina Ocupacional - Retiro", active: true, jasper_template_url: "public/reports/medical_record_mov.odt", exam_id: 223})

service_1 = MedicalService.create({company_id: nil, name: "Valoracion Oftalmologica (Única)", active: true, jasper_template_url: "public/reports/medical_record.odf", exam_id: 1615})
service_2 = MedicalService.create({company_id: 3, name: "Valoracion Oftalmologica (Movistar)", active: true, jasper_template_url: "public/reports/medical_record_mov.odt", exam_id: 1615})
p "Load Services"

#Medical profile
medical_profile = MedicalProfile.create(name: "Perfil Ejecutivo 2019", code: "DE2019", active: true, company_id: company.id)
medical_profile.exam_ids = [1, 2, 3, 4]

#Positions 1
Position.create(name: 'Director Ejecutivo',company_id: company.id, area_id: first_area.id, medical_profile_id: medical_profile.id, code: "CEO", gender: 3, age_min: 25, age_max: 35, active: true, number: 0, mission: "")
Position.create(name: 'Director de Operaciones', company_id: company.id, area_id: first_area.id, medical_profile_id: medical_profile.id, code: "COO", gender: 3, age_min: 25, age_max: 35, active: true, number: 1, mission: "Responsable operativo")
Position.create(name: 'Director Comercial', company_id: company.id, area_id: first_area.id, medical_profile_id: medical_profile.id, code: "CSO", gender: 3, age_min: 25, age_max: 35, active: true, number: 2, mission: "Responsable operativo")
Position.create(name: 'Director de Marketing', company_id: company.id, area_id: first_area.id, medical_profile_id: medical_profile.id, code: "CMO", gender: 3, age_min: 25, age_max: 35, active: true, number: 3, mission: "Responsable de marketing")
Position.create(name: 'Director de Recursos Humanos', company_id: company.id, area_id: first_area.id, medical_profile_id: medical_profile.id, code: "CHRO", gender: 3, age_min: 25, age_max: 35, active: true, number: 4, mission: "Responsable de las Personas")

Patient.create(ci: '0000000001', ci_type: 2, name: 'María', last_name: 'Acevedo', position_id: 1, birth_date: Time.now - 25.years, gender: 1, active: true, civil_status: 1, company_id: company.id, area_id: first_area.id)
Patient.create(ci: '0000000002', ci_type: 2, name: 'Fredy', last_name: 'Aguilar', position_id: 2, birth_date: Time.now - 24.years, gender: 2, active: true, civil_status: 1, company_id: company.id, area_id: first_area.id)
Patient.create(ci: '0000000003', ci_type: 2, name: 'Alfredo', last_name: 'Barrera', position_id: 3, birth_date: Time.now - 28.years, gender: 2, active: true, civil_status: 1, company_id: company.id, area_id: first_area.id)

p "Load Positions and Patients"
#Company 2
company_2 = Company.create(name: 'Petro Ecuador', code: "PE", active: false, mission: "", mission: "")
main_area_2 = Area.create(name: company_2.name, code: company_2.code, active: true, parent_id: 0, company_id: company_2.id)
first_area_1 = Area.create(name: 'Finaciero', code: "AF", active: true, parent_id: main_area_2.id, company_id: company_2.id)
Area.create(name: 'Compras', code: "FC", active: true, parent_id: first_area_1.id, company_id: company_2.id)
Area.create(name: 'Ventas', code: "FV", active: true, parent_id: first_area_1.id, company_id: company_2.id)
Area.create(name: 'Contable', code: "AC", active: true, parent_id: main_area_2.id, company_id: company_2.id)
Area.create(name: 'Sistemas', code: "AS", active: true, parent_id: main_area_2.id, company_id: company_2.id)
Area.create(name: 'Bodega', code: "AB", active: true, parent_id: main_area_2.id, company_id: company_2.id)


# Risk
RiskCategory.create(name: "Mecánicos", color: "#ffc107", active: true)
RiskCategory.create(name: "Físicos", color: "#51A7C5", active: true)
RiskCategory.create(name: "Químicos", color: "#308AF3", active: true)
RiskCategory.create(name: "Biológicos", color: "#48BA16", active: true)
RiskCategory.create(name: "Ergonómicos", color: "#dc3545", active: true)
RiskCategory.create(name: "Psicosociales", color: "#5163C5", active: true)

require 'csv'
datafile = Rails.root + 'db/riesgos.csv'
CSV.foreach(datafile, headers: true) do |row|
  risk = Risk.where(:name => row[1]).last
  unless risk.present?
    e = Risk.new
    e.code = row[0]
    e.name = row[1]
    e.description = row[2]
    e.risk_category_id = row[3]
    e.active = true
    e.save
  end
end

p "Load Risks"

# History record 
ToxicHabit.create(name: "Tabaco")
ToxicHabit.create(name: "Alcohol")
ToxicHabit.create(name: "Consume Drogras")

Lifestyle.create(name: "Actividad Física")
Lifestyle.create(name: "Medicación Habitual")

FamilyHistory.create(name: "Enfermedad Cardio-vascular")
FamilyHistory.create(name: "Enfermedad Metabólica")
FamilyHistory.create(name: "Enfermedad Neurológica")
FamilyHistory.create(name: "Enfermedad Neurológica")
FamilyHistory.create(name: "Enfermedad Infecciosa")
FamilyHistory.create(name: "Enfermedad Hereditaria / Congénita")
FamilyHistory.create(name: "Discapacidades")
FamilyHistory.create(name: "Otros")

SystemRevision.create(name: "Piel - Anexos")
SystemRevision.create(name: "Órganos De Los Sentidos")
SystemRevision.create(name: "Respiratorio")
SystemRevision.create(name: "Cardio-vascular")
SystemRevision.create(name: "Digestivo")
SystemRevision.create(name: "Genito - Urinario")
SystemRevision.create(name: "Músculo Esquelético")
SystemRevision.create(name: "Endocrino")
SystemRevision.create(name: "Hemo Linfático")
SystemRevision.create(name: "Nervioso")

VitalConstant.create(name: "PRESIÓN ARTERIAL(mmHg)")
VitalConstant.create(name: "TEMPERATURA (°C)")
VitalConstant.create(name: "FRECUENCIA CARDIACA (Lat/min)")
VitalConstant.create(name: "SATURACIÓN DE OXÍGENO (O2%)")
VitalConstant.create(name: "FRECUENCIA RESPIRATORIA (fr/min)")
VitalConstant.create(name: "PESO (Kg)")
VitalConstant.create(name: "TALLA (cm)")
VitalConstant.create(name: "ÍNDICE DE MASA CORPORAL (Kg/m2)")
VitalConstant.create(name: "PERÍMETRO ABDOMINAL (cm)")

require 'csv'
datafile = Rails.root + 'db/cied.csv'
CSV.foreach(datafile, headers: true) do |row|
  risk = Diagnostic.where(:code => row[0].strip.to_s).last
  unless risk.present?
    e = Diagnostic.new
    e.code = row[0].strip.to_s if row[0].present?
    e.name = row[1].strip.to_s if row[1].present?
    e.save
  end
end

p "Load CIED"

PhysicalExam.create(name: "a. Cicatrices", region: "1. Piel")
PhysicalExam.create(name: "b. Tatuajes", region: "1. Piel")
PhysicalExam.create(name: "c. Piel  y faneras", region: "1. Piel")
PhysicalExam.create(name: "a. Párpados", region: "2. Ojos")
PhysicalExam.create(name: "b. Conjuntivas", region: "2. Ojos")
PhysicalExam.create(name: "c. Pupilas", region: "2. Ojos")
PhysicalExam.create(name: "d. Córnea", region: "2. Ojos")
PhysicalExam.create(name: "e. Motilidad", region: "2. Ojos")
PhysicalExam.create(name: "a. C. auditivo externo", region: "3. Oído")
PhysicalExam.create(name: "b. Pabellón", region: "3. Oído")
PhysicalExam.create(name: "c. Tímpanos", region: "3. Oído")
PhysicalExam.create(name: "a. Labios", region: "4. Oro faringe")
PhysicalExam.create(name: "b. Lengua", region: "4. Oro faringe")
PhysicalExam.create(name: "c. Faringe", region: "4. Oro faringe")
PhysicalExam.create(name: "d. Amígdalas", region: "4. Oro faringe")
PhysicalExam.create(name: "e. Dentadura", region: "4. Oro faringe")
PhysicalExam.create(name: "a. Tabique", region: "5. Nariz")
PhysicalExam.create(name: "b. Cornetes", region: "5. Nariz")
PhysicalExam.create(name: "c. Mucosas", region: "5. Nariz")
PhysicalExam.create(name: "d. Senos paranasales", region: "5. Nariz")
PhysicalExam.create(name: "a. Tiroides / masas", region: "6. Cuello")
PhysicalExam.create(name: "b. Movilidad", region: "6. Cuello")
PhysicalExam.create(name: "a. Mamas", region: "7. Tórax")
PhysicalExam.create(name: "b. Corazón", region: "7. Tórax")
PhysicalExam.create(name: "a. Pulmones", region: "8. Tórax")
PhysicalExam.create(name: "b. Parrilla costal", region: "8. Tórax")
PhysicalExam.create(name: "a. Vísceras", region: "9. Abdomen")
PhysicalExam.create(name: "b. Pared abdominal", region: "9. Abdomen")
PhysicalExam.create(name: "a. Flexibilidad", region: "10. Columna")
PhysicalExam.create(name: "b. Desviación", region: "10. Columna")
PhysicalExam.create(name: "c. Dolor", region: "10. Columna")
PhysicalExam.create(name: "a. Pelvis", region: "11. Pelvis")
PhysicalExam.create(name: "b. Genitales", region: "11. Pelvis")
PhysicalExam.create(name: "a. Vascular", region: "12. Extremidades")
PhysicalExam.create(name: "b. Miembros superiores", region: "12. Extremidades")
PhysicalExam.create(name: "c. Miembros inferiores", region: "12. Extremidades")
PhysicalExam.create(name: "a. Fuerza", region: "13. Neurológico")
PhysicalExam.create(name: "b. Sensibilidad", region: "13. Neurológico")
PhysicalExam.create(name: "c. Marcha", region: "13. Neurológico")
PhysicalExam.create(name: "d. Reflejos", region: "13. Neurológico")

p "Load demo data"
demo_company = Company.create(name: 'EcuaAmerican', code: "EC", ruc: "000000001" , active: true, mission: "", mission: "")
main_area = Area.create(name: demo_company.name, code: demo_company.code, active: true, parent_id: 0, company_id: demo_company.id)
first_area = Area.create(name: 'Finaciero M', code: "AF", active: true, parent_id: main_area.id, company_id: demo_company.id)
Area.create(name: 'Compras M', code: "FC", active: true, parent_id: first_area.id, company_id: demo_company.id)
Area.create(name: 'Ventas M', code: "FV", active: true, parent_id: first_area.id, company_id: demo_company.id)
Area.create(name: 'Contable M', code: "AC", active: true, parent_id: main_area.id, company_id: demo_company.id)
Area.create(name: 'Sistemas M', code: "AS", active: true, parent_id: main_area.id, company_id: demo_company.id)
last_area = Area.create(name: 'Bodega M', code: "AB", active: true, parent_id: main_area.id, company_id: demo_company.id)

medical_profile = MedicalProfile.create(name: "Perfil 2020 M", code: "DE2020", active: true, company_id: demo_company.id)
medical_profile.exam_ids = [1, 2, 3, 4]

# first_position = Position.create(name: 'Director Ejecutivo M',company_id: demo_company.id, area_id: first_area.id, medical_profile_id: medical_profile.id, code: "CEOM", gender: 3, age_min: 25, age_max: 35, active: true, number: 0, mission: "")
# Position.create(name: 'Director de Operaciones M', company_id: demo_company.id, area_id: first_area.id, medical_profile_id: medical_profile.id, code: "COOM", gender: 3, age_min: 25, age_max: 35, active: true, number: 1, mission: "Responsable operativo")
# Position.create(name: 'Director Comercial M', company_id: demo_company.id, area_id: first_area.id, medical_profile_id: medical_profile.id, code: "CSOM", gender: 3, age_min: 25, age_max: 35, active: true, number: 2, mission: "Responsable operativo")
# Position.create(name: 'Director de Marketing M', company_id: demo_company.id, area_id: first_area.id, medical_profile_id: medical_profile.id, code: "CMOM", gender: 3, age_min: 25, age_max: 35, active: true, number: 3, mission: "Responsable de marketing")
# last_position = Position.create(name: 'Director de Recursos Humanos M', company_id: demo_company.id, area_id: first_area.id, medical_profile_id: medical_profile.id, code: "CHROM", gender: 3, age_min: 25, age_max: 35, active: true, number: 4, mission: "Responsable de las Personas")


p "Load Positions by CIUO"
first_position = nil
last_position  = nil
# Examns
require 'csv'
datafile = Rails.root + 'db/puestos_ciuo.csv'
CSV.foreach(datafile, headers: true) do |row|
  position = Position.where(:code => row[0].to_s).last
  unless position.present?
    if row[2].to_i == 6
      e = Position.new
      e.code = row[0].to_s
      e.name = row[1].to_s.strip
      e.active = true
      # e.company_id = demo_company.id
      e.gender = Faker::Number.between(from: 1, to: 3)
      e.age_min = Faker::Number.between(from: 25, to: 30)
      e.age_max = Faker::Number.between(from: 55, to: 60) 
      e.save if e.validate
      first_position = e if e.code == "0110.01"
      last_position = e if e.code == "9629.01"
    end
  end
end

WorkCenter.create(name: 'Quito', description: "Matriz", active: true, company_id: demo_company.id, address: "Av. 10 de Agosto")
WorkCenter.create(name: 'Guayaquil', description: "Sucursal 1", active: true, company_id: demo_company.id, address: "Av. 9 de Octubre")
WorkCenter.create(name: 'Cuenca', description: "Sucursal 2", active: true, company_id: demo_company.id, address: "Av. 2 de Noviembre")



150.times do
  partient = Patient.create(ci: Faker::Number.number(digits: 10), 
               ci_type: 2, 
               name: Faker::Name.first_name, 
               name_1: Faker::Name.first_name, 
               last_name: Faker::Name.last_name, 
               last_name_1: Faker::Name.last_name, 
               position_id: Faker::Number.between(from: first_position.id, to: last_position.id),  
               birth_date: Faker::Date.birthday(min_age: 18, max_age: 65), 
               gender: Faker::Boolean.boolean ? 1 : 2, 
               active: true, 
               phone_1: Faker::PhoneNumber.cell_phone,
               email: Faker::Internet.email,
               direction: Faker::Address.full_address,
               civil_status: Faker::Number.between(from: 1, to: 5), 
               work_center_id: Faker::Number.between(from: 1, to: 3), 
               nationality: 52,
               company_id: demo_company.id, 
               province: Faker::Number.between(from: 1, to: 24), 
               city: Faker::Number.between(from: 1, to: 2) == 1 ? "1701" : "0901", 
               height: Faker::Number.between(from: 150, to: 180),
               weight: Faker::Number.between(from: 39, to: 59),
               date_in: Faker::Date.between(from: 5.days.ago, to: Date.today),
               blood_type: Faker::Number.between(from: 1, to: 8),
               area_id: Faker::Number.between(from: first_area.id, to: last_area.id)
              )
  puts "Patient created: #{partient.name} #{partient.last_name}"
  # health_cycle = HealthCycle.create(:year => "2020", :month => "1", :patient_id => partient.id, 
  #                                   :area_id => partient.area_id, :work_center_id => partient.work_center_id,
  #                                   :company_id => demo_company.id, complete: Faker::Boolean.boolean, active: true)
end

main_process = CompanyProcess.create(name: "Almacenaje", owner_id: Patient.first.id, company_id: demo_company.id, active: true, parent_id: nil)
CompanyProcess.create(name: "Pedido de producto", owner_id: Patient.first.id, company_id: demo_company.id, active: true, parent_id: main_process.id)
CompanyProcess.create(name: "Recepción de producto", owner_id: Patient.first.id, company_id: demo_company.id, active: true, parent_id: main_process.id)
CompanyProcess.create(name: "Almacenaje en bodega", owner_id: Patient.first.id, company_id: demo_company.id, active: true, parent_id: main_process.id)
CompanyProcess.create(name: "Acta de entrega", owner_id: Patient.first.id, company_id: demo_company.id, active: true, parent_id: main_process.id)


Activity.create(name: "Encontrar proveedor", process_id: main_process.id, number: 1)
Activity.create(name: "Compra de materiales ", process_id: main_process.id, number: 2)
Activity.create(name: "Realizar Factura y registro contable", process_id: main_process.id, number: 3)


p "Finished load data!!"
