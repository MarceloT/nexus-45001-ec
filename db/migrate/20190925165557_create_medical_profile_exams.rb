class CreateMedicalProfileExams < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_profile_exams do |t|
      t.integer :exam_id
      t.integer :medical_profile_id
      t.timestamps
    end
  end
end
