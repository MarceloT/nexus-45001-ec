class AddAptitudeFieldsToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :retirement_satisfaction, :boolean
    add_column :medical_records, :retirement_conditions, :text
  end
end
