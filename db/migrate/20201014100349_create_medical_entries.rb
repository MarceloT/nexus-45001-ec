class CreateMedicalEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_entries do |t|
      t.string :nro_ord
      t.string :cod_ana
      t.integer :patient_id
      t.integer :doctor_id
      t.integer :company_id
      t.integer :medical_service_id
      t.integer :created_by_id

      t.timestamps
    end
  end
end
