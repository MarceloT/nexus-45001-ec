class CreateCompanyProcesses < ActiveRecord::Migration[5.2]
  def change
    create_table :company_processes do |t|
      t.string :name
      t.text :description
      t.integer :owner_id
      t.integer :company_id
      t.integer :parent_id
      t.integer :level
      t.integer :process_in_id
      t.integer :process_out_id
      t.integer :process_type

      t.timestamps
    end
  end
end
