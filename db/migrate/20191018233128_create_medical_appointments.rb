class CreateMedicalAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_appointments do |t|
      t.datetime :date
      t.datetime :time
      t.integer :medical_type_id
      t.integer :duration
      t.integer :doctor_id
      t.integer :patient_id
      t.integer :medical_record_id
      t.integer :status
      t.text :reason

      t.timestamps
    end
  end
end
