class CreateMedicalRecordExams < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_exams do |t|
      t.datetime :date
      t.text :result
      t.integer :medical_record_id
      t.integer :exam_id

      t.timestamps
    end
  end
end
