class AddCommentsToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :comments, :text
  end
end
