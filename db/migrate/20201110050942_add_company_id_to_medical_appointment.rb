class AddCompanyIdToMedicalAppointment < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_appointments, :company_id, :integer
  end
end
