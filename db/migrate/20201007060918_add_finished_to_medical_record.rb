class AddFinishedToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :is_finished, :boolean
  end
end
