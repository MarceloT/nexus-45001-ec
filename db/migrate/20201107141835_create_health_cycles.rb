class CreateHealthCycles < ActiveRecord::Migration[5.2]
  def change
    create_table :health_cycles do |t|
      t.string :year
      t.string :month
      t.integer :patient_id
      t.integer :area_id
      t.integer :work_center_id
      t.integer :company_id
      t.boolean :complete
      t.boolean :active

      t.timestamps
    end
  end
end
