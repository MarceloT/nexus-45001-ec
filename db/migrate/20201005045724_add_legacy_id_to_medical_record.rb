class AddLegacyIdToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :legacy_id, :bigint
  end
end
