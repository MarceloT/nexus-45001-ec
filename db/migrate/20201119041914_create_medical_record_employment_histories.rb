class CreateMedicalRecordEmploymentHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_employment_histories do |t|
      t.integer :medical_record_id
      t.string :company_name
      t.string :job
      t.string :activities
      t.integer :job_time
      t.boolean :physical_risk
      t.boolean :mechanical_risk
      t.boolean :chemical_risk
      t.boolean :biological_risk
      t.boolean :ergonomic_risk
      t.boolean :psychosocial_risk
      t.text :comments

      t.timestamps
    end
  end
end
