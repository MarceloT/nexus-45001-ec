class AddUserIdToArea < ActiveRecord::Migration[5.2]
  def change
  	add_column :areas, :user_id, :integer
  end
end
