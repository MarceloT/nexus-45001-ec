class CreatePhysicalExams < ActiveRecord::Migration[5.2]
  def change
    create_table :physical_exams do |t|
      t.text :name
      t.string :region

      t.timestamps
    end
  end
end
