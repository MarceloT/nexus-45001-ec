class CreateMedicalRecordAccidentDeseases < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_accident_deseases do |t|
      t.integer :medical_record_id
      t.string :type
      t.boolean :qualified
      t.string :qualified_reason
      t.date :quelified_date
      t.text :comments
      t.boolean :active

      t.timestamps
    end
    add_column :medical_record_employment_histories, :active, :boolean
  end
end
