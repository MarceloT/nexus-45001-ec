class AddLegacyIdToPatient < ActiveRecord::Migration[5.2]
  def change
    add_column :patients, :legacy_id, :integer
  end
end
