class AddLegacyIdToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :legacy_id, :integer
  end
end
