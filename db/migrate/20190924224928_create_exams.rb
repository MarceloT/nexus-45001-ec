class CreateExams < ActiveRecord::Migration[5.2]
  def change
    create_table :exams do |t|
      t.string :name
      t.string :code
      t.boolean :active
      t.text :description

      t.timestamps
    end

    add_index :exams, :name
    add_index :exams, :code
    add_index :exams, :id
  end
end
