class CreateCommissions < ActiveRecord::Migration[5.2]
  def change
    create_table :commissions do |t|
      t.integer :work_center_id
      t.integer :company_id
      t.integer :nro_members
      t.datetime :date
      t.boolean :active
      t.integer :type_commission
      t.integer :member_1
      t.integer :member_2
      t.integer :member_3
      t.integer :member_4
      t.integer :member_5
      t.integer :member_6
      t.integer :member_7
      t.integer :member_8
      t.integer :member_9
      t.integer :member_10
      t.integer :member_11
      t.integer :member_12

      t.timestamps
    end
  end
end
