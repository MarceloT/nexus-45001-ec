class CreateAreas < ActiveRecord::Migration[5.2]
  def change
    create_table :areas do |t|
      t.string :name
      t.string :code
      t.boolean :active
      t.text :description
      t.integer :parent_id
      t.integer :company_id
      t.timestamps
    end
  end
end
