class CreateMedicalAppointmentExams < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_appointment_exams do |t|
      t.integer :medical_appointment_id
      t.integer :exam_id
      t.string :description

      t.timestamps
    end
  end
end
