class CreateWorkCenters < ActiveRecord::Migration[5.2]
  def change
    create_table :work_centers do |t|
      t.string :name
      t.text :description
      t.boolean :active
      t.integer :company_id
      t.string :address

      t.timestamps
    end
  end
end
