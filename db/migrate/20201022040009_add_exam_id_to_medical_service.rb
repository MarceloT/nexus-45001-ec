class AddExamIdToMedicalService < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_services, :exam_id, :integer
  end
end
