class CreatePersonalProtectionEntryPersonalProtections < ActiveRecord::Migration[5.2]
  def change
    create_table :personal_protection_entry_personal_protections do |t|
      t.integer :personal_protection_entry_id
      t.integer :personal_protection_id
      t.datetime :max_date
      t.boolean :changeable
      t.integer :quantity
      t.text :description

      t.timestamps
    end
  end
end
