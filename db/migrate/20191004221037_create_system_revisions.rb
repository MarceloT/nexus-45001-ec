class CreateSystemRevisions < ActiveRecord::Migration[5.2]
  def change
    create_table :system_revisions do |t|
      t.string :name

      t.timestamps
    end
  end
end
