class CreateMedicalServices < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_services do |t|
      t.integer :exam_id
      t.integer :company_id
      t.string :name
      t.boolean :active
      t.boolean :simple

      t.timestamps
    end
  end
end
