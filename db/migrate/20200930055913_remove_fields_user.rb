class RemoveFieldsUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :clave, :string
    remove_column :users, :cod_postal, :string
    remove_column :users, :direccion, :string
    remove_column :users, :fax, :string
    remove_column :users, :forma_envio, :string
    remove_column :users, :mat_med, :string
    remove_column :users, :observacion, :string
    remove_column :users, :tel_med, :string
  end
end
