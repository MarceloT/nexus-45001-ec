class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :description
      t.date :due_date
      t.date :start_date
      t.integer :number
      t.integer :progress
      t.integer :status
      t.integer :owner_id
      t.integer :project_id

      t.timestamps
    end
  end
end
