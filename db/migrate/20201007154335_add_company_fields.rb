class AddCompanyFields < ActiveRecord::Migration[5.2]
  def change
  	add_column :companies, :ruc, :string
  	add_column :companies, :email, :string
  	add_column :companies, :phone, :string
  	add_column :companies, :contact_name, :string
  	add_column :companies, :address, :string
  end
end
