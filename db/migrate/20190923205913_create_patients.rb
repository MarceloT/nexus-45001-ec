class CreatePatients < ActiveRecord::Migration[5.2]
  def change
    create_table :patients do |t|
      t.string :name
      t.string :name_1
      t.string :last_name
      t.string :last_name_1
      t.string :ci
      t.integer :ci_type
      t.integer :gender
      t.datetime :birth_date
      t.string :phone_1
      t.string :phone_2
      t.string :email
      t.integer :civil_status
      t.integer :nationality
      t.text :direction
      t.integer :profession
      t.string :photo
      t.integer :blood_type
      t.boolean :active
      t.text :description
      t.integer :company_id
      t.integer :area_id
      t.integer :position_id

      t.timestamps
    end
  end
end
