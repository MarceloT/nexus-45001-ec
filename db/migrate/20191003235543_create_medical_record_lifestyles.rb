class CreateMedicalRecordLifestyles < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_lifestyles do |t|
      t.boolean :active
      t.string :description
      t.string :time
      t.integer :medical_record_id
      t.integer :lifestyle_id

      t.timestamps
    end
  end
end
