class AddFieldToRiskEvaluationRisks < ActiveRecord::Migration[5.2]
  def change
  	add_column :risk_evaluation_risks, :control_id, :integer
  	add_column :risk_evaluation_risks, :process_work, :string
  	add_column :risk_evaluation_risks, :information, :string
  	add_column :risk_evaluation_risks, :formation, :string
  	add_column :risk_evaluation_risks, :controled_risk, :boolean
  	add_column :risk_evaluation_risks, :project_id, :integer
  end
end
