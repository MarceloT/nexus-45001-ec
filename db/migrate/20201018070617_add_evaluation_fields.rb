class AddEvaluationFields < ActiveRecord::Migration[5.2]
  def change
  	add_column :risk_evaluations, :members, :integer
  	add_column :risk_evaluations, :activity_id, :integer
  	add_column :risk_evaluations, :created_by_id, :integer
  	add_column :risk_evaluations, :owner_id, :integer
  	add_column :risk_evaluations, :evaluation_type_id, :integer
  end
end
