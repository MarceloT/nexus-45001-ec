class AddMeficalProfileToPosition < ActiveRecord::Migration[5.2]
  def change
    add_column :positions, :medical_profile_id, :integer
  end
end
