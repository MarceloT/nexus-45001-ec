class AddActiceToAtivities < ActiveRecord::Migration[5.2]
  def change
  	add_column :activities, :active, :boolean, default: false
  	add_column :medical_records, :initial, :boolean, default: false
  	add_column :medical_records, :clinical_history, :text
  	add_column :medical_records, :incident, :text

  	add_column :medical_records, :work_accidents, :text
  	add_column :medical_records, :work_accidents_check, :boolean
  	add_column :medical_records, :work_accidents_description, :string
  	add_column :medical_records, :work_accidents_date, :date

  	add_column :medical_records, :occupational_diseases, :text
  	add_column :medical_records, :occupational_diseases_check, :boolean
  	add_column :medical_records, :occupational_diseases_description, :string
  	add_column :medical_records, :occupational_diseases_date, :date

    add_column :medical_records, :family_history_description, :text

    add_column :medical_records, :revision_description, :text
    
    add_column :medical_records, :exam_description, :text

  end
end
