class CreateRiskCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :risk_categories do |t|
      t.string :name
      t.string :color
      t.text :description
      t.boolean :active

      t.timestamps
    end
  end
end
