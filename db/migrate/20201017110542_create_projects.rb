class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.text :object
      t.text :scope
      t.date :due_date
      t.integer :status, default: 0
      t.integer :owner_id
      t.integer :project_type_id
      t.integer :company_id
      t.integer :created_by_id

      t.timestamps
    end
  end
end
