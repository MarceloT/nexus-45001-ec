class CreateMedicalRecordVitalConstants < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_vital_constants do |t|
      t.decimal :value
      t.decimal :max_value
      t.decimal :min_value
      t.integer :medical_record_id
      t.integer :vital_constant_id

      t.timestamps
    end
  end
end
