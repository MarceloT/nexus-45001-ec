class AddStuatusToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :status, :integer, default: 0
    remove_column :medical_records, :is_finished
  end
end
