class UpdateMedicalServices < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_services, :jasper_template_url, :string
    remove_column :medical_services, :exam_id
    remove_column :medical_services, :simple
  end
end
