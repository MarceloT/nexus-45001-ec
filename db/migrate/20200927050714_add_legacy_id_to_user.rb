class AddLegacyIdToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :legacy_id, :int
    add_column :users, :clave, :string
    add_column :users, :cod_postal, :string
    add_column :users, :direccion, :string
    add_column :users, :fax, :string
    add_column :users, :forma_envio, :string
    add_column :users, :mat_med, :string
    add_column :users, :observacion, :string
    add_column :users, :tel_med, :string
  end
end
