class CreateMedicalRecordSystemRevisions < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_system_revisions do |t|
      t.boolean :active
      t.string :description
      t.integer :medical_record_id
      t.integer :system_revision_id

      t.timestamps
    end
  end
end
