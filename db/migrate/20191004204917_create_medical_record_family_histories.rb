class CreateMedicalRecordFamilyHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_family_histories do |t|
      t.boolean :active
      t.string :description
      t.integer :medical_record_id
      t.integer :family_history_id

      t.timestamps
    end
  end
end
