class CreatePersonalHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :personal_histories do |t|
      t.string :name

      t.timestamps
    end
  end
end
