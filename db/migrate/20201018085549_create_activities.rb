class CreateActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :activities do |t|
      t.text :name
      t.integer :process_id
      t.integer :number

      t.timestamps
    end
    add_column :company_processes, :active, :boolean
  end
end
