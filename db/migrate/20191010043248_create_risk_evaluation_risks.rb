      class CreateRiskEvaluationRisks < ActiveRecord::Migration[5.2]
  def change
    create_table :risk_evaluation_risks do |t|
      t.integer :m_count, default: 0
      t.integer :f_count, default: 0
      t.integer :s_count, default: 0
      t.text :description
      t.decimal :reference_value, precision: 5, scale: 2, default: 0
      t.decimal :medium_value, precision: 5, scale: 2, default: 0
      t.decimal :exposition_value, precision: 5, scale: 2, default: 0
      t.decimal :result, precision: 5, scale: 2, default: 0
      t.string :result_gr
      t.integer :risk_id
      t.integer :risk_category_id
      t.integer :risk_evaluation_id

      t.timestamps
    end
  end
end
