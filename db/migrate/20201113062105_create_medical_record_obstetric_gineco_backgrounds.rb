class CreateMedicalRecordObstetricGinecoBackgrounds < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_obstetric_gineco_backgrounds do |t|
      t.integer :medical_record_id
      
      t.string :menarche
      t.string :cycles
      t.date :last_menstruation_date
      t.integer :pregnancies
      t.integer :labors
      t.integer :caesarean
      t.integer :abortions
      t.integer :living_children
      t.integer :dead_children
      t.boolean :active_sex_life
      t.boolean :family_planning
      t.string :family_planning_type

      t.timestamps
    end
  end
end
