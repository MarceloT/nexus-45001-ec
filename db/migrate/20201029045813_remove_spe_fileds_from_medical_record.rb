class RemoveSpeFiledsFromMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    remove_column :medical_records, :ophthalmolytic_obs
    remove_column :medical_records, :far_correction
    remove_column :medical_records, :far_left_eye
    remove_column :medical_records, :far_right_eye
    remove_column :medical_records, :near_correction
    remove_column :medical_records, :near_left_eye
    remove_column :medical_records, :near_right_eye

    remove_column :medical_records, :trauma_type
    remove_column :medical_records, :sitting_posture
    remove_column :medical_records, :standing_posture
    remove_column :medical_records, :forced_posture
    remove_column :medical_records, :repetitive_movements
    remove_column :medical_records, :manual_heavy_cargo
    remove_column :medical_records, :avg_wieght
    remove_column :medical_records, :y_distance
    remove_column :medical_records, :dorsal_kyphosis
    remove_column :medical_records, :lumbar_kyphosis
    remove_column :medical_records, :dorsal_scoliosis
    remove_column :medical_records, :lumbar_scoliosis
    remove_column :medical_records, :cervical_lordosis_curvature
    remove_column :medical_records, :dorsal_kyphosis_curvature
    remove_column :medical_records, :lumbar_kyphosis_curvature
  end
end
