class ChangeLegacyIdColumnType < ActiveRecord::Migration[5.2]
  def change
    change_column :companies, :legacy_id, :string
    change_column :patients, :legacy_id, :string
    change_column :users, :legacy_id, :string
    change_column :medical_records, :legacy_id, :string
  end
end
