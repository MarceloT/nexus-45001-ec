class AddNroOrderToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
  	add_column :medical_records, :nro_ord, :string
  	add_column :medical_records, :cod_ana, :string
  end
end
