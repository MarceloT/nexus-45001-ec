class CreateMedicalRecordToxicHabits < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_toxic_habits do |t|
      t.boolean :active
      t.string :time
      t.string :quantity
      t.boolean :ex_consumer
      t.string :abstinence_time
      t.integer :medical_record_id
      t.integer :toxic_habit_id

      t.timestamps
    end
  end
end
