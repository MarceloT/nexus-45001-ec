class CreatePersonalProtectionPatients < ActiveRecord::Migration[5.2]
  def change
    create_table :personal_protection_patients do |t|
      t.integer :personal_protection_id
      t.integer :patient_id
      t.datetime :date
      t.string :signature
      t.integer :company_id

      t.timestamps
    end
  end
end
