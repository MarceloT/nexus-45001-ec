class CreateMedicalProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_profiles do |t|
      t.string :name
      t.string :code
      t.boolean :active
      t.text :description
      t.integer :company_id

      t.timestamps
    end
  end
end
