class CreateRiskEvaluations < ActiveRecord::Migration[5.2]
  def change
    create_table :risk_evaluations do |t|
      t.string :name
      t.integer :company_id
      t.integer :process_id
      t.integer :sub_process_id
      t.integer :position_id
      t.integer :area_id
      t.datetime :date
      t.string :occupational_manager
      t.string :evaluation_manager
      t.string :company_evaluation
      t.text :description

      t.timestamps
    end
  end
end
