class AddAptitudeRecomendationsToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :aptitude_recomendations, :text
  end
end
