class CreateMedicalRecordPhysicalExams < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_physical_exams do |t|
      t.boolean :active
      t.string :description
      t.integer :medical_record_id
      t.integer :physical_exam_id

      t.timestamps
    end
  end
end
