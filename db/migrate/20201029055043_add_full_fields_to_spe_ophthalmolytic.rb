class AddFullFieldsToSpeOphthalmolytic < ActiveRecord::Migration[5.2]
  def change
    add_column :spe_ophthalmolytics, :tonometry_left_eye, :string
    add_column :spe_ophthalmolytics, :tonometry_right_eye, :string
    add_column :spe_ophthalmolytics, :color_test_left_eye, :string
    add_column :spe_ophthalmolytics, :color_test_right_eye, :string
    add_column :spe_ophthalmolytics, :perimetry_left_eye, :string
    add_column :spe_ophthalmolytics, :perimetry_right_eye, :string
    add_column :spe_ophthalmolytics, :stereopsis_left_eye, :string
    add_column :spe_ophthalmolytics, :stereopsis_right_eye, :string
    add_column :spe_ophthalmolytics, :ocular_movements, :string
    add_column :spe_ophthalmolytics, :biomicroscopy, :string
    add_column :spe_ophthalmolytics, :eye_background, :string
  end
end
