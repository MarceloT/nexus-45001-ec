class CreateMedicalRecordPersonalHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_personal_histories do |t|
      t.integer :medical_record_id
      t.integer :personal_history_id

      t.timestamps
    end
  end
end
