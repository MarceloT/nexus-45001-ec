class CreateVitalConstants < ActiveRecord::Migration[5.2]
  def change
    create_table :vital_constants do |t|
      t.string :name

      t.timestamps
    end
  end
end
