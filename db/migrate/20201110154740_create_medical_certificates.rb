class CreateMedicalCertificates < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_certificates do |t|
      t.datetime :date
      t.integer :patient_id
      t.integer :company_id
      t.integer :medical_record_id
      t.text :reason
      t.text :diagnostic_description
      t.integer :days
      t.datetime :starts_date
      t.datetime :ends_date
      t.integer :created_by_id
      t.integer :document_id
      t.integer :type_certificate

      t.timestamps
    end
  end
end
