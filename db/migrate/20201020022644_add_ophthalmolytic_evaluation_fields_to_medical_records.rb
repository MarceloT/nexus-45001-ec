class AddOphthalmolyticEvaluationFieldsToMedicalRecords < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :ophthalmolytic_obs, :text
    add_column :medical_records, :far_correction, :boolean
    add_column :medical_records, :far_left_eye, :string
    add_column :medical_records, :far_right_eye, :string
    add_column :medical_records, :near_correction, :boolean
    add_column :medical_records, :near_left_eye, :string
    add_column :medical_records, :near_right_eye, :string
  end
end
