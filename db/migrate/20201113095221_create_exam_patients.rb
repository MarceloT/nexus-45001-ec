class CreateExamPatients < ActiveRecord::Migration[5.2]
  def change
    create_table :exam_patients do |t|
      t.integer :patient_id
      t.integer :exam_id
      t.integer :determination_id
      t.text :determination
      t.string :result
      t.string :unit
      t.string :ref_value
      t.string :nro_ord
      t.boolean :active

      t.timestamps
    end
  end
end
