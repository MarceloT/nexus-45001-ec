class CreatePersonalProtectionEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :personal_protection_entries do |t|
      t.integer :origin_work_center_id
      t.integer :destination_work_center_id
      t.integer :patient_id
      t.datetime :date
      t.string :signature
      t.integer :company_id
      t.integer :user_id
      t.text :description

      t.timestamps
    end
  end
end
