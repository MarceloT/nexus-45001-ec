class AddCompanyToPositions < ActiveRecord::Migration[5.2]
  def change
    add_column :positions, :area_id, :integer
    add_column :positions, :company_id, :integer
  end
end
