class CreateRecipes < ActiveRecord::Migration[5.2]
  def change
    create_table :recipes do |t|
      t.text :diagnostic_description
      t.text :medicines
      t.text :indications
      t.integer :medical_record_id
      t.integer :patient_id
      t.integer :doctor_id

      t.timestamps
    end
  end
end
