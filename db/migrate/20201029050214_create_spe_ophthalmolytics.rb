class CreateSpeOphthalmolytics < ActiveRecord::Migration[5.2]
  def change
    create_table :spe_ophthalmolytics do |t|
      t.integer :medical_record_id

      t.text :ophthalmolytic_obs
      t.boolean :far_correction
      t.string :far_left_eye
      t.string :far_right_eye
      t.boolean :near_correction
      t.string :near_left_eye
      t.string :near_right_eye

      t.timestamps
    end
  end
end
