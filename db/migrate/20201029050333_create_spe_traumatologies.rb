class CreateSpeTraumatologies < ActiveRecord::Migration[5.2]
  def change
    create_table :spe_traumatologies do |t|
      t.integer :medical_record_id

      t.string :trauma_type
      t.integer :sitting_posture
      t.integer :standing_posture
      t.boolean :forced_posture
      t.boolean :repetitive_movements
      t.boolean :manual_heavy_cargo
      t.decimal :avg_wieght
      t.decimal :y_distance
      t.boolean :dorsal_kyphosis
      t.boolean :lumbar_kyphosis
      t.boolean :dorsal_scoliosis
      t.boolean :lumbar_scoliosis
      t.string :cervical_lordosis_curvature
      t.string :dorsal_kyphosis_curvature
      t.string :lumbar_kyphosis_curvature


      t.timestamps
    end
  end
end
