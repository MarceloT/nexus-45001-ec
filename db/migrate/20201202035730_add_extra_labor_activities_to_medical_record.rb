class AddExtraLaborActivitiesToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :extra_labor_activities, :text
  end
end
