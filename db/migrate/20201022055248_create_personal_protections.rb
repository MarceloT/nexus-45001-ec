class CreatePersonalProtections < ActiveRecord::Migration[5.2]
  def change
    create_table :personal_protections do |t|
      t.string :name
      t.integer :protection_type_id
      t.integer :company_id
      t.integer :stock
      t.string :image
      t.text :features
      t.text :details

      t.timestamps
    end
  end
end
