class CreatePositions < ActiveRecord::Migration[5.2]
  def change
    create_table :positions do |t|
      t.string :name
      t.string :code
      t.integer :gender
      t.integer :age_min
      t.integer :age_max
      t.boolean :active
      t.integer :number
      t.string :mission

      t.timestamps
    end
  end
end
