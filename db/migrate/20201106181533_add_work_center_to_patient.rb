class AddWorkCenterToPatient < ActiveRecord::Migration[5.2]
  def change
  	add_column :patients, :work_center_id, :integer
  end
end
