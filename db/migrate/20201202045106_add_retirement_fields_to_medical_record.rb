class AddRetirementFieldsToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :retirement_evaluation_carried_out, :boolean
    add_column :medical_records, :retirement_comments, :text
  end
end
