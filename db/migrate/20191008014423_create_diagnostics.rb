class CreateDiagnostics < ActiveRecord::Migration[5.2]
  def change
    create_table :diagnostics do |t|
      t.text :name
      t.string :code
      t.integer :chapter

      t.timestamps
    end
  end
end
