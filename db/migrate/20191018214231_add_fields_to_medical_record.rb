class AddFieldsToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :company_id, :integer
    add_column :medical_records, :position_id, :integer
    add_column :medical_records, :area_id, :integer
    add_column :medical_records, :ruc, :string
    add_column :medical_records, :ciu, :string
    add_column :medical_records, :medical_department, :string
    add_column :medical_records, :number_medical_record, :string
    add_column :medical_records, :number_file, :string
    add_column :medical_records, :starts_work, :datetime
    add_column :medical_records, :ends_work, :datetime
    add_column :medical_records, :time_work, :string

    add_column :medical_records, :last_day_work, :datetime
    add_column :medical_records, :reinsert_work, :datetime
    add_column :medical_records, :days_work, :string
    add_column :medical_records, :reason_to_go, :string

    add_column :patients, :religion_id, :integer
    add_column :patients, :laterality, :integer
    add_column :patients, :sexual_orientation, :integer
    add_column :patients, :sexual_identity, :integer
    add_column :patients, :disability, :boolean
    add_column :patients, :disability_type_id, :integer
    add_column :patients, :disabilit_percentage, :integer
    add_column :patients, :signature, :string

    add_column :users, :code, :string
    add_column :users, :signature, :string


  end
end
