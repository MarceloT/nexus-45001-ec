class AddTraumaFieldsToMedicalRecord < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_records, :trauma_type, :string
    add_column :medical_records, :sitting_posture, :integer
    add_column :medical_records, :standing_posture, :integer
    add_column :medical_records, :forced_posture, :boolean
    add_column :medical_records, :repetitive_movements, :boolean
    add_column :medical_records, :manual_heavy_cargo, :boolean
    add_column :medical_records, :avg_wieght, :decimal
    add_column :medical_records, :y_distance, :decimal
    add_column :medical_records, :dorsal_kyphosis, :boolean
    add_column :medical_records, :lumbar_kyphosis, :boolean
    add_column :medical_records, :dorsal_scoliosis, :boolean
    add_column :medical_records, :lumbar_scoliosis, :boolean
    add_column :medical_records, :cervical_lordosis_curvature, :string
    add_column :medical_records, :dorsal_kyphosis_curvature, :string
    add_column :medical_records, :lumbar_kyphosis_curvature, :string
  end
end
