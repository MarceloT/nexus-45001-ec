class AddFieldsToPatients < ActiveRecord::Migration[5.2]
  def change
  	add_column :patients, :province, :integer
  	add_column :patients, :city, :integer
  	add_column :patients, :height, :decimal, precision: 5, scale: 2
  	add_column :patients, :weight, :decimal, precision: 5, scale: 2
  	add_column :patients, :date_in, :datetime
  	add_column :patients, :date_out, :datetime
  end
end
