class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :code
      t.boolean :active
      t.text :mission
      t.text :vision
      t.string :logo

      t.timestamps
    end
  end
end
