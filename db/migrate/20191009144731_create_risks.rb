class CreateRisks < ActiveRecord::Migration[5.2]
  def change
    create_table :risks do |t|
      t.string :name
      t.string :code
      t.text :description
      t.boolean :active
      t.integer :risk_category_id

      t.timestamps
    end
  end
end
