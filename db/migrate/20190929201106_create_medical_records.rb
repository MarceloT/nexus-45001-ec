class CreateMedicalRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_records do |t|
      t.datetime :date
      t.text :reason
      t.text :current_disease
      t.text :recommendations
      t.integer :patient_id
      t.integer :doctor_id
      t.integer :medical_type_id
      t.string :doctor_signature
      t.integer :aptitude
      t.text :aptitude_obs
      t.string :aptitude_limit

      t.timestamps
    end
  end
end
