class CreateMedicalRecordDiagnostics < ActiveRecord::Migration[5.2]
  def change
    create_table :medical_record_diagnostics do |t|
      t.boolean :pre, default: false
      t.integer :medical_record_id
      t.integer :diagnostic_id

      t.timestamps
    end
  end
end
