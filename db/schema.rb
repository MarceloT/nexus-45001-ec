# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_02_065631) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.text "name"
    t.integer "process_id"
    t.integer "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: false
  end

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.boolean "active"
    t.text "description"
    t.integer "parent_id"
    t.integer "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
  end

  create_table "commissions", force: :cascade do |t|
    t.integer "work_center_id"
    t.integer "company_id"
    t.integer "nro_members"
    t.datetime "date"
    t.boolean "active"
    t.integer "type_commission"
    t.integer "member_1"
    t.integer "member_2"
    t.integer "member_3"
    t.integer "member_4"
    t.integer "member_5"
    t.integer "member_6"
    t.integer "member_7"
    t.integer "member_8"
    t.integer "member_9"
    t.integer "member_10"
    t.integer "member_11"
    t.integer "member_12"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "president"
    t.integer "delagate"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.boolean "active"
    t.text "mission"
    t.text "vision"
    t.string "logo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "legacy_id"
    t.string "ruc"
    t.string "email"
    t.string "phone"
    t.string "contact_name"
    t.string "address"
  end

  create_table "company_processes", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "owner_id"
    t.integer "company_id"
    t.integer "parent_id"
    t.integer "level"
    t.integer "process_in_id"
    t.integer "process_out_id"
    t.integer "process_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active"
  end

  create_table "diagnostics", force: :cascade do |t|
    t.text "name"
    t.string "code"
    t.integer "chapter"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exam_patients", force: :cascade do |t|
    t.integer "patient_id"
    t.integer "exam_id"
    t.integer "determination_id"
    t.text "determination"
    t.string "result"
    t.string "unit"
    t.string "ref_value"
    t.string "nro_ord"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exams", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.boolean "active"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_exams_on_code"
    t.index ["id"], name: "index_exams_on_id"
    t.index ["name"], name: "index_exams_on_name"
  end

  create_table "family_histories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "health_cycles", force: :cascade do |t|
    t.string "year"
    t.string "month"
    t.integer "patient_id"
    t.integer "area_id"
    t.integer "work_center_id"
    t.integer "company_id"
    t.boolean "complete"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lifestyles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_appointment_exams", force: :cascade do |t|
    t.integer "medical_appointment_id"
    t.integer "exam_id"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_appointments", force: :cascade do |t|
    t.datetime "date"
    t.datetime "time"
    t.integer "medical_type_id"
    t.integer "duration"
    t.integer "doctor_id"
    t.integer "patient_id"
    t.integer "medical_record_id"
    t.integer "status"
    t.text "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "company_id"
  end

  create_table "medical_certificates", force: :cascade do |t|
    t.datetime "date"
    t.integer "patient_id"
    t.integer "company_id"
    t.integer "medical_record_id"
    t.text "reason"
    t.text "diagnostic_description"
    t.integer "days"
    t.datetime "starts_date"
    t.datetime "ends_date"
    t.integer "created_by_id"
    t.integer "document_id"
    t.integer "type_certificate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_entries", force: :cascade do |t|
    t.string "nro_ord"
    t.string "cod_ana"
    t.integer "patient_id"
    t.integer "doctor_id"
    t.integer "company_id"
    t.integer "medical_service_id"
    t.integer "created_by_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_profile_exams", force: :cascade do |t|
    t.integer "exam_id"
    t.integer "medical_profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_profiles", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.boolean "active"
    t.text "description"
    t.integer "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_accident_deseases", force: :cascade do |t|
    t.integer "medical_record_id"
    t.string "accident_type"
    t.boolean "qualified"
    t.string "qualified_reason"
    t.date "quelified_date"
    t.text "comments"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_diagnostics", force: :cascade do |t|
    t.boolean "pre", default: false
    t.integer "medical_record_id"
    t.integer "diagnostic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_employment_histories", force: :cascade do |t|
    t.integer "medical_record_id"
    t.string "company_name"
    t.string "job"
    t.string "activities"
    t.integer "job_time"
    t.boolean "physical_risk"
    t.boolean "mechanical_risk"
    t.boolean "chemical_risk"
    t.boolean "biological_risk"
    t.boolean "ergonomic_risk"
    t.boolean "psychosocial_risk"
    t.text "comments"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active"
  end

  create_table "medical_record_exams", force: :cascade do |t|
    t.datetime "date"
    t.text "result"
    t.integer "medical_record_id"
    t.integer "exam_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_family_histories", force: :cascade do |t|
    t.boolean "active"
    t.string "description"
    t.integer "medical_record_id"
    t.integer "family_history_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_lifestyles", force: :cascade do |t|
    t.boolean "active"
    t.string "description"
    t.string "time"
    t.integer "medical_record_id"
    t.integer "lifestyle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_obstetric_gineco_backgrounds", force: :cascade do |t|
    t.integer "medical_record_id"
    t.string "menarche"
    t.string "cycles"
    t.date "last_menstruation_date"
    t.integer "pregnancies"
    t.integer "labors"
    t.integer "caesarean"
    t.integer "abortions"
    t.integer "living_children"
    t.integer "dead_children"
    t.boolean "active_sex_life"
    t.boolean "family_planning"
    t.string "family_planning_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_personal_histories", force: :cascade do |t|
    t.integer "medical_record_id"
    t.integer "personal_history_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_physical_exams", force: :cascade do |t|
    t.boolean "active"
    t.string "description"
    t.integer "medical_record_id"
    t.integer "physical_exam_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_system_revisions", force: :cascade do |t|
    t.boolean "active"
    t.string "description"
    t.integer "medical_record_id"
    t.integer "system_revision_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_toxic_habits", force: :cascade do |t|
    t.boolean "active"
    t.string "time"
    t.string "quantity"
    t.boolean "ex_consumer"
    t.string "abstinence_time"
    t.integer "medical_record_id"
    t.integer "toxic_habit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_record_vital_constants", force: :cascade do |t|
    t.decimal "value"
    t.decimal "max_value"
    t.decimal "min_value"
    t.integer "medical_record_id"
    t.integer "vital_constant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_records", force: :cascade do |t|
    t.datetime "date"
    t.text "reason"
    t.text "current_disease"
    t.text "recommendations"
    t.integer "patient_id"
    t.integer "doctor_id"
    t.integer "medical_type_id"
    t.string "doctor_signature"
    t.integer "aptitude"
    t.text "aptitude_obs"
    t.string "aptitude_limit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "company_id"
    t.integer "position_id"
    t.integer "area_id"
    t.string "ruc"
    t.string "ciu"
    t.string "medical_department"
    t.string "number_medical_record"
    t.string "number_file"
    t.datetime "starts_work"
    t.datetime "ends_work"
    t.string "time_work"
    t.datetime "last_day_work"
    t.datetime "reinsert_work"
    t.string "days_work"
    t.string "reason_to_go"
    t.string "legacy_id"
    t.integer "status", default: 0
    t.text "comments"
    t.boolean "initial", default: false
    t.text "clinical_history"
    t.text "incident"
    t.text "work_accidents"
    t.boolean "work_accidents_check"
    t.string "work_accidents_description"
    t.date "work_accidents_date"
    t.text "occupational_diseases"
    t.boolean "occupational_diseases_check"
    t.string "occupational_diseases_description"
    t.date "occupational_diseases_date"
    t.text "family_history_description"
    t.text "revision_description"
    t.text "exam_description"
    t.string "nro_ord"
    t.string "cod_ana"
    t.text "extra_labor_activities"
    t.boolean "retirement_evaluation_carried_out"
    t.text "retirement_comments"
    t.boolean "retirement_satisfaction"
    t.text "retirement_conditions"
    t.text "aptitude_recomendations"
  end

  create_table "medical_services", force: :cascade do |t|
    t.integer "company_id"
    t.string "name"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "jasper_template_url"
    t.integer "exam_id"
  end

  create_table "patients", force: :cascade do |t|
    t.string "name"
    t.string "name_1"
    t.string "last_name"
    t.string "last_name_1"
    t.string "ci"
    t.integer "ci_type"
    t.integer "gender"
    t.datetime "birth_date"
    t.string "phone_1"
    t.string "phone_2"
    t.string "email"
    t.integer "civil_status"
    t.integer "nationality"
    t.text "direction"
    t.integer "profession"
    t.string "photo"
    t.integer "blood_type"
    t.boolean "active"
    t.text "description"
    t.integer "company_id"
    t.integer "area_id"
    t.integer "position_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "religion_id"
    t.integer "laterality"
    t.integer "sexual_orientation"
    t.integer "sexual_identity"
    t.boolean "disability"
    t.integer "disability_type_id"
    t.integer "disabilit_percentage"
    t.string "signature"
    t.string "legacy_id"
    t.integer "province"
    t.integer "city"
    t.decimal "height", precision: 5, scale: 2
    t.decimal "weight", precision: 5, scale: 2
    t.datetime "date_in"
    t.datetime "date_out"
    t.integer "work_center_id"
  end

  create_table "personal_histories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "personal_protection_entries", force: :cascade do |t|
    t.integer "origin_work_center_id"
    t.integer "destination_work_center_id"
    t.integer "patient_id"
    t.datetime "date"
    t.string "signature"
    t.integer "company_id"
    t.integer "user_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "personal_protection_entry_personal_protections", force: :cascade do |t|
    t.integer "personal_protection_entry_id"
    t.integer "personal_protection_id"
    t.datetime "max_date"
    t.boolean "changeable"
    t.integer "quantity"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "personal_protection_patients", force: :cascade do |t|
    t.integer "personal_protection_id"
    t.integer "patient_id"
    t.datetime "date"
    t.string "signature"
    t.integer "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "personal_protections", force: :cascade do |t|
    t.string "name"
    t.integer "protection_type_id"
    t.integer "company_id"
    t.integer "stock"
    t.string "image"
    t.text "features"
    t.text "details"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "physical_exams", force: :cascade do |t|
    t.text "name"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "positions", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.integer "gender"
    t.integer "age_min"
    t.integer "age_max"
    t.boolean "active"
    t.integer "number"
    t.string "mission"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "area_id"
    t.integer "company_id"
    t.integer "medical_profile_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.text "object"
    t.text "scope"
    t.date "due_date"
    t.integer "status", default: 0
    t.integer "owner_id"
    t.integer "project_type_id"
    t.integer "company_id"
    t.integer "created_by_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recipes", force: :cascade do |t|
    t.text "diagnostic_description"
    t.text "medicines"
    t.text "indications"
    t.integer "medical_record_id"
    t.integer "patient_id"
    t.integer "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "risk_categories", force: :cascade do |t|
    t.string "name"
    t.string "color"
    t.text "description"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "risk_evaluation_risks", force: :cascade do |t|
    t.integer "m_count", default: 0
    t.integer "f_count", default: 0
    t.integer "s_count", default: 0
    t.text "description"
    t.decimal "reference_value", precision: 5, scale: 2, default: "0.0"
    t.decimal "medium_value", precision: 5, scale: 2, default: "0.0"
    t.decimal "exposition_value", precision: 5, scale: 2, default: "0.0"
    t.decimal "result", precision: 5, scale: 2, default: "0.0"
    t.string "result_gr"
    t.integer "risk_id"
    t.integer "risk_category_id"
    t.integer "risk_evaluation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "control_id"
    t.string "process_work"
    t.string "information"
    t.string "formation"
    t.boolean "controled_risk"
    t.integer "project_id"
  end

  create_table "risk_evaluations", force: :cascade do |t|
    t.string "name"
    t.integer "company_id"
    t.integer "process_id"
    t.integer "sub_process_id"
    t.integer "position_id"
    t.integer "area_id"
    t.datetime "date"
    t.string "occupational_manager"
    t.string "evaluation_manager"
    t.string "company_evaluation"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "members"
    t.integer "activity_id"
    t.integer "created_by_id"
    t.integer "owner_id"
    t.integer "evaluation_type_id"
  end

  create_table "risks", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.text "description"
    t.boolean "active"
    t.integer "risk_category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "spe_ophthalmolytics", force: :cascade do |t|
    t.integer "medical_record_id"
    t.text "ophthalmolytic_obs"
    t.boolean "far_correction"
    t.string "far_left_eye"
    t.string "far_right_eye"
    t.boolean "near_correction"
    t.string "near_left_eye"
    t.string "near_right_eye"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tonometry_left_eye"
    t.string "tonometry_right_eye"
    t.string "color_test_left_eye"
    t.string "color_test_right_eye"
    t.string "perimetry_left_eye"
    t.string "perimetry_right_eye"
    t.string "stereopsis_left_eye"
    t.string "stereopsis_right_eye"
    t.string "ocular_movements"
    t.string "biomicroscopy"
    t.string "eye_background"
  end

  create_table "spe_traumatologies", force: :cascade do |t|
    t.integer "medical_record_id"
    t.string "trauma_type"
    t.integer "sitting_posture"
    t.integer "standing_posture"
    t.boolean "forced_posture"
    t.boolean "repetitive_movements"
    t.boolean "manual_heavy_cargo"
    t.decimal "avg_wieght"
    t.decimal "y_distance"
    t.boolean "dorsal_kyphosis"
    t.boolean "lumbar_kyphosis"
    t.boolean "dorsal_scoliosis"
    t.boolean "lumbar_scoliosis"
    t.string "cervical_lordosis_curvature"
    t.string "dorsal_kyphosis_curvature"
    t.string "lumbar_kyphosis_curvature"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "system_revisions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.date "due_date"
    t.date "start_date"
    t.integer "number"
    t.integer "progress"
    t.integer "status"
    t.integer "owner_id"
    t.integer "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "toxic_habits", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "login", default: "", null: false
    t.string "name", default: "", null: false
    t.string "photo", default: "", null: false
    t.boolean "active", default: true
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code"
    t.string "signature"
    t.string "legacy_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["login"], name: "index_users_on_login", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  create_table "vital_constants", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "work_centers", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.boolean "active"
    t.integer "company_id"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
