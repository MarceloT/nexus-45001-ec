Rails.application.routes.draw do
  resources :medical_certificates
  resources :personal_protection_entries
  resources :recipes
  resources :personal_protection_patients
  resources :personal_protections
  resources :company_processes
  resources :tasks
  resources :diagnostics
  resources :projects
  resources :commissions
  resources :work_centers
  resources :medical_services
  resources :risk_evaluations
  resources :risk_evaluation_risks
  resources :risks
  resources :risk_categories
  devise_for :users, controllers: { sessions: 'users/sessions' }
  match '/positions/diagram' => 'positions#diagram', :via => [:get, :post, :put]
  resources :medical_appointments do
    collection do
      get "today"
    end
  end
  resources :medical_records do
    collection do
      get "audits"
    end
    member do
      get "audit"
    end
  end
  resources :patient_records do 
    member do
      get "vaccines"
      get "recipes"
      get "laboratories"
      get "medical_history"
      get "medical_certificates"
    end
    resources :medical_checks do
      member do
        get "preview"
        get "result_file"
      end

    end
  end
  resources :medical_results, only: [:index, :show] do
    collection do
      get "distribution"
      get "diseases"
    end
  end
  resources :indicators, only: [:index] do
    collection do
      get "absenteeism"
    end
  end
  resources :medical_entries, only: [:index, :show, :new] do
    collection do
      get "register_values"
      get "list"
    end
  end
  resources :medical_profiles
  resources :exams
  resources :patients do
    collection do
      get "import_nl_patient"
      get "reports"
      get "print" 
    end
  end
  resources :areas
  resources :companies do
    collection do
      get "import_nl_source"
    end
  end
  resources :positions
  resources :users

  get 'risk_evaluations_details_dataset', to: 'risk_evaluations#get_dataset_details'
  get 'risk_evaluations_dataset', to: 'risk_evaluations#get_dataset'
  get 'risk_categories_dataset', to: 'risk_categories#get_dataset'
  get 'risks_dataset', to: 'risks#get_dataset'
  get 'projects_dataset', to: 'projects#get_dataset'
  get 'companies_dataset', to: 'companies#get_dataset'
  get 'commissions_dataset', to: 'commissions#get_dataset'
  get 'work_centers_dataset', to: 'work_centers#get_dataset'
  get 'exams_dataset', to: 'exams#get_dataset'
  get 'diagnostics_dataset', to: 'diagnostics#get_dataset'
  get 'personal_protections_dataset', to: 'personal_protections#get_dataset'
  get 'personal_protection_entries_dataset', to: 'personal_protection_entries#get_dataset'
  get 'positions_dataset', to: 'positions#get_dataset'
  get 'medical_profiles_dataset', to: 'medical_profiles#get_dataset'
  get 'patients_dataset', to: 'patients#get_dataset'
  get 'patient_records_dataset', to: 'patient_records#get_dataset'
  get 'medical_services_dataset', to: 'medical_services#get_dataset'
  get 'users_dataset', to: 'users#get_dataset'
  post 'save_current_company', to: 'companies#save_current_company'
  post 'get_exams_1', to: 'medical_profiles#get_exams_1'
  # Auto save methods
  post 'new_personal_history', to:'medical_records#new_personal_history'
  post 'remove_personal_history', to:'medical_records#remove_personal_history'

  root to: 'stack#index'
  get ':page' => 'stack#show', as: 'stack'

  mount ReportsKit::Engine, at: '/'
  
end
